export class Asistencia {

  id: number;
  name: string;
  asistire: number;
  presente: number;

//para tabla responsiva
apellido: string;
nombre: string;
puesto: number
telefono: string;

  public constructor(name: string, asistire: number, presente: number) {


    this.name = name,
    this.asistire = asistire,
    this.presente = presente

  }

}
