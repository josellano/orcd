export class Reunion{

    id: number;
    fecha: string;
    tipo: string;
    quorum: string;
    numero: number;

    asistira: string;  // se le agrego para mostrar la asistencia en reunion.html

    public constructor (fecha: string, tipo: string){
            this.fecha = fecha,
            this.tipo = tipo
    }
}
