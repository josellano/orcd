export class ComentarioDecision {

  id: number;
  contenido: string;
  item_id: number;
  user_id: number;
  nombre: string;
  apellido:string;

  public constructor( contenido: string, item_id: number) {

    this.contenido = contenido,
    this.item_id= item_id

  }

}
