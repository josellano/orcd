export class Precargada {

  id: number;
  contenido: string;
  tipo: string;

  public constructor( contenido: string, tipo: string) {

    this.contenido = contenido,
    this.tipo = tipo

  }

}
