export class Decision {

  id: number;
  contenido: string;
  item_id: number;

  public constructor( contenido: string, item_id: number) {

    this.contenido = contenido,
    this.item_id= item_id

  }

}
