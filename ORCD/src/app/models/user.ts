export class User {

  id: number;
  nombre: string;
  apellido: string;
  email: string;
  telefono: string;
  password: string;
  confirmPassword: string;
  roles: string[];
  puesto: string;
  titulo_academico: string;
  genero: string;

  public constructor(nombre: string, apellido: string, email: string, telefono: string, password: string, roles: string[], puesto: string, tituloAcademico: string, genero: string) {
      this.nombre = nombre,
      this.email = email,
      this.telefono = telefono,
      this.password = password,
      this.confirmPassword = password,
      this.roles = roles,
      this.puesto = puesto
      this.apellido = apellido;
      this.genero = genero;
      this.titulo_academico = tituloAcademico;
  }
}
