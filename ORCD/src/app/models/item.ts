export class Item {

  id: number;
  numero: number;
  tipo: string;
  contenido: string;
  ordendia_id: number;
  adjunto: string;
  cantComentarios: number;

  public constructor( tipo: string, contenido: string, ordendia_id: number) {
    this.tipo = tipo,
    this.contenido = contenido,
    this.ordendia_id= ordendia_id
  }
}
