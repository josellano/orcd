export class Traductor {

  meses: string[] = new Array(12);


  public constructor() {

    this.meses[0] = "Enero";
    this.meses[1] = "Febrero";
    this.meses[2] = "Marzo";
    this.meses[3] = "Abril";
    this.meses[4] = "Mayo";
    this.meses[5] = "Junio";
    this.meses[6] = "Julio";
    this.meses[7] = "Agosto";
    this.meses[8] = "Septiembre";
    this.meses[9] = "Octubre";
    this.meses[10] = "Noviembre";
    this.meses[11] = "Diciembre";

  }

  public getNombreMes(mesNumero: number):string{
    return this.meses[mesNumero];
  }

}
