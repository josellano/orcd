export class Comunicacion {

  id: number;
  numero: number;
  contenido: string;
  ordenId: number;

  public constructor( contenido: string, ordenId: number) {

    this.contenido = contenido,
    this.ordenId= ordenId

  }

}
