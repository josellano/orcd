import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
@Component({
  selector: 'app-dialog-body',
  templateUrl: './dialog-body.component.html',
  styleUrls: ['./dialog-body.component.scss']
})
export class DialogBodyComponent implements OnInit {

  title: string;
  bottomText: string = "Continuar";
  showBottom: boolean = false;
  showBottomYesNo: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<DialogBodyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.title = data.title;
  }

  ngOnInit() {
    if (this.title.includes("cerrada"))
      this.showBottom = false;
    if (this.title.includes("Creando") || this.title.includes("Guardando") || this.title.includes("Eliminando") || this.title.includes("Agregando") || this.title.includes("Enviando") || this.title.includes("Editando")) {
      this.showBottom = false;
      this.dialogRef.disableClose = true;
    }
    if (this.title.includes("Desea"))
      this.showBottomYesNo = true;

  }

  public closeDialog() {
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSiClick(): void {
    this.dialogRef.close();

  }

}


