import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearPrecargadaComponent } from './crear-precargada.component';

describe('CrearPrecargadaComponent', () => {
  let component: CrearPrecargadaComponent;
  let fixture: ComponentFixture<CrearPrecargadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearPrecargadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearPrecargadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
