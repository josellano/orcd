import { Component, OnInit, ViewChild } from '@angular/core';
import { PrecargadaService } from 'src/app/services/precargada/precargada.service';
import { RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { Precargada } from 'src/app/models/precargada';

@Component({
  selector: 'app-crear-precargada',
  templateUrl: './crear-precargada.component.html',
  styleUrls: ['./crear-precargada.component.scss']
})
export class CrearPrecargadaComponent implements OnInit {

  precargadaId: string;
  tipo: string;
  textoValido: boolean = false;

  title: string = "Creación de contenido precargado";
  precargadaEditada: Precargada;
  editState: boolean = false;

  //errores
  errorsDialog: any= [];
  errorsBack: any = [];

  @ViewChild('fromRTE')
  private rteEle: RichTextEditorComponent;
  public value: string;
  rteCreated(): void {
    this.rteEle.element.focus();
  }

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable', '|',
      'ClearFormat', '|', 'FullScreen']
  };

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog,
    private precargadaService: PrecargadaService
  ) { }

  async ngOnInit() {
    this.precargadaId = this.actRoute.snapshot.paramMap.get('id');
    if (this.precargadaId) {
      this.title = "Edicion de contenido precargado";
      this.editState = true;
      setTimeout(() => {  // para evitar mostrar error por changeDetection DOM desfasada
        this.value = "Buscando contenido precargado";
      });
      await this.obtenerPrecargada(this.precargadaId);
    }
    else {
      this.tipo = this.actRoute.snapshot.paramMap.get('tipo');
      setTimeout(() => {  // para evitar mostrar error por changeDetection DOM desfasada
        this.value = "Ingrese aqui el contenido";
      });
    }
  }

  async obtenerPrecargada(id: string) {
    await this.precargadaService.getPrecargadaByIDPromise(id).then((data: any) => {
      this.precargadaEditada = data;
      this.tipo= this.precargadaEditada.tipo;
    });
    this.value = this.precargadaEditada.contenido;
  }

  validarTexto() {
    const errorSms = '<p><span style="color: rgb(255, 0, 0); text-decoration: inherit;">Por favor ingrese el contenido precargado.</span></p>';
    if (this.value == null || this.value == errorSms) {
      this.value = errorSms;
      this.textoValido = false;
      return false
    }
    else {
      this.textoValido = true;
      return true;
    }
  }

  limpiarTexto() {
    this.value = null;
  }

  goToPrecargadas() {
    this.router.navigate([LinkConst.urlPrecargada + this.tipo]);
  }

  crearPrecargada() {
    var nuevaPrecargada = new Precargada(this.value, this.tipo);
    if (this.textoValido) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Creando el contenido precargado, por favor espere...");

      this.precargadaService.crearPrecargada(nuevaPrecargada, this.tipo).subscribe((data: any) => {
        this.closeDialog();
        this.showDialog("Contenido precargado creado correctamente");
        this.goToPrecargadas();
      },
        err => {
          this.errorsBack = err.error.errors;
          this.concatenarErrores(this.errorsBack);

          if (err.status == 400) {
            this.showDialog("Error al crear contenido precargado");
          }

          if (err.status == 422) {
            this.closeDialog();
            this.showDialog("Error: "+this.errorsDialog);
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados");
          }
        });
    }
  }

  editarPrecargada() {
    this.precargadaEditada.contenido = this.value;
    if (this.textoValido) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Editando el contenido precargado, por favor espere...");

      this.precargadaService.editPrecargada(this.precargadaEditada, this.precargadaId).subscribe((data: any) => {

        this.showDialog("Contenido precargado editado correctamente");
        this.goToPrecargadas();
      },
        err => {
          this.errorsBack = err.error.errors;
          this.concatenarErrores(this.errorsBack);

          if (err.status == 400) {
            this.showDialog("Error al editar contenido precargado");
          }

          if (err.status == 422) {
            this.closeDialog();
            this.showDialog("Error: "+this.errorsDialog);
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados");
          }
        });
    }
  }

  concatenarErrores(errorsBack: any) {
    if (errorsBack.contenido){
      this.errorsDialog += errorsBack.fecha;
      this.errorsDialog += " "
    }
    if (errorsBack.tipo){
      this.errorsDialog += errorsBack.tipo;
      this.errorsDialog += " "
    }
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }
}
