import { Component, OnInit } from '@angular/core';
import { PrecargadaService } from 'src/app/services/precargada/precargada.service';
import { Precargada } from 'src/app/models/precargada';
import { errorMessages } from 'src/app/validators/customValidators';
import { Router, ActivatedRoute} from '@angular/router';
import { MatDialog } from '@angular/material';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { PrecargadaConst } from 'src/app/Constants/PrecargadaConst';

@Component({
  selector: 'app-item-precargada',
  templateUrl: './item-precargada.component.html',
  styleUrls: ['./item-precargada.component.scss']
})
export class ItemPrecargadaComponent implements OnInit {

  tipo: string = PrecargadaConst.item;
  itemsPrecargados: Precargada[];
  errors = errorMessages; //To bind errors messages into html
  editState: boolean = false;
  itemRealId: string;
  estaSeleccionando: boolean = false;
  textoFiltro: string = "";

  urlPrevia: string;

  constructor(
    private precargadaService: PrecargadaService,
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getPrecargados();
    this.getUrlPrevia();
    this.itemRealId = this.actRoute.snapshot.paramMap.get('id');
    if (this.itemRealId != null)
      this.estaSeleccionando = true;
  }

  getPrecargados() {
    this.precargadaService.getAllItemsPrecargados().subscribe((data: any) => {
      this.itemsPrecargados = data;
    });
  }

  getUrlPrevia() {
    this.precargadaService.getUrlPrevia().subscribe((value) => {
      this.urlPrevia = value;
    });
  }

  seleccionarPrecargada(contenido: string) {
    this.precargadaService.setContenidoPrecargado(contenido);
      this.router.navigate([this.urlPrevia]);
  }

  createPrecargada() {
    this.router.navigate([LinkConst.urlPrecargadaCrear + "/" + this.tipo]);
  }

  editPrecargada(id: string) {
    this.router.navigate([LinkConst.urlPrecargadaCrear + "/" + this.tipo + "/" + id]);
  }

  confirmarEliminarPrecargada(precargadaId: string) {
    this.showDialogYesNo("¿Desea eliminar el contenido precargado?", precargadaId);
  }

  showDialogYesNo(title: string, precargadaId: string): any {
    let dialogRef = this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deletePrecargada(precargadaId);
    });
  }

  deletePrecargada(precargadaId: string) {
    this.precargadaService.deletePrecargada(precargadaId).subscribe((data: any) => {
      this.getPrecargados();
      this.showDialog("El item precargado ha sido eliminado con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar el item precargado.");
        }
      }
    );
  }

  volverInicio() {
    this.router.navigate([LinkConst.url]);
  }

  volverItem() {
    if (this.itemRealId != "0")
    this.router.navigate([LinkConst.urlItem + "/" + this.itemRealId]);
  else
    this.router.navigate([this.urlPrevia]);  // estas estan mal
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}


