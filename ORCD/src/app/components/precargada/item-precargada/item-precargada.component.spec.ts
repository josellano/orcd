import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemPrecargadaComponent } from './item-precargada.component';

describe('ItemPrecargadaComponent', () => {
  let component: ItemPrecargadaComponent;
  let fixture: ComponentFixture<ItemPrecargadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemPrecargadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemPrecargadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
