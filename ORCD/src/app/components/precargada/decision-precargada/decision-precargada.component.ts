import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Precargada } from 'src/app/models/precargada';
import { errorMessages } from 'src/app/validators/customValidators';
import { PrecargadaService } from 'src/app/services/precargada/precargada.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { PrecargadaConst } from 'src/app/Constants/PrecargadaConst';

@Component({
  selector: 'app-decision-precargada',
  templateUrl: './decision-precargada.component.html',
  styleUrls: ['./decision-precargada.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class DecisionPrecargadaComponent implements OnInit {

  decisionesPrecargadas: Precargada[];
  errors = errorMessages; //To bind errors messages into html
  tipo: string = PrecargadaConst.decision;
  editState: boolean = false;

  textoFiltro: string="";
  decisionRealId: string;
  estaSeleccionando: boolean= false;

  constructor(
    private precargadaService: PrecargadaService,
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog) {

  }

  ngOnInit() {
    this.getPrecargados();
    this.decisionRealId = this.actRoute.snapshot.paramMap.get('id');
    if(this.decisionRealId)
      this.estaSeleccionando=true;

  }

  getPrecargados() {
    this.precargadaService.getAllDecisionesPrecargadas().subscribe((data: any) => {
      this.decisionesPrecargadas = data;
    });
  }

  seleccionarPrecargada(contenido: string){
    this.precargadaService.setContenidoPrecargado(contenido);
    this.router.navigate([LinkConst.urlDecision+ "/" +this.decisionRealId]);
  }

  createPrecargada() {
    this.router.navigate([LinkConst.urlPrecargadaCrear+ "/" +this.tipo]);
  }

  editPrecargada(id: string) {
    this.router.navigate([LinkConst.urlPrecargadaCrear + "/" +this.tipo +"/"+ id]);
  }

  confirmarEliminarPrecargada(precargadaId: string) {
    this.showDialogYesNo("¿Desea eliminar el contenido precargado?", precargadaId);
  }

  showDialogYesNo(title: string, precargadaId: string): any {
    let dialogRef = this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deletePrecargada(precargadaId);
    });
  }

  deletePrecargada(precargadaId : string) {
    this.precargadaService.deletePrecargada(precargadaId).subscribe((data: any) => {
      this.getPrecargados();
      this.showDialog("La decision precargada ha sido eliminada con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar la decision precargada.");
        }
      }
    );
  }

  volverInicio() {
    this.router.navigate([LinkConst.url]);
  }

  volverDecision() {
    this.router.navigate([LinkConst.urlDecision+"/"+this.decisionRealId]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}
