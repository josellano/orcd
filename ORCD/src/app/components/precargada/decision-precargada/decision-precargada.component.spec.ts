import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecisionPrecargadaComponent } from './decision-precargada.component';

describe('DecisionPrecargadaComponent', () => {
  let component: DecisionPrecargadaComponent;
  let fixture: ComponentFixture<DecisionPrecargadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecisionPrecargadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecisionPrecargadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
