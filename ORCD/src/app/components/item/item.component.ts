import { Component, OnInit, ViewChild, SecurityContext } from '@angular/core';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, TableService, RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ItemConst } from 'src/app/Constants/ItemConst';
import { ItemService } from 'src/app/services/item/item.service';
import { MatDialog } from '@angular/material';
import { Item } from 'src/app/models/item';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { PrecargadaService } from 'src/app/services/precargada/precargada.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html', providers: [ToolbarService, LinkService, ImageService, HtmlEditorService, TableService],
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  ordenId: string;
  itemId: string;
  textoValido: boolean = false;
  cargoItem: boolean = false;
  habilitarRTE: boolean= true;

  mostrarCargarAdjunto: boolean = false; //usada para mostrar el div de agregar adjunto si estoy editando y no hay adjunto
  mostrarEliminarAdjunto: boolean = false; //usada para mostrar el div de eliminar adjunto si estoy editando y hay adjunto
  seleccionoArchivo: boolean = false; //usada para habiltiar el boton de agregar adjunto una vez que seleccionan archivo

  tipoSeleccionado: string;

  itemComun: string = ItemConst.itemComun;
  itemSobretabla: string = ItemConst.itemSobretabla;

  title: string = "Buscando ítem";
  itemEditado: Item;
  editState: boolean = false;

  contenidoPrecargado: string;
  urlPrevia: string;
  urlActual: string;

   //errores
   errorsDialog: any= [];
   errorsBack: any = [];

  @ViewChild('fromRTE')
  private rteEle: RichTextEditorComponent;
  public value: string = null;
  rteCreated(): void {
    this.rteEle.element.focus();
  }

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable', '|',
      'ClearFormat', '|', 'FullScreen']
  };

  //manejo archivos
  filedata: File;
  fileEvent(e){
    this.filedata = e.target.files[0];
    this.seleccionoArchivo=true;
  }

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog,
    private itemService: ItemService,
    private precargadaService: PrecargadaService,
    private sanitized: DomSanitizer
  ) {

    router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        this.urlPrevia = event.url;
      }
    });
  }

  ngOnInit() {
    this.seleccionoArchivo=false;

    this.ordenId = this.actRoute.snapshot.paramMap.get('ordenid');
    this.itemId = this.actRoute.snapshot.paramMap.get('itemid');
    this.tipoSeleccionado = this.actRoute.snapshot.paramMap.get('tipo');
    this.obtenerPrecargado(); // para obtener si es que lo hay contenido precargado seleccionado
    this.mostrarCargarAdjunto=true;

    if (this.tipoSeleccionado != null) {
      this.cargoItem = true;
      this.title = "Creación de un ítem";
      if (this.contenidoPrecargado != "") { //verifica si hay contenido precargado
        this.value = this.contenidoPrecargado;
        this.precargadaService.setContenidoPrecargado("");
      }
    }
    else {
      this.title = "Edición de un ítem";
      setTimeout(() => {  // para evitar mostrar error por changeDetection DOM desfasada
        this.value = "Buscando contenido del item";
      });
      this.editState = true;
      this.obtenerItem(this.itemId);
    }
  }

  obtenerPrecargado() {
    this.precargadaService.getContenidoPrecargado().subscribe((value) => {
      this.contenidoPrecargado = value;
    });
  }

  seleccionarPrecargada() {
    if (this.editState)
      this.router.navigate([LinkConst.urlPrecargadaItem + "/" + this.itemEditado.id]);
    else
      this.router.navigate([LinkConst.urlPrecargadaItem + "/" + 0]);

    this.precargadaService.setUrlPrevia(this.urlPrevia);
  }

  async obtenerItem(id: string) {
    await this.itemService.getItemByIDPromise(id).then((data: any) => {
      this.itemEditado = data;
      this.ordenId = data.ordendia_id;
      this.cargoItem = true;
    });
    this.title = "Edición del ítem " + this.itemEditado.numero;

    //verifico si tiene adjunto
    if(this.itemEditado.adjunto){
      this.mostrarEliminarAdjunto=true;
      this.mostrarCargarAdjunto=false;
    }
    else
      this.mostrarCargarAdjunto=true;

    // si se selecciono un contenido precargado hay que setear ese valor y no el obtenido.
    if (this.contenidoPrecargado == "")
      this.value = this.itemEditado.contenido;
    else {
      this.value = this.contenidoPrecargado;

      this.precargadaService.setContenidoPrecargado("");
    }
  }

  //si editan el contenido del item antes de agregar el item, el boton agregar se vuelve a deshabilitar
  textoModificado(e){
    this.textoValido = false;
  }

  validarTexto() {
    this.habilitarRTE=false;
    this.value = this.sanitized.sanitize(SecurityContext.NONE, this.value);
    const errorSms = '<p><span style="color: rgb(255, 0, 0); text-decoration: inherit;">Por favor ingrese el contenido del Item.</span></p>';
    if (this.value == null || this.value == errorSms) {
      this.value = errorSms;
      this.textoValido = false;
      return false
    }
    else {
      this.textoValido = true;
      return true;
    }
  }

  //habiltiar el RTE na vez que ya se habia confirmado el texto.
  cambiarTexto() {
    this.habilitarRTE=true;
    this.textoValido=false;
  }

  limpiarTexto() {
    this.value = null;
  }

  goToOrdendia() {
    this.router.navigate([LinkConst.urlOrdendia + "/" + this.ordenId]);
  }

  goToItem() {
    this.router.navigate([LinkConst.urlItem + "/" + this.itemId]);
  }

  guardarItem() {
    if (this.textoValido) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Creando el item, por favor espere...");
      this.itemService.postNewItem(new Item(this.tipoSeleccionado, this.value, Number(this.ordenId)), this.ordenId, this.tipoSeleccionado, this.filedata).subscribe((data: any) => {

        this.closeDialog();
        this.showDialog("Ítem creado correctamente.");
        this.goToOrdendia();
      },
        err => {
          this.errorsBack = err.error.errors;
          this.concatenarErrores(this.errorsBack);

          if (err.status == 400) {
            this.showDialog("Error al crear el ítem.");
          }

          if (err.status == 422) {
            this.closeDialog();
            this.showDialog("Error: "+this.errorsDialog);
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados.");
          }
        });

        //this.showDialog("Creando el ítem, por favor espere.");
    }
  }

  editarItem() {
    var auxItem = new Item(this.itemEditado.tipo, this.value, this.itemEditado.ordendia_id);
    auxItem.numero = this.itemEditado.numero;
    auxItem.ordendia_id = this.itemEditado.ordendia_id;

    if (this.textoValido) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Editando el item, por favor espere...");

      this.itemService.editItem(auxItem, this.itemId).subscribe((data: any) => {

        this.closeDialog();
        this.showDialog("Item editado correctamente.");
        this.goToOrdendia();
      },
        err => {
          this.errorsBack = err.error.errors;
          this.concatenarErrores(this.errorsBack);

          if (err.status == 400) {
            this.showDialog("Error al editar el ítem.");
          }

          if (err.status == 422) {
            this.closeDialog();
            this.showDialog("Error: "+this.errorsDialog);
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados.");
            this.closeDialog();
          }
        });

       // this.showDialog("Guardando el ítem, por favor espere.");
    }
  }

  eliminarAdjunto() {
      this.itemService.deleteAdjunto(this.itemId).subscribe((data: any) => {

        this.closeDialog();
        this.showDialog("Adjunto eliminado correctamente.");
        this.goToItem();
        this.mostrarEliminarAdjunto= false;
        this.mostrarCargarAdjunto=true;
      },
        err => {
          const errorMessages = new Array<{ propName: string; errors: string }>();

          if (err.status == 400) {
            this.showDialog("Error al eliminar el adjunto.");
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados.");
          }
        });

        this.showDialog("Eliminando el adjunto, por favor espere.");
  }

  agregarAdjunto() {

    this.itemService.agregarAdjunto(this.itemId,this.filedata).subscribe((data: any) => {

      this.closeDialog();
      this.showDialog("Adjunto agregado correctamente.");
      this.goToItem();
      this.mostrarEliminarAdjunto= true;
      this.mostrarCargarAdjunto=false;
    },
      err => {

        if (err.status == 400) {
          this.showDialog("Error al agregado el adjunto.");
        }

        if (err.status == 500) {
          this.showDialog("Error, Verifique los datos ingresados.");
        }
      });

      this.showDialog("Agregando el adjunto, por favor espere.");
}

concatenarErrores(errorsBack: any) {
  if (errorsBack.contenido){
    this.errorsDialog += errorsBack.fecha;
    this.errorsDialog += " "
  }
  if (errorsBack.tipo){
    this.errorsDialog += errorsBack.tipo;
    this.errorsDialog += " "
  }
}

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }
}
