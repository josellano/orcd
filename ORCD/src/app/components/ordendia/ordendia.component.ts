import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Ordendia } from 'src/app/models/ordendia';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { StorageService } from 'src/app/services/localStorage/storage.service';
import { RoleConst } from 'src/app/Constants/RoleConst';
import { Item } from 'src/app/models/item';
import { ItemService } from 'src/app/services/item/item.service';
import { ItemConst } from 'src/app/Constants/ItemConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { Reunion } from 'src/app/models/reunion';
import { ReunionService } from 'src/app/services/reunion/reunion.service';
import { Traductor } from 'src/app/models/traductor';

@Component({
  selector: 'app-ordendia',
  templateUrl: './ordendia.component.html',
  styleUrls: ['./ordendia.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OrdendiaComponent implements OnInit {
  reunionId: string;
  reunion: Reunion;
  orden_id: string;
  esAdmin: boolean = false;
  esSecretario: boolean = false;

  itemsComunes: Item[];
  itemsComunesLength: boolean;
  itemsComunesMuestra: Item[] = new Array();
  itemsSobretabla: Item[];
  itemsComunicacion: Item[];

  titulo: string = "Orden del día";
  introduccion: string;
  separadorSobretabla: string = "Items Sobretabla";
  separadorComunicaciones: string = "Comunicaciones ingresadas";
  smsNohayItemsComunes: string = "No hay ítems Comunes";
  smsNohayItemsSobretabla: string = "No hay ítems Sobretabla";
  orden: Ordendia;

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog,
    private storageService: StorageService,
    private itemService: ItemService,
    private reunionService: ReunionService
  ) { }

  async ngOnInit() {

    this.mostrarOperacionesAdmin();
    this.orden_id = this.actRoute.snapshot.paramMap.get('id');
    this.getItems();

    await this.getOrdendia();
  }

  async getOrdendia() {
    await this.reunionService.getOrdendiaPromise(this.orden_id).then((data: any) => {
      this.orden = data;
    });
    this.getReunion();
  }

  async getReunion() {
    await this.reunionService.getReunionByIDPromise(this.orden.reunion_id).then((data: any) => {
      this.reunion = data;
    });
    this.armarHTML();
  }

  armarHTML() {
    this.armarIntroduccion();
    this.separadorSobretabla = "Items Sobretabla";
    this.separadorComunicaciones = "Comunicaciones ingresadas";
  }

  private armarIntroduccion() {
    var date = new Date(this.reunion.fecha);
    var mesTraducido = new Traductor();
    var dia = this.agregarDigito(date.getDate());
    var hora = this.agregarDigito(date.getHours());
    var minuto = this.agregarDigito(date.getMinutes());
    this.introduccion = "Reunión <b>" + this.reunion.tipo + "</b> de Consejo Departamental, del día <b>" + dia + "</b> de <b>" + mesTraducido.getNombreMes(date.getMonth()) + "</b> de <b>" + date.getFullYear() + "</b> a las <b>" + hora + ":" + minuto + " horas.</b>";

  }

  private agregarDigito(numero: Number) {
    var numeroString = '';
    if (numero < 10)
      numeroString = '0' + numero;
    else
      numeroString = '' + numero;
    return numeroString;
  }

  getItems() {
    this.getItemsComun();
    this.getItemsSobretabla();
    this.getItemsComunicacion();
  }

  getItemsComun() {
    this.itemService.getItemsByType(this.orden_id, ItemConst.itemComun).subscribe((data: any) => {
      this.itemsComunes = data;
    });
  }

  getItemsSobretabla() {
    this.itemService.getItemsByType(this.orden_id, ItemConst.itemSobretabla).subscribe((data: any) => {
      this.itemsSobretabla = data;
    });
  }

  getItemsComunicacion() {
    this.itemService.getItemsByType(this.orden_id, ItemConst.itemComunicacion).subscribe((data: any) => {
      this.itemsComunicacion = data;
    });
  }

  editItem(itemId: string) {
    this.router.navigate([LinkConst.urlItem + "/" + itemId]);
  }

  confirmarEliminarItem(item: Item) {

    this.showDialogYesNo("¿Desea eliminar el item "+item.numero+"?", item.id.toString());
  }

  showDialogYesNo(title: string, itemId: string): any {
    let dialogRef = this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteItem(itemId);
    });
  }

  deleteItem(itemId: string) {
    this.itemService.deleteItem(itemId).subscribe((data: any) => {
      this.getItems();
      this.showDialog("El item ha sido eliminado con exito");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar el item");
        }
      }
    );
  }

  mostrarOperacionesAdmin() {
    if (this.storageService.getRoles().includes(RoleConst.rolAdmin)) {
      this.esAdmin = true;
    }
    if (this.storageService.getRoles().includes(RoleConst.rolSecretarioAcademico)) {
      this.esSecretario = true;
    }
  }

  aumentarItemNumero(itemId: string, tipo: string) {
    this.itemService.upItemNumero(itemId).subscribe((data: any) => {

      if (tipo == ItemConst.itemComun) {
        this.getItemsComun();
      }
      else {
        if (tipo == ItemConst.itemSobretabla)
          this.getItemsSobretabla();
        else
          this.getItemsComunicacion();

      }
      this.showDialog("El numero se ha actualizado con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer aumentar el numero.");
        }
      }
    );
  }

  decrementarItemNumero(itemId: string, tipo: string) {
    this.itemService.downItemNumero(itemId).subscribe((data: any) => {

      if (tipo == ItemConst.itemComun) {
        this.getItemsComun();
      }
      else {
        if (tipo == ItemConst.itemSobretabla)
          this.getItemsSobretabla();
        else
          this.getItemsComunicacion();
      }
      this.showDialog("El numero se ha actualizado con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer aumentar el numero.");
        }
      }
    );
  }

  verAdjunto(item_id: string){
    this.router.navigate([LinkConst.urlAdjuntoPDF + "/" + item_id]);
  }

  goToAgregarItemComun() {
    this.router.navigate([LinkConst.urlItem + "/" + this.orden_id + "/" + ItemConst.itemComun]);
  }

  verDecision(item_id: string) {
    this.router.navigate([LinkConst.urlDecision + "/" + item_id]);
  }

  showOrdendiaPDF() {
    this.router.navigate([LinkConst.urlOrdendiaPDF + "/" + this.orden_id]);
  }

  /*goToAgregarItemSobretabla() { no va mas porque el back determina el tipo ahora.
    this.router.navigate([LinkConst.urlItem + "/" + this.orden_id + "/" + ItemConst.itemSobretabla]);
  }*/

  goToAgregarItemComunicacion() {
    this.router.navigate([LinkConst.urlItem + "/" + this.orden_id + "/" + ItemConst.itemComunicacion]);
  }

  volverReunion() {
    this.router.navigate([LinkConst.urlReunion]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}
