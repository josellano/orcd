import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdendiaComponent } from './ordendia.component';

describe('OrdendiaComponent', () => {
  let component: OrdendiaComponent;
  let fixture: ComponentFixture<OrdendiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdendiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdendiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
