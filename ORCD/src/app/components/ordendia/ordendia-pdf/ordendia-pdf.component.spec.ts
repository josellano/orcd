import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdendiaPDFComponent } from './ordendia-pdf.component';

describe('OrdendiaPDFComponent', () => {
  let component: OrdendiaPDFComponent;
  let fixture: ComponentFixture<OrdendiaPDFComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdendiaPDFComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdendiaPDFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
