import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APIConst } from 'src/app/Constants/APIConst';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { Reunion } from 'src/app/models/reunion';
import { ReunionService } from 'src/app/services/reunion/reunion.service';

@Component({
  selector: 'app-ordendia-pdf',
  templateUrl: './ordendia-pdf.component.html',
  styleUrls: ['./ordendia-pdf.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OrdendiaPDFComponent implements OnInit {

  orden_id: string;
  OrdenPDF: any;
  pdfSrc = "";
  tituloPDF: string ="orden"

  reunion: Reunion;

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private reunionService: ReunionService
  ) { }

  async ngOnInit() {
    this.orden_id = this.actRoute.snapshot.paramMap.get('id');
    await this.obtenerReunion(this.orden_id);
    this.cargarPDF();
  }

  cargarPDF() {
    this.pdfSrc = APIConst.urlOrdendiaPDF + this.orden_id;
  }

  async obtenerReunion(id: string) {
    await this.reunionService.getReunionByOrdendiaIDPromise(id).then((data: any) => {
      this.reunion = data;

      var date = new Date(this.reunion.fecha);
      var anio = date.getFullYear().toString();
      var anioDosDigitos = anio.substr(2);

      this.tituloPDF+= " "+this.reunion.numero+"-"+anioDosDigitos;
    });
  }

  volverOrdendia() {
    this.router.navigate([LinkConst.urlOrdendia+"/"+this.orden_id]);
  }
}
