import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ApiServiceLoginService } from 'src/app/services/login/api-service-login.service';
import { Router } from '@angular/router';
import { errorMessages } from 'src/app/validators/customValidators';
import { StorageService } from 'src/app/services/localStorage/storage.service';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { LinkConst } from 'src/app/Constants/LinkConst';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errors = errorMessages; //To bind errors messages into html
  roles: any;
  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private apiServiceLogin: ApiServiceLoginService,
    private router: Router,
    private localStorageService: StorageService
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  login() {
    if (this.loginForm.valid) {
      this.apiServiceLogin.login(this.loginForm.get("email").value, this.loginForm.get("password").value).subscribe((data: any) => {
        this.localStorageService.login(data);
        this.router.navigate(['/']);
      },
        err => {
          if (err.status == 401) {
            this.showDialog("El usuario y/o contraseña es inválido");
          }
        });
    }
  }

  goToResetPass(){
    this.router.navigate([LinkConst.urlPassword]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}

