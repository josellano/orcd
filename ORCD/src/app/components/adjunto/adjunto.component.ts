import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { APIConst } from 'src/app/Constants/APIConst';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { ItemService } from 'src/app/services/item/item.service';
import { Item } from 'src/app/models/item';

@Component({
  selector: 'app-adjunto',
  templateUrl: './adjunto.component.html',
  styleUrls: ['./adjunto.component.scss']
})
export class AdjuntoComponent implements OnInit {

  orden_id: string;
  item_id: string;
  item: Item;
  OrdenPDF: any;
  pdfSrc = "";
  tituloPDF: string ="Adjunto"
  cargoItem: boolean = false;

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private itemService: ItemService
  ) { }

  async ngOnInit() {
    this.item_id = this.actRoute.snapshot.paramMap.get('id');
    this.cargarPDF();
    this.obtenerItem(this.item_id);
  }

  cargarPDF() {
    this.pdfSrc = APIConst.urlAdjuntoPDF + this.item_id;
  }

  obtenerItem(id: string) {
    this.itemService.getItemByIDPromiseSecretario(id).then((data: any) => {
      this.item = data;
      this.orden_id = data.ordendia_id;
      this.cargoItem = true;
      this.tituloPDF = data.adjunto;
    });

  }

  volverOrdendia() {
    this.router.navigate([LinkConst.urlOrdendia+"/"+this.orden_id]);
  }

}
