import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { errorMessages, CustomValidators } from 'src/app/validators/customValidators';
import { User } from 'src/app/models/user';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleConst } from 'src/app/Constants/RoleConst';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { UserConst } from 'src/app/Constants/Userconst';
import { Rol } from 'src/app/models/rol';
import { ComparadorRolesService } from 'src/app/services/comparador-roles/comparador-roles.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  roles: Rol[] = new Array();

  registerForm: FormGroup;
  errors = errorMessages; //To bind errors messages into html
  errorsDialog: any= [];
  errorsBack: any = [];

  minLengthUser = 6;
  textPattern = '[a-zA-Z ñ á-ú Á-Ú]*';
  telefonoPattern = '[0-9]*';

  user: User;
  username: string;
  id: string;
  rol: string;
  editState: boolean;
  title: string = "Crear Usuario";
  list: string;

  //For dropdown roles menu
  currentChoiceList: string[] = Array<string>();
  myChoice: string = RoleConst.rolPublico;

  //Radiobuttons roles
  masterSelected: boolean = false;
  listaRolesCheckList: Array<{ id: number, value: string, isSelected: boolean }> = new Array();

  listaRoles: string[] = [];
  listaClaustro: string[] = [];

  rolesSeleccionados: string[];

  rolesUserEditado: Rol[];

  //For dropdown puestos menu
  listaPuestos: string[] = Array<string>();
  puestoElegido: string = UserConst.puestoSuplenteString;
  puestoElegidoNumerico: string = UserConst.puestoSuplente;

  //For dropdown generos menu
  listaGeneros: string[] = Array<string>();
  generoElegido: string = 'Seleccione un Genero';
  generoElegidoNumerico: string;

  //For dropdown titulo academico menu
  listaTitulos: string[] = Array<string>();
  tituloElegido: string = "Seleccione un Titulo Academico";



  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private actRoute: ActivatedRoute,
    private comparadorRolesService: ComparadorRolesService,
    private apiUsuarioService: UsuarioService
  ) {
    this.createForm(); //Intialize controls
  }

  async ngOnInit() {
    this.cargarDropdownMenues();

    this.id = this.actRoute.snapshot.paramMap.get('id');
    if (this.id) {
      this.editState = true;

      this.title = "Actualizar Usuario";

      await this.obtenerUser(this.id);
      this.populateForm(this.user);
      await this.obtenerRolesUser(this.id);
    }
  }

  cargarDropdownMenues() {
    this.puestosForUser();
    this.generosForUser();
    this.titulosForUser();
  }

  async obtenerUser(id: string) {
    await this.apiUsuarioService.getUserByIDPromise(id).then((data: any) => {
      this.user = data;
    });
  }

  async obtenerRolesUser(id: string) {
    await this.apiUsuarioService.getRolesUserPromise(id).then((data: any) => {
      this.rolesUserEditado = data;
      this.initializeRadioButtonsRoles();
    });
  }

  createForm() {
    this.registerForm = this.formBuilder.group({

      nombre: ['', [
        Validators.required,
        Validators.pattern(this.textPattern)
      ]],
      apellido: ['', [
        Validators.required,
        Validators.pattern(this.textPattern)
      ]],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      telefono: ['', [
        Validators.pattern(this.telefonoPattern),
        Validators.required,
        Validators.minLength(this.minLengthUser)
      ]],
      passwordGroup: this.formBuilder.group({
        password: ['', [
          Validators.required
        ]],
        repeatPassword: ['', Validators.required]
      }, { validator: CustomValidators.childrenEqual })
    });
  }

  populateForm(user: User) {
    this.registerForm = this.formBuilder.group({

      nombre: [user.nombre, [
        Validators.required,
        Validators.pattern(this.textPattern)
      ]],
      apellido: [user.apellido, [
        Validators.pattern(this.textPattern),
        Validators.required
      ]],
      email: [user.email, [
        Validators.required,
        Validators.email
      ]],
      telefono: [user.telefono, [
        Validators.pattern(this.telefonoPattern),
        Validators.required,
        Validators.minLength(this.minLengthUser)
      ]],
      titulo_Academico: [user.titulo_academico, [
        Validators.required
      ]],
      passwordGroup: this.formBuilder.group({
        password: [user.password, [
          Validators.required
        ]],
        repeatPassword: [user.password, Validators.required]
      }, { validator: CustomValidators.childrenEqual })
    });

    this.puestoElegido = this.listaPuestos[Number(user.puesto)]
    this.puestoElegidoNumerico = user.puesto;

    this.generoElegido = this.listaGeneros[Number(user.genero)]
    this.generoElegidoNumerico = user.genero;

    this.tituloElegido = user.titulo_academico;
  }

  puestosForUser() {
    this.listaPuestos.push(UserConst.puestoSuplenteString);
    this.listaPuestos.push(UserConst.puestoTitularString);
  }

  generosForUser() {
    this.listaGeneros.push(UserConst.generoFemeninoString);
    this.listaGeneros.push(UserConst.generoMasculinoString);
  }

  titulosForUser() {
    this.listaTitulos.push(UserConst.titulo_academico_dr);
    this.listaTitulos.push(UserConst.titulo_academico_dra);
    this.listaTitulos.push(UserConst.titulo_academico_mg);
    this.listaTitulos.push(UserConst.titulo_academico_ing);
    this.listaTitulos.push(UserConst.titulo_academico_lic);
    this.listaTitulos.push(UserConst.titulo_academico_sr);
    this.listaTitulos.push(UserConst.titulo_academico_sra);
  }

  puestoSeleccionado(puesto: string) {
    this.puestoElegido = puesto;
    this.puestoElegidoNumerico = this.listaPuestos.indexOf(puesto).toString();
  }

  generoSeleccionado(genero: string) {
    this.generoElegido = genero;
    this.generoElegidoNumerico = this.listaGeneros.indexOf(genero).toString();
  }

  tituloSeleccionado(titulo: string) {
    this.tituloElegido = titulo;
  }

  register() {

    if (!this.registerForm.get("passwordGroup").valid) {
      this.showDialog("Las contraseñas no coinciden");
    }
    if (this.registerForm.valid && this.registerForm.get("passwordGroup").valid) {

      this.errorsDialog = []; // limpio los errores del dialogo de error

      this.apiUsuarioService.postNewUser(new User(
        this.registerForm.get("nombre").value,
        this.registerForm.get("apellido").value,
        this.registerForm.get("email").value,
        this.registerForm.get("telefono").value,
        this.registerForm.get("passwordGroup").get("password").value,
        [RoleConst.rolPublico], this.puestoElegidoNumerico,
        this.tituloElegido,
        this.generoElegidoNumerico,
      )).subscribe((data: any) => {

        this.showDialog("Usuario creado correctamente");
        this.goToListaUsers();
      },
        err => {
          const errorMessages = new Array<{ propName: string; errors: string }>();
          this.errorsBack = err.error.errors;
          this.concatenarErrores(this.errorsBack);

          if (err.status == 400) {
            this.showDialog("Error al crear el usuario");
          }

          if (err.status == 422) {
            this.showDialog("Error: "+this.errorsDialog);
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados");
          }
        });
    }
  }

  concatenarErrores(errorsBack: any) {
    if (errorsBack.nombre){
      this.errorsDialog += errorsBack.nombre;
      this.errorsDialog += " "
    }
    if (errorsBack.apellido){
      this.errorsDialog += errorsBack.apellido;
      this.errorsDialog += " "
    }
    if (errorsBack.email){
      this.errorsDialog += errorsBack.email;
      this.errorsDialog += " "
    }
    if (errorsBack.telefono){
      this.errorsDialog += errorsBack.telefono;
      this.errorsDialog += " "
    }
    if (errorsBack.puesto){
      this.errorsDialog += errorsBack.puesto;
      this.errorsDialog += " "
    }
    if (errorsBack.genero){
      this.errorsDialog += errorsBack.genero;
      this.errorsDialog += " "
    }
    if (errorsBack.titulo_academico){
      this.errorsDialog += errorsBack.titulo_academico;
      this.errorsDialog += " "
    }
    if (errorsBack.password){
      this.errorsDialog += errorsBack.password;
      this.errorsDialog += " "
    }
  }


  update() {
    this.getCheckedlistaRoles();
    if (this.rolesSeleccionados.length) {

      if (!this.registerForm.get("passwordGroup").valid) {
        this.showDialog("Las contraseñas no coinciden");
      }

      if (this.registerForm.valid) {
        this.errorsDialog = []; // limpio los errores del dialogo de error

        this.apiUsuarioService.editUser(new User(
          this.registerForm.get("nombre").value,
          this.registerForm.get("apellido").value,
          this.registerForm.get("email").value,
          this.registerForm.get("telefono").value,
          this.registerForm.get("passwordGroup").get("password").value,
          this.rolesSeleccionados, this.puestoElegidoNumerico,
          this.tituloElegido,
          this.generoElegidoNumerico,
        ), this.id).subscribe((data: any) => {

          this.showDialog("Usuario actualizado correctamente");
          this.goToListaUsers();
        },
          err => {
            const errorMessages = new Array<{ propName: string; errors: string }>();
            this.errorsBack = err.error.errors;
            this.concatenarErrores(err.error.errors);

            if (err.status == 400) {
              this.showDialog("Error al actualizar el usuario");
            }

            if (err.status == 422) {
               this.showDialog("Error: "+this.errorsDialog);
            }

            if (err.status == 500) {
              this.showDialog("Error, Verifique los datos ingresados");
            }
          });
      }
    }
    else{
      this.showDialog("Debes seleccionar al menos un rol para el usuario.");
    }
  }


  goToListaUsers() {
    this.router.navigate([LinkConst.urlAdmin]);
  }

  goToRegister() {
    this.router.navigate([LinkConst.urlRegister]);
  }

  goToLogin() {
    this.router.navigate([LinkConst.urlLogin]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  login() {
    //this.router.navigate([LinkConst.urlSecurityAdmin]);
  }

  volverListado() {
    this.router.navigate([this.list]);
  }

  cancel() {
    if(this.comparadorRolesService.esAdmin())
      this.router.navigate([LinkConst.urlAdmin]);
    else
      this.router.navigate([LinkConst.url]);
  }

  // inicio Dropdown de roles User

  // Inicializa el dropdown de roles
  initializeRadioButtonsRoles() {
    this.rolesForUser();

    for (var i = 0; i < this.listaRoles.length; i++) {
      this.listaRolesCheckList[i] = { id: i, value: this.listaClaustro[i], isSelected: false };
    }

    //si estoy editando debo agregar los roles actuales del usuario en cuestion
    if (this.editState) {
      for (let rolUser of this.rolesUserEditado) {
        for (var i = 0; i < this.listaRolesCheckList.length; i++) {
            if (this.listaRoles[i] == rolUser.name)
            this.listaRolesCheckList[i].isSelected = true;
        }
      }
    }
  }

  rolesForUser() {
    this.listaRoles[0] = RoleConst.rolAdmin;
    this.listaRoles[1] = RoleConst.rolDecano;
    this.listaRoles[2] = RoleConst.rolVicedecano;
    this.listaRoles[3] = RoleConst.rolSecretarioAcademico;
    this.listaRoles[4] = RoleConst.rolProfesor;
    this.listaRoles[5] = RoleConst.rolAuxiliar;
    this.listaRoles[6] = RoleConst.rolAlumno;
    this.listaRoles[7] = RoleConst.rolPublico;

    this.listaClaustro[0] = RoleConst.claustroAdmin;
    this.listaClaustro[1] = RoleConst.claustroDecano;
    this.listaClaustro[2] = RoleConst.claustroVicedecano;
    this.listaClaustro[3] = RoleConst.claustroSecretarioAcademico;
    this.listaClaustro[4] = RoleConst.claustroProfesor;
    this.listaClaustro[5] = RoleConst.claustroAuxiliar;
    this.listaClaustro[6] = RoleConst.claustroAlumno;
    this.listaClaustro[7] = RoleConst.claustroPublico;
  }

  getCheckedlistaRoles() {
    this.rolesSeleccionados = [];
    var indiceInsercion = 0;
    for (var i = 0; i < this.listaRolesCheckList.length; i++) {
      if (this.listaRolesCheckList[i].isSelected) {
        this.rolesSeleccionados[indiceInsercion] = this.listaRoles[i];
        indiceInsercion++;
      }
    }
  }

  // fin metodos para el dropdown de roles

  //validacioens input
  getEmailError(): String {
    var toRet: String;
    if (this.registerForm.get("email").hasError('required'))
      toRet = this.errors.required;
    else if (this.errorsBack.email == "Email ya esta en uso.")
      toRet = this.errors.uniqueEmail;
    else
      toRet = this.errors.email;
    return toRet;
  }

  getFieldLettersError(field: string): string {
    var toRet: string;
    if (this.registerForm.get(field).hasError('required'))
      toRet = this.errors.required;
    else if (this.registerForm.get(field).hasError('pattern'))
      toRet = this.errors.justLetters;
    else
      toRet = "Existe un error en el campo";
    return toRet;
  }

  getFieldNumbersError(field: string): string {
    var toRet: string;
    if (this.registerForm.get(field).hasError('required'))
      toRet = this.errors.required;
    else if (this.registerForm.get(field).hasError('pattern'))
      toRet = this.errors.justNumbers;
    else if (this.registerForm.get(field).hasError('minlength'))
      toRet = "El campo debe tener al menos " + this.minLengthUser + " digitos";
    else
      toRet = "Existe un error en el campo";
    return toRet;
  }
}
