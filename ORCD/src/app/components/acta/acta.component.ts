import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Reunion } from '../../models/reunion';
import { Router, ActivatedRoute } from '@angular/router';
import { ReunionService } from '../../services/reunion/reunion.service';
import { LinkConst } from '../../Constants/LinkConst';
import { APIConst } from 'src/app/Constants/APIConst';

@Component({
  selector: 'app-acta',
  templateUrl: './acta.component.html',
  styleUrls: ['./acta.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ActaComponent implements OnInit {

  orden_id: string;
  OrdenPDF: any;
  pdfSrc="";
  tituloPDF: string ="acta"
  reunion: Reunion;

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private reunionService: ReunionService
  ) { }

  async ngOnInit() {
    this.orden_id = this.actRoute.snapshot.paramMap.get('id');
    await this.obtenerReunion(this.orden_id);
    this.cargarPDF();
  }

  cargarPDF() {
    this.pdfSrc=APIConst.urlActaPDF+this.orden_id;
  }

  async obtenerReunion(id: string) {
    await this.reunionService.getReunionByOrdendiaIDPromise(id).then((data: any) => {
      this.reunion = data;

      var date = new Date(this.reunion.fecha);
      var anio = date.getFullYear().toString();
      var anioDosDigitos = anio.substr(2);

      this.tituloPDF+= " "+this.reunion.numero+"-"+anioDosDigitos;
    });
  }

  volverReunion() {
    this.router.navigate([LinkConst.urlReunion]);
  }

}
