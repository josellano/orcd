import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { SharedRolesService } from 'src/app/services/shared-roles/shared-roles.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  token: string;
  showImage: boolean = true;

  esAdmin: boolean = false;
  esSecretario: boolean = false;
  esMiembrodelConsejo: boolean = false;

  constructor(
    private router: Router,
    private sharedRoles: SharedRolesService
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] == '/') {
          this.armarHTML();
        }
      }
    });
  }

  ngOnInit() {
    this.armarHTML();
  }

  armarHTML() {
    this.sharedRoles.getEsAdmin().subscribe((value) => {
      this.esAdmin = value;
    });
    this.sharedRoles.getEsSecretario().subscribe((value) => {
      this.esSecretario = value;
    });
  }

  goToReunion() {
    this.router.navigate([LinkConst.urlReunion]);
  }

  goToUsuarios() {
    this.router.navigate([LinkConst.urlAdmin]);
  }

  goToItemsPrecargados() {
    this.router.navigate([LinkConst.urlPrecargadaItem]);
  }

  goToDecisionesPrecargadas() {
    this.router.navigate([LinkConst.urlPrecargadaDecision]);
  }
}
