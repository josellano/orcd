import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { errorMessages } from 'src/app/validators/customValidators';
import { Router } from '@angular/router';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: User[];
  errors = errorMessages; //To bind errors messages into html
  @ViewChild('f') myNgForm;
  editState: boolean = false;

  user: User;

  //Tabla responsiva
  displayedColumns = ["Nombre", "Apellido", "E-Mail", "Telefono", "Puesto", "TituloAcademico", "Acciones"];
  dataSource = new MatTableDataSource<User>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private ApiAccountService: UsuarioService,
    private router: Router,
    private dialog: MatDialog) {

  }

  ngOnInit() {

    this.getUsers();
  }

  getUsers() {
    this.ApiAccountService.getAllUsers().subscribe((data: any) => {
      this.users = data;
      this.dataSource = new MatTableDataSource<User>(this.users);
      this.dataSource.filterPredicate = (data: User, filter: string) => data.apellido.indexOf(filter) != -1; // configuro el filtro por apellido
      this.dataSource.paginator = this.paginator;
    });
  }

  addAdminView() {
    this.router.navigate([LinkConst.urlRegister]);
  }

  editUser(id: string) {
    this.router.navigate([LinkConst.urlRegister + "/" + id]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  confirmarEliminarUsuario(userId: string) {
    this.showDialogYesNo("¿Desea eliminar el usuario?", userId);
  }

  showDialogYesNo(title: string, userId: string): any {
    let dialogRef = this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteUser(userId);
    });
  }

  deleteUser(userId: string) {
    this.ApiAccountService.deleteUser(userId).subscribe((data: any) => {
      this.getUsers();
      this.showDialog("El usuario ha sido eliminado con exito");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar el usuario");
        }
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim();
  }
}
