import { Component, OnInit } from '@angular/core';
import { ReunionService } from 'src/app/services/reunion/reunion.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleConst } from 'src/app/Constants/RoleConst';
import { Asistencia } from 'src/app/models/asistencia';
import { UserConst } from 'src/app/Constants/Userconst';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { SharedRolesService } from 'src/app/services/shared-roles/shared-roles.service';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { AsistenciaService } from 'src/app/services/asistencia/asistencia.service';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.scss']
})
export class AsistenciaComponent implements OnInit {

  esSecretario: boolean;

  reunionId: string;
  decanoTitular: Asistencia[];
  profesoresTitulares: Asistencia[];
  auxiliaresTitulares: Asistencia[];
  alumnosTitulares: Asistencia[];

  vicedecanoSuplente: Asistencia[];
  profesoresSuplentes: Asistencia[];
  auxiliaresSuplentes: Asistencia[];
  alumnosSuplentes: Asistencia[];

  //Tabla responsiva titulares
  displayedColumnsTit = ["TITULARES","C","TELEFONO"];
  dataSourceProfesorTit = new MatTableDataSource<Asistencia>();
  dataSourceAuxiliarTit = new MatTableDataSource<Asistencia>();
  dataSourceAlumnoTit = new MatTableDataSource<Asistencia>();

  //Tabla responsiva suplentes
  displayedColumnsSup = ["SUPLENTES","C","TELEFONO"];
  dataSourceProfesorSup = new MatTableDataSource<Asistencia>();
  dataSourceAuxiliarSup = new MatTableDataSource<Asistencia>();
  dataSourceAlumnoSup = new MatTableDataSource<Asistencia>();

  //Tabla responsiva autoridades titulares
  displayedColumnsAutoridadesTit = ["Decano","C","TELEFONO"];
  dataSourceAutoridadesTit = new MatTableDataSource<Asistencia>();

  //Tabla responsiva autoridades suplentes
  displayedColumnsAutoridadesSup = ["Vicedecano","C","TELEFONO"];
  dataSourceAutoridadesSup = new MatTableDataSource<Asistencia>();

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private reunionService: ReunionService,
    private sharedRoles: SharedRolesService,
    private dialog: MatDialog,
    private asistenciaService: AsistenciaService
  ) { }

  ngOnInit() {
    this.armarHTML();
    this.reunionId = this.actRoute.snapshot.paramMap.get('id');

    this.getParticipantes();
  }

  getParticipantes() {
    this.getAutoridades();
    this.getProfesores();
    this.getAuxiliares();
    this.getAlumnos();
  }

  armarHTML() {
    this.sharedRoles.getEsSecretario().subscribe((value) => {
      this.esSecretario = value;
    });
  }

  getDisplayedColumnsTit(): string[] {
    if (this.esSecretario) {
      this.displayedColumnsTit = ["TITULARES","C","P","TELEFONO","Accion"];
    }

   return this.displayedColumnsTit;
  }

  getDisplayedColumnsSup(): string[] {
    if (this.esSecretario) {
      this.displayedColumnsSup = ["SUPLENTES","C","P","TELEFONO","Accion"];
    }

   return this.displayedColumnsSup;
  }

  getDisplayedColumnsAutoridadesTit(): string[] {
    if (this.esSecretario) {
      this.displayedColumnsAutoridadesTit = ["Decano","C","P","TELEFONO","Accion"];
    }

   return this.displayedColumnsAutoridadesTit;
  }

  getDisplayedColumnsAutoridadesSup(): string[] {
    if (this.esSecretario) {
      this.displayedColumnsAutoridadesSup = ["Vicedecano","C","P","TELEFONO","Accion"];
    }

   return this.displayedColumnsAutoridadesSup;
  }

  cambiarPresente(asistenciaId: string) {
    this.asistenciaService.actualizarPresenteEnReunion(asistenciaId).subscribe((data: any) => {
      this.getParticipantes();
      this.showDialog("El presente se cambio con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer actualizar el presente.");
        }
      }
    );
  }

  getAutoridades() {
    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolDecano, UserConst.puestoTitular).subscribe((data: any) => {
      this.decanoTitular = data;
      this.dataSourceAutoridadesTit = new MatTableDataSource<Asistencia>(this.decanoTitular);
    });

    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolVicedecano, UserConst.puestoTitular).subscribe((data: any) => {
      this.vicedecanoSuplente = data;
      this.dataSourceAutoridadesSup = new MatTableDataSource<Asistencia>(this.vicedecanoSuplente);
    });

  }

  getProfesores() {
    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolProfesor, UserConst.puestoTitular).subscribe((data: any) => {
      this.profesoresTitulares = data;
      this.dataSourceProfesorTit = new MatTableDataSource<Asistencia>(this.profesoresTitulares);
    });

    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolProfesor, UserConst.puestoSuplente).subscribe((data: any) => {
      this.profesoresSuplentes = data;
      this.dataSourceProfesorSup = new MatTableDataSource<Asistencia>(this.profesoresSuplentes);
    });

  }

  getAuxiliares() {
    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolAuxiliar, UserConst.puestoTitular).subscribe((data: any) => {
      this.auxiliaresTitulares = data;
      this.dataSourceAuxiliarTit = new MatTableDataSource<Asistencia>(this.auxiliaresTitulares);
    });
    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolAuxiliar, UserConst.puestoSuplente).subscribe((data: any) => {
      this.auxiliaresSuplentes = data;
      this.dataSourceAuxiliarSup = new MatTableDataSource<Asistencia>(this.auxiliaresSuplentes);
    });
  }

  getAlumnos() {
    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolAlumno, UserConst.puestoTitular).subscribe((data: any) => {
      this.alumnosTitulares = data;
      this.dataSourceAlumnoTit = new MatTableDataSource<Asistencia>(this.alumnosTitulares);
    });
    this.reunionService.getAllAsistenciasByRol(this.reunionId, RoleConst.rolAlumno, UserConst.puestoSuplente).subscribe((data: any) => {
      this.alumnosSuplentes = data;
      this.dataSourceAlumnoSup = new MatTableDataSource<Asistencia>(this.alumnosSuplentes);
    });
  }

  volverReunion() {
    this.router.navigate([LinkConst.urlReunion]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

}
