import { Component, OnInit, ViewChild } from '@angular/core';
import { Reunion } from 'src/app/models/reunion';
import { errorMessages } from 'src/app/validators/customValidators';
import { User } from 'src/app/models/user';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { Router } from '@angular/router';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { ReunionService } from 'src/app/services/reunion/reunion.service';
import { RoleConst } from 'src/app/Constants/RoleConst';
import { ComparadorRolesService } from 'src/app/services/comparador-roles/comparador-roles.service';
import { Asistencia } from 'src/app/models/asistencia';
import { AsistenciaService } from 'src/app/services/asistencia/asistencia.service';

@Component({
  selector: 'app-reunion',
  templateUrl: './reunion.component.html',
  styleUrls: ['./reunion.component.scss']
})
export class ReunionComponent implements OnInit {

  reuniones: Reunion[];
  errors = errorMessages; //To bind errors messages into html
  @ViewChild('f') myNgForm;
  editState: boolean = false;

  textoFiltro: string = "";

  showABMReunion: boolean = false;
  showAsistencias: boolean = false;
  showConfirmarAsistencia: boolean = false;
  showDecisiones: boolean = false;
  showAcciones: boolean = false;

  user: User;

  asistiraNumerico : Asistencia[];

  //Tabla responsiva
  displayedColumns = ["Fecha", "Tipo","Nro","Ordendia","Acta"];
  dataSource = new MatTableDataSource<Reunion>();

  constructor(
    private usuarioService: UsuarioService,
    private reunionService: ReunionService,
    private router: Router,
    private dialog: MatDialog,
    private comparadorRolesService: ComparadorRolesService,
    private asistenciaService: AsistenciaService
  ) { }

  ngOnInit() {
    this.armarHTMLsegunRol();
    this.getReuniones();
  }

  armarHTMLsegunRol() {
    if (this.comparadorRolesService.compararRoles([RoleConst.rolAdmin])) {
      this.showABMReunion = true;
      this.showAsistencias = true;
      this.showAcciones = true;
      this.showDecisiones = true;
    }

    if (this.comparadorRolesService.compararRoles([RoleConst.rolSecretarioAcademico])) {
      this.showAsistencias = true;

    }

    if (this.comparadorRolesService.esMiembroDelConsejo()) {
      this.showDecisiones = true;
      this.showAcciones = true;
      this.showConfirmarAsistencia = true;
    }
  }

  getDisplayedColumns(): string[] {
    if (this.comparadorRolesService.esMiembroDelConsejo()) {
      if (this.comparadorRolesService.compararRoles([RoleConst.rolSecretarioAcademico]))
        this.displayedColumns = ["Fecha","Tipo","Nro","Asistencia","Ordendia","Decisiones","Acta","Asistiras","Acciones"];
      else
        this.displayedColumns = ["Fecha", "Tipo","Nro","Ordendia","Decisiones","Acta","Asistiras","Acciones"];
    }

    if (this.comparadorRolesService.compararRoles([RoleConst.rolAdmin])) {
      this.displayedColumns = ["Fecha", "Tipo","Nro","Asistencia","Ordendia","Decisiones","Acta","Quorum","Acciones"];
    }

   return this.displayedColumns;
  }

  getReuniones() {
    this.reunionService.getAllReuniones().subscribe((data: any) => {
      this.reuniones = data;
      this.dataSource = new MatTableDataSource<Reunion>(this.reuniones);
      this.dataSource.filterPredicate = (data: Reunion, filter: string) => data.fecha.indexOf(filter) != -1; // configureo el filtro por fecha
      if( this.comparadorRolesService.esMiembroDelConsejo()){
      var i = 0;
      for (i; i < this.reuniones.length; i++) {
        this.getAsistenciaEnReunion(i);
      }}
      if( this.comparadorRolesService.esAdmin()){
        var i = 0;
        for (i; i < this.reuniones.length; i++) {
          this.hayQuorum(i);
        }}
    });
  }

  getAsistenciaEnReunion(index: number) {
    var reunionActual = this.reuniones[index];
    this.usuarioService.getAsistenciaEnReunion(reunionActual.id.toString()).subscribe((data: any[]) => {
      this.asistiraNumerico = data[0];
      if (data[0].asistire)
        this.reuniones[index].asistira = "Si";
      else
        this.reuniones[index].asistira = "No";
    });
  }

  cambiarAsistencia(reunionId: string) {
    this.asistenciaService.actualizarAsistenciaEnReunion(reunionId).subscribe((data: any) => {
      this.getReuniones();
      this.showDialog("La asistencia se cambio con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer actualizar la asistencia.");
        }
      }
    );
  }

  hayQuorum(index: number){
    var hayQuorum;
    var reunionActual = this.reuniones[index];
    this.reunionService.hayQuorum(reunionActual.id.toString()).subscribe((data: any) => {
      hayQuorum = data;
      if (hayQuorum)
        reunionActual.quorum= "Si";
    else
        reunionActual.quorum= "No";
    });
  }

  goToCrearReunion() {
    this.router.navigate([LinkConst.urlReunionCrear]);
  }

  showAsistencia(id: string) {
    this.router.navigate([LinkConst.urlAsistencia + "/" + id]);
  }

  showOrdendia(id: string) {
    this.router.navigate([LinkConst.urlOrdendia + "/" + id]);
  }

  showDecision(id: string) {
    this.router.navigate([LinkConst.urlDecisionListar + "/" + id]);
  }

  showActa(id: string) {
    this.router.navigate([LinkConst.urlActa + "/" + id]);
  }

  editReunion(id: string) {
    this.router.navigate([LinkConst.urlReunionCrear + "/" + id]);
  }

  confirmarEliminarReunion(reunionId: string) {
    this.showDialogYesNo("¿Desea eliminar la reunión?", reunionId);
  }

  showDialogYesNo(title: string, reunionId: string): any {
    let dialogRef = this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.deleteReunion(reunionId);
    });
  }

  deleteReunion(reunionId: string) {
    this.reunionService.deleteReunion(reunionId).subscribe((data: any) => {
      this.getReuniones();
      this.showDialog("La reunión ha sido eliminada con exito.");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar la reunión.");
        }
      }
    );
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
