import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { errorMessages } from 'src/app/validators/customValidators';
import { ReunionService } from 'src/app/services/reunion/reunion.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { Reunion } from 'src/app/models/reunion';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { ReunionConst } from 'src/app/Constants/ReunionConst';

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'YYYY-MM-DD HH:mm:ss',
  parseInput: 'YYYY-MM-DD HH:mm:ss',
  datePickerInput: 'YYYY-MM-DD HH:mm:ss',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};

@Component({
  selector: 'app-crear-reunion',
  templateUrl: './crear-reunion.component.html',
  styleUrls: ['./crear-reunion.component.scss']
})
export class CrearReunionComponent implements OnInit {

  minDate: Date;
  events: string[] = [];

  reunionDate: Date = new Date("2020-05-26 19:07");
  reunionDateString: string;
  mes: number;

  registerForm: FormGroup;
  reunion: Reunion;
  id: string;

  //For dropdown tipo menu
  currentChoiceList: string[] = Array<string>();
  myChoice: string = "Seleccione un Tipo";

  editState: boolean;
  title: string = "Crear Reunión";

  //errores
  errorsDialog: any = [];
  errorsBack: any = [];

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private actRoute: ActivatedRoute,
    private reunionService: ReunionService,
    dateTimeAdapter: DateTimeAdapter<any>
  ) {
    dateTimeAdapter.setLocale('es');

    this.createForm();
  }

  async ngOnInit() {
    this.minDate = new Date(Date.now());
    this.cargarTiposReunion();

    this.id = this.actRoute.snapshot.paramMap.get('id');

    if (this.id) {
      this.editState = true;
      this.title = "Actualizar Reunión";
      await this.obtenerReunion(this.id);
      this.populateForm(this.reunion);
    }
  }

  async obtenerReunion(id: string) {
    await this.reunionService.getReunionByIDPromise(id).then((data: any) => {
      this.reunion = data;
    });
  }

  createForm() {
    this.registerForm = this.formBuilder.group({

      fecha: ['', [
        Validators.required
      ]]
    });
  }

  populateForm(reunion: Reunion) {
    this.registerForm = this.formBuilder.group({

      fecha: [new Date(this.reunion.fecha), [
        Validators.required
      ]]
    });
  }

  register() {
    if (this.myChoice != "Seleccione un Tipo") {
      if (this.registerForm.valid) {
        this.errorsDialog = []; // limpio los errores del dialogo de error

        this.reunionService.postNewReunion(new Reunion(
          this.armarFecha(this.registerForm.get("fecha").value), this.myChoice)).subscribe((data: any) => {

            this.closeDialog();
            this.showDialog("Reunión creada correctamente");
            this.goToListaUsers();
          },
            err => {
              this.errorsBack = err.error.errors;
              this.concatenarErrores(this.errorsBack);
              if (err.status == 400) {
                this.showDialog("Error al crear la reunion");
              }

              if (err.status == 422) {
                this.closeDialog();
                this.showDialog("Error: " + this.errorsDialog);
              }

              if (err.status == 500) {
                this.showDialog("Error, Verifique los datos ingresados");
              }
            });

        this.smsEsperaCreacionReunion();
      }
      else {
        this.showDialog("Ingrese una fecha correcta.");
      }
    }
    else
      this.showDialog("Debe seleccionar un tipo para la reunion.");
  }

  smsEsperaCreacionReunion() {
    this.showDialog("Creando la reunión...");
    setTimeout(() => { this.closeDialog(); }, 15000);
    setTimeout(() => { this.showDialog("Agregando miembros del consejo a la reunión..."); }, 15000);
    setTimeout(() => { this.closeDialog(); }, 30000);
    setTimeout(() => { this.closeDialog(); this.showDialog("Enviando mail a los miembros del consejo, por favor espere..."); }, 30000);
  }

  armarFecha(fechaCompleta: Date): string {
    return (fechaCompleta.getFullYear() + '-' + this.agregarDigito(fechaCompleta.getMonth() + 1) + '-' + this.agregarDigito(fechaCompleta.getDate()) + ' ' + this.agregarDigito(fechaCompleta.getHours()) + ':' + this.agregarDigito(fechaCompleta.getMinutes()));
  }

  agregarDigito(numero: number): string {
    var numeroString;
    if (numero < 10)
      numeroString = "0" + numero;
    else
      numeroString = "" + numero;
    return numeroString;
  }

  concatenarErrores(errorsBack: any) {
    if (errorsBack.fecha) {
      this.errorsDialog += errorsBack.fecha;
      this.errorsDialog += " "
    }
    if (errorsBack.tipo) {
      this.errorsDialog += errorsBack.tipo;
      this.errorsDialog += " "
    }
  }

  update() {
    if (this.registerForm.valid) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Editando la reunion, por favor espere.");

      this.reunionService.editReunion(new Reunion(
        this.armarFecha(this.registerForm.get("fecha").value), this.reunion.tipo), this.id).subscribe((data: any) => {
          this.closeDialog();
          this.showDialog("Reunión editada correctamente");
          this.goToListaUsers();
        },
          err => {
            this.errorsBack = err.error.errors;
            this.concatenarErrores(this.errorsBack);

            if (err.status == 400) {
              this.showDialog("Error al editar la reunion");
            }

            if (err.status == 422) {
              this.closeDialog();
              this.showDialog("Error: " + this.errorsDialog);
            }

            if (err.status == 500) {
              this.showDialog("Error del servidor, intente mas tarde.");
            }
          });
    }
    //this.showDialog("Editando la reunion, por favor espere.");
  }

  goToListaUsers() {
    this.router.navigate([LinkConst.urlReunion]);
  }

  goToRegister() {
    this.router.navigate([LinkConst.urlRegister]);
  }

  goToLogin() {
    this.router.navigate([LinkConst.urlLogin]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }

  cancel() {
    this.router.navigate([LinkConst.urlReunion]);
  }

  cargarTiposReunion() {
    this.currentChoiceList.push(ReunionConst.reunionOrdinaria);
    this.currentChoiceList.push(ReunionConst.reunionExtraordinaria);
    this.currentChoiceList.push(ReunionConst.reunionConstitutiva);
  }

  tipoReunionSeleccionado(choice: string) {
    this.myChoice = choice;
  }
}
