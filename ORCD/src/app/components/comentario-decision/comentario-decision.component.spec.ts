import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentarioDecisionComponent } from './comentario-decision.component';

describe('ComentarioDecisionComponent', () => {
  let component: ComentarioDecisionComponent;
  let fixture: ComponentFixture<ComentarioDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentarioDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentarioDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
