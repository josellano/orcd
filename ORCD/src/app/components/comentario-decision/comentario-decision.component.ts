import { Component, OnInit, ViewChild, SecurityContext, ViewEncapsulation } from '@angular/core';
import { Item } from 'src/app/models/item';
import { ItemConst } from 'src/app/Constants/ItemConst';
import { Decision } from 'src/app/models/decision';
import { RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DecisionService } from 'src/app/services/decision/decision.service';
import { ItemService } from 'src/app/services/item/item.service';
import { DomSanitizer } from '@angular/platform-browser';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { ComentarioDecisionService } from 'src/app/services/comentarioDecision/comentario-decision.service';
import { ComentarioDecision } from 'src/app/models/comentarioDecision';

@Component({
  selector: 'app-comentario-decision',
  templateUrl: './comentario-decision.component.html',
  styleUrls: ['./comentario-decision.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ComentarioDecisionComponent implements OnInit {

  comentarios: ComentarioDecision[];
  misComentarios: ComentarioDecision[];

  crearComentario: boolean = false;
  mostrarMisComentarios: boolean = false;

  tituloComentarios: string = "Comentarios";

  editState: boolean = false;
  nombreUsuario: string;
  apellidoUsuario: string;
  comentarioEditado: ComentarioDecision;

  itemId: string;
  item: Item = null;
  textoValido: boolean = false;
  cargoDecision: boolean = false;

  itemComun: string = ItemConst.itemComun;
  itemSobretabla: string = ItemConst.itemSobretabla;

  title: string = "Buscando Comentarios";
  decision: Decision;

  //errores
  errorsDialog: any= [];
  errorsBack: any = [];

  //RichtextEDitor
  @ViewChild('fromRTE')
  private rteEle: RichTextEditorComponent;
  public value: string = "Ingrese aqui el comentario";

  rteCreated(): void {
    this.rteEle.element.focus();
  }

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable', '|',
      'ClearFormat', '|', 'FullScreen']
  };
  //RichtextEDitor fin

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog,
    private decisionService: DecisionService,
    private comentarioDecisionService: ComentarioDecisionService,
    private itemService: ItemService,
    private sanitized: DomSanitizer
  ) { }

  async ngOnInit() {
    this.itemId = this.actRoute.snapshot.paramMap.get('itemid');
    this.obtenerComentarios();
    await this.obtenerItem();
    this.title = "Comentarios del Item " + this.item.numero;
    this.obtenerDecision(this.itemId);
  }

  obtenerComentarios() {
    this.comentarioDecisionService.getComentarios(this.itemId).subscribe((data: any) => {
      this.comentarios = data;
    });
  }

  obtenerMisComentarios() {
    this.comentarioDecisionService.getComentariosUser(this.itemId).subscribe((data: any) => {
      this.misComentarios = data;
    });
  }

  async obtenerItem() {
    await this.itemService.getItemByIDMCPromise(this.itemId).then((data: any) => {
      this.item = data;
    });
  }

  async obtenerDecision(id: string) {
    await this.decisionService.getDecisionByIDMCPromise(id).then((data: any) => {
      this.decision = data;
      this.cargoDecision = true;
    });
  }

  validarTexto() {
    const errorSms = '<p><span style="color: rgb(255, 0, 0); text-decoration: inherit;">Por favor ingrese el comentario de la Decision.</span></p>';
    if (this.value == null || this.value == errorSms) {
      this.value = errorSms;
      this.textoValido = false;
      return false
    }
    else {
      this.textoValido = true;
      this.value = this.sanitized.sanitize(SecurityContext.NONE, this.value);
      return true;
    }
  }

  limpiarTexto() {
    this.value = null;
  }

  goToComentariosItem() {
    this.router.navigate([LinkConst.urlComentarioDecision + "/" + this.itemId]);
  }

  goToDecisiones() {
    this.router.navigate([LinkConst.urlDecisionListar + "/" + this.item.ordendia_id]);
  }

  mostrarDivAgregar() {
    this.crearComentario = true;
    this.ocultarDivMisComentarios();
  }

  ocultarDivAgregar() {
    this.crearComentario = false;
  }

  mostrarDivMisComentarios() {
    this.mostrarMisComentarios = true;
    this.obtenerMisComentarios();
    this.ocultarDivAgregar();
    this.tituloComentarios = "Mis comentarios";
  }

  ocultarDivMisComentarios() {
    this.mostrarMisComentarios = false;
    this.tituloComentarios = "Comentarios";
  }

  guardarComentario() {
    if (this.textoValido) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Creando el comentario, por favor espere.");

      this.comentarioDecisionService.guardarComentario(new ComentarioDecision(this.value, Number(this.itemId))).subscribe((data: any) => {

        this.closeDialog();
        this.showDialog("Comentario creado correctamente.");
        this.obtenerComentarios();
        this.crearComentario = false;
        this.value = "Ingrese aqui el comentario";
        this.goToComentariosItem();
      },
        err => {
          this.errorsBack = err.error.errors;
          this.concatenarErrores(this.errorsBack);

          if (err.status == 400) {
            this.showDialog("Error al crear el comentario.");
          }

          if (err.status == 422) {
            this.closeDialog();
            this.showDialog("Error: "+this.errorsDialog);
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados.");
          }
        });

     // this.showDialog("Creando el comentario, por favor espere.");
    }
  }

  async irEditarComentario(comentarioId: string) {
    this, this.editState = true;
    this.mostrarDivAgregar();

    await this.comentarioDecisionService.getComentarioByIDPromise(comentarioId).then((data: any) => {
      this.comentarioEditado = data;
    });
    this.value = this.comentarioEditado.contenido;
  }

  editarComentario() {
    if (this.textoValido) {
      this.errorsDialog = []; // limpio los errores del dialogo de error
      this.showDialog("Actualizando comentario, por favor espere.");

      this.comentarioEditado.contenido= this.value;
      this.comentarioDecisionService.editarComentario(this.comentarioEditado, this.comentarioEditado.id.toString()).subscribe((data: any) => {

        this.closeDialog();
        this.showDialog("Comentario editado correctamente.");

        this.mostrarDivMisComentarios();
        this.value = "Ingrese aqui el comentario.";
        this, this.editState = false;
        this.goToComentariosItem();
      },
        err => {

          if (err.status == 400) {
            this.showDialog("Error al actualizar el comentario.");
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados.");
          }
        });

      //this.showDialog("Actualizando comentario, por favor espere.");
    }
  }

  eliminarComentario(comentarioId: string) {
    this.comentarioDecisionService.eliminarComentario(comentarioId).subscribe((data: any) => {
      this.obtenerMisComentarios();
      this.showDialog("El comentario ha sido eliminado con exito");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar el comentario");
        }
      }
    );
  }

  concatenarErrores(errorsBack: any) {
    if (errorsBack && errorsBack.contenido){
      this.errorsDialog += errorsBack.fecha;
      this.errorsDialog += " "
    }
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }

}
