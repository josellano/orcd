import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { errorMessages, CustomValidators } from 'src/app/validators/customValidators';
import { MatDialog } from '@angular/material';
import { ApiServiceLoginService } from 'src/app/services/login/api-service-login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/services/localStorage/storage.service';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  resetForm: FormGroup;
  token: any;

  errors = errorMessages; //To bind errors messages into html

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private actRoute: ActivatedRoute,
    private usuarioService: UsuarioService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.token = this.actRoute.snapshot.paramMap.get('token');
  }

  createForm() {
    this.resetForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      passwordGroup: this.formBuilder.group({
        password: ['', [
          Validators.required
        ]],
        repeatPassword: ['', Validators.required]
      }, { validator: CustomValidators.childrenEqual })
    });
  }

  resetPassword() {

    if (!this.resetForm.get("passwordGroup").valid) {
      this.showDialog("Las contraseñas no coinciden");
    }
    else
      if (this.resetForm.valid) {
        this.usuarioService.resetPassword(this.resetForm.get("email").value, this.resetForm.get("passwordGroup").get("password").value, this.token).subscribe((data: any) => {
          this.showDialog("Su contraseña ha sido actualizada correctamente.");
          this.router.navigate(['/']);
        },
          err => {
            if (err.status == 401) {
              this.showDialog("El usuario y/o contraseña es inválido");
            }

            if (err.status == 422) {
              this.showDialog("Error: " + err.error.error);
            }
          });
      }
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}
