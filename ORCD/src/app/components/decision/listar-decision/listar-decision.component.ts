import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Reunion } from 'src/app/models/reunion';
import { Item } from 'src/app/models/item';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ItemService } from 'src/app/services/item/item.service';
import { ReunionService } from 'src/app/services/reunion/reunion.service';
import { Traductor } from 'src/app/models/traductor';
import { ItemConst } from 'src/app/Constants/ItemConst';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { Ordendia } from 'src/app/models/ordendia';
import { ComparadorRolesService } from 'src/app/services/comparador-roles/comparador-roles.service';

@Component({
  selector: 'app-listar-decision',
  templateUrl: './listar-decision.component.html',
  styleUrls: ['./listar-decision.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListarDecisionComponent implements OnInit {

  reunionId: string;
  reunion: Reunion;
  orden_id: string;
  orden: Ordendia;
  esAdmin: boolean = false;
  esSecretario: boolean = false;
  esMiembroConsejo: boolean = false;

  itemsComunes: Item[];
  itemsComunesLength: boolean;
  itemsComunesMuestra: Item[] = new Array();
  itemsSobretabla: Item[];
  itemsComunicacion: Item[];


  titulo: string = "Orden del día";
  introduccion: string;
  separadorSobretabla: string = "Items Sobretabla";
  separadorComunicaciones: string = "Comunicaciones ingresadas";
  smsNohayItemsComunes: string = "No hay ítems Comunes";
  smsNohayItemsSobretabla: string = "No hay ítems Sobretabla";
  OrdenPDF: any;

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog,
    private itemService: ItemService,
    private reunionService: ReunionService,
    private comparadorRolesService: ComparadorRolesService
  ) { }

  async ngOnInit() {
    this.mostrarOperaciones();
    this.orden_id = this.actRoute.snapshot.paramMap.get('ordenid');
    this.getItems();
    await this.getOrdendia();
  }

  async getOrdendia() {
    await this.reunionService.getOrdendiaPromise(this.orden_id).then((data: any) => {
      this.orden = data;
    });
    this.getReunion();
  }

  async getReunion() {
    await this.reunionService.getReunionByIDPromise(this.orden.reunion_id).then((data: any) => {
      this.reunion = data;
    });
    this.armarHTML();
  }

  armarHTML() {
    var date = new Date(this.reunion.fecha);
    var mesTraducido = new Traductor();
    this.titulo = "Orden del día";
    this.introduccion = "Reunión <b>" + this.reunion.tipo + "</b> de Consejo Departamental, del día <b>" + date.getDate() + "</b> de <b>" + mesTraducido.getNombreMes(date.getMonth()) + "</b> de <b>" + date.getFullYear() + "</b> a las <b>" + date.getHours() + ":" + date.getMinutes() + " horas.</b>";
    this.separadorSobretabla = "Items Sobretabla";
    this.separadorComunicaciones = "Comunicaciones ingresadas";
  }

  getItems() {
    this.itemService.getItemsByTypeWithDecision(this.orden_id, ItemConst.itemComun).subscribe((data: any) => {
      this.itemsComunes = data;

      var i = 0;
      for (i; i < this.itemsComunes.length; i++) {
        this.getCantComentariosDecision(this.itemsComunes, this.itemsComunes[i].id.toString(), i);
      }
    });

    this.itemService.getItemsByTypeWithDecision(this.orden_id, ItemConst.itemSobretabla).subscribe((data: any) => {
      this.itemsSobretabla = data;

      var i = 0;
      for (i; i < this.itemsSobretabla.length; i++) {
        this.getCantComentariosDecision(this.itemsSobretabla, this.itemsSobretabla[i].id.toString(), i);
      }
    });

    this.itemService.getItemsByTypeWithDecision(this.orden_id, ItemConst.itemComunicacion).subscribe((data: any) => {
      this.itemsComunicacion = data;

      var i = 0;
      for (i; i < this.itemsComunicacion.length; i++) {
        this.getCantComentariosDecision(this.itemsComunicacion, this.itemsComunicacion[i].id.toString(), i);
      }
    });
  }

  getCantComentariosDecision(items: Item[], itemId: string, index: number) {
    var cantidad;

    this.itemService.getCantidadComentariosItem(itemId).subscribe((data: any) => {
      cantidad = data;
      items[index].cantComentarios = cantidad;
    });
  }

  editItem(itemId: string) {
    this.router.navigate([LinkConst.urlItem + "/" + itemId]);
  }

  deleteItem(itemId: string) {
    this.itemService.deleteItem(itemId).subscribe((data: any) => {
      this.getItems();
      this.showDialog("El item ha sido eliminado con exito");
    },
      err => {
        if (err.status == 404) {
          this.showDialog("Se ha producido un error al querer eliminar el item");
        }
      }
    );
  }

  mostrarOperaciones() {
    if (this.comparadorRolesService.esAdmin()) {
      this.esAdmin = true;
    }
    if (this.comparadorRolesService.esSecretario()) {
      this.esSecretario = true;
    }
    if (this.comparadorRolesService.esMiembroDelConsejo()) {
      this.esMiembroConsejo = true;
    }
  }



  goToAgregarItemComun() {
    this.router.navigate([LinkConst.urlItem + "/" + this.orden_id + "/" + ItemConst.itemComun]);
  }

  verDecision(item_id: string) {
    this.router.navigate([LinkConst.urlDecision + "/" + item_id]);
  }

  verComentarios(item_id: string) {
    this.router.navigate([LinkConst.urlComentarioDecision + "/" + item_id]);
  }

  verAdjunto(item_id: string) {
    this.router.navigate([LinkConst.urlAdjuntoPDF + "/" + item_id]);
  }

  goToAgregarItemSobretabla() {
    this.router.navigate([LinkConst.urlItem + "/" + this.orden_id + "/" + ItemConst.itemSobretabla]);
  }

  goToAgregarItemComunicacion() {
    this.router.navigate([LinkConst.urlItem + "/" + this.orden_id + "/" + ItemConst.itemComunicacion]);
  }

  goToOrdendia() {
    this.router.navigate([LinkConst.urlOrdendia + "/" + this.orden_id]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}
