import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarDecisionComponent } from './listar-decision.component';

describe('ListarDecisionComponent', () => {
  let component: ListarDecisionComponent;
  let fixture: ComponentFixture<ListarDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
