import { Component, OnInit, ViewChild, SecurityContext } from '@angular/core';
import { ItemConst } from 'src/app/Constants/ItemConst';
import { Item } from 'src/app/models/item';
import { RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DecisionService } from 'src/app/services/decision/decision.service';
import { LinkConst } from 'src/app/Constants/LinkConst';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
import { Decision } from 'src/app/models/decision';
import { ItemService } from 'src/app/services/item/item.service';
import { PrecargadaService } from 'src/app/services/precargada/precargada.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.scss']
})
export class DecisionComponent implements OnInit {

  itemId: string;
  item: Item;
  textoValido: boolean = false;
  cargoDecision: boolean = false;

  habilitarRTE: boolean= true;

  itemComun: string = ItemConst.itemComun;
  itemSobretabla: string = ItemConst.itemSobretabla;

  title: string = "Buscando decision";
  decisionEditada: Decision;

  @ViewChild('fromRTE')
  private rteEle: RichTextEditorComponent;
  public value: string = "Buscando contenido de la decision";

  contenidoPrecargado: string;

  rteCreated(): void {
    this.rteEle.element.focus();
  }

  public tools: object = {
    items: [
      'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
      'FontName', 'FontSize', 'FontColor', '|',
      'LowerCase', 'UpperCase', '|', 'Undo', 'Redo', '|',
      'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
      'Indent', 'Outdent', '|', 'CreateLink', 'CreateTable', '|',
      'ClearFormat', '|', 'FullScreen']
  };

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private dialog: MatDialog,
    private decisionService: DecisionService,
    private itemService: ItemService,
    private precargadaService: PrecargadaService,
    private sanitized: DomSanitizer
  ) { }

  async ngOnInit() {
    this.itemId = this.actRoute.snapshot.paramMap.get('itemid');
    this.obtenerPrecargado(); // para obtener si es que lo hay contenido precargado seleccionado

    await this.obtenerItem();
    this.title = "Decision del Item " + this.item.numero;
    this.obtenerDecision(this.itemId);
  }

  seleccionarPrecargada() {
    this.router.navigate([LinkConst.urlPrecargadaDecision + "/" + this.decisionEditada.id]);
  }

  async obtenerDecision(id: string) {
    await this.decisionService.getDecisionByIDPromise(id).then((data: any) => {
      this.decisionEditada = data;
      this.cargoDecision=true;
    });
    // si se selecciono un contenido precargado hay que setear ese valor y no el obtenido.
    if (this.contenidoPrecargado == "")
      this.value = this.decisionEditada.contenido;
    else
      this.value = this.contenidoPrecargado;
  }

  obtenerPrecargado() {
    this.precargadaService.getContenidoPrecargado().subscribe((value) => {
      this.contenidoPrecargado = value;
    });
  }

  async obtenerItem() {
    await this.itemService.getItemByIDPromiseSecretario(this.itemId).then((data: any) => {
      this.item = data;
    });
  }

  validarTexto() {
    this.habilitarRTE=false;
    const errorSms = '<p><span style="color: rgb(255, 0, 0); text-decoration: inherit;">Por favor ingrese el contenido de la Decision.</span></p>';
    if (this.value == null || this.value == errorSms) {
      this.value = errorSms;
      this.textoValido = false;
      return false
    }
    else {
      this.textoValido = true;
      this.value = this.sanitized.sanitize(SecurityContext.NONE, this.value);
      return true;
    }
  }

  //habiltiar el RTE una vez que ya se habia confirmado el texto.
  cambiarTexto() {
    this.habilitarRTE=true;
    this.textoValido=false;
  }

  //Limpiar el texto del RTE
  limpiarTexto() {
    this.value = null;
  }

  goToOrdendia() {
    this.router.navigate([LinkConst.urlOrdendia + "/" + this.item.ordendia_id]);
  }

  goToDecisiones() {
    this.router.navigate([LinkConst.urlDecisionListar + "/" + this.item.ordendia_id]);
  }

  editarDecision() {
    this.decisionEditada.contenido = this.value;
    if (this.textoValido) {

      this.decisionService.editDecision(this.decisionEditada, this.itemId).subscribe((data: any) => {

        this.closeDialog();
        this.showDialog("Decision actualizada correctamente");
        this.goToDecisiones();
      },
        err => {
          const errorMessages = new Array<{ propName: string; errors: string }>();

          if (err.status == 400) {
            this.showDialog("Error al actualizar la decision");
          }

          if (err.status == 500) {
            this.showDialog("Error, Verifique los datos ingresados");
          }
        });

        this.showDialog("Actualizando decision, por favor espere.");
    }
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }
}
