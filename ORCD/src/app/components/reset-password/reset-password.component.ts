import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { errorMessages } from 'src/app/validators/customValidators';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  emailForm: FormGroup;
  errors = errorMessages; //To bind errors messages into html

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private usuarioService: UsuarioService
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required]]
    })
  }

  enviarMailResetPass() {
    if (this.emailForm.valid) {
      this.usuarioService.enviarMailResetPass(this.emailForm.get("email").value).subscribe((data: any) => {
        this.showDialog("Le hemos enviado un email para restablecer su contraseña.");
        this.router.navigate(['/']);
      },
        err => {
          if (err.status == 401) {
            this.showDialog("El usuario y/o contraseña es inválido");
          }
        });
    }
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }

}
