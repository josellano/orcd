import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatDialogModule,
  MatOptionModule,
  MatSelectModule,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatTooltipModule,
  MatDatepickerModule,
  MatNativeDateModule,
  NativeDateAdapter,
   DateAdapter,
  MAT_DATE_FORMATS,
  MatTableModule,
  MatPaginatorModule,
  MatPaginatorIntl
} from '@angular/material';

import { DialogBodyComponent } from './dialog-body/dialog-body.component';
import { StorageService } from './services/localStorage/storage.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from './components/register/register.component';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Interceptor } from './services/interceptors/http-interceptors';
import { RoleGuard } from './guards/role.guard';
import { UsersComponent } from './components/users/users.component';
import { ReunionComponent } from './components/reunion/reunion.component';
import { AsistenciaComponent } from './components/asistencia/asistencia.component';
import { CrearReunionComponent } from './components/reunion/crear-reunion/crear-reunion.component';
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
} from 'ng-pick-datetime';

import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { OrdendiaComponent } from './components/ordendia/ordendia.component';
import { ItemComponent } from './components/item/item.component';
import { ListarDecisionComponent } from './components/decision/listar-decision/listar-decision.component';
import { DecisionComponent } from './components/decision/decision.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ActaComponent } from './components/acta/acta.component';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { OrdendiaPDFComponent } from './components/ordendia/ordendia-pdf/ordendia-pdf.component';
import { ItemPrecargadaComponent } from './components/precargada/item-precargada/item-precargada.component';
import { DecisionPrecargadaComponent } from './components/precargada/decision-precargada/decision-precargada.component';
import { FilterPipe } from './components/precargada/filter.pipe';
import { CrearPrecargadaComponent } from './components/precargada/crear-precargada/crear-precargada.component';
import { ComentarioDecisionComponent } from './components/comentario-decision/comentario-decision.component';
import { AdjuntoComponent } from './components/adjunto/adjunto.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { getSpanishPaginatorIntl } from './spanishPagination/spanish-paginator-intl';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DialogBodyComponent,
    RegisterComponent,
    DashboardComponent,
    UsersComponent,
    ReunionComponent,
    AsistenciaComponent,
    CrearReunionComponent,
    OrdendiaComponent,
    ItemComponent,
    DecisionComponent,
    ListarDecisionComponent,
    ActaComponent,
    OrdendiaPDFComponent,
    ItemPrecargadaComponent,
    DecisionPrecargadaComponent,
    FilterPipe,
    CrearPrecargadaComponent,
    ComentarioDecisionComponent,
    AdjuntoComponent,
    ResetPasswordComponent,
    ChangePasswordComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RichTextEditorAllModule,
    NgxExtendedPdfViewerModule,
    ReactiveFormsModule, //Para que lo pueda bindear del html
    NgbModule, //Para tener ngbootstrap
    HttpClientModule, //Para poder utilizar la API
    //angular materials
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatOptionModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatPaginatorModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatNativeDateModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [StorageService, AuthenticatedGuard, RoleGuard, MatDatepickerModule, {provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true}, {provide: LocationStrategy, useClass: HashLocationStrategy}, { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() }],
  bootstrap: [AppComponent], entryComponents: [DialogBodyComponent]
})
export class AppModule { }

//platformBrowserDynamic().bootstrapModule(AppModule);
