import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '../services/localStorage/storage.service';
import { LinkConst } from '../Constants/LinkConst';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  constructor(
      private router: Router,
      private storageService: StorageService
  ){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if(!this.storageService.isTokenExpired()){
        return true;
    }
    if(localStorage.getItem('currentUser'))
        return true;
    //If not exist the user in the local storage then navigate to login page
    this.router.navigate([LinkConst.urlLogin]);
    return false;
}


}
