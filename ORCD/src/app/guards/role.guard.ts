import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '../services/localStorage/storage.service';
import { LinkConst } from '../Constants/LinkConst';
import { UsuarioService } from '../services/usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  username: string;
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  roles: Promise<boolean>;
  rolesNombre: string;
  misroles: string[];

  constructor(
    private router: Router,
    private storageService: StorageService,
    private accountService: UsuarioService
  ) { }

   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

   const expectedRole: string[] = route.data.expectedRole;

   this.misroles = expectedRole;

    if (!this.storageService.isTokenExpired() && this.storageService.getToken() )  {

      this.rolesNombre = this.storageService.getRoles();
      if ( this.estaRol()) {
        return true;
      }
      else{
        this.router.navigate([LinkConst.url]);
        return false;
      }


    } // del if

    //If not exist the user in the local storage then navigate to login page
    this.router.navigate([LinkConst.urlLogin]);
    return false;
  }

  estaRol(){
    var esta: boolean = false;
    for (var i = 0; i < this.misroles.length && !esta; i++) {
      if (this.rolesNombre.includes(this.misroles[i]))
        esta = true;
    }
    return esta;
  }
}
