import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart} from '@angular/router';
import { StorageService } from './services/localStorage/storage.service';
import { MatDialog } from '@angular/material';
import { RoleConst } from './Constants/RoleConst';
import { LinkConst } from './Constants/LinkConst';
import { DialogBodyComponent } from './dialog-body/dialog-body.component';
import { UsuarioService } from './services/usuario/usuario.service';
import { SharedRolesService } from './services/shared-roles/shared-roles.service';
import { ComparadorRolesService } from './services/comparador-roles/comparador-roles.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  showDropdownLogin = true;

  name: String;
  roles: string = RoleConst.rolPublico;
  stringRoles: string;
  username: string = "Ingresa";

  constructor(
    private router: Router,
    private storageService: StorageService,
    private accountService: UsuarioService,
    private sharedRoles: SharedRolesService,
    private comparadorRoles: ComparadorRolesService,
    private dialog: MatDialog
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart)
          this.actualizarUserData();
    });
  }

  ngOnInit() {
    this.storageService.setUsername(this.username);

    if (this.storageService.getToken() != null) {
      this.showDropdownLogin = false;
      this.obtenerUserData();
    }
    else {
      this.storageService.setRoles(this.roles)
    }
  }

  obtenerUserData() {
    if (this.storageService.getUsername() == "Ingresa") {  // si esta logeuado y no cargo el username, los pide, sino los carga directo
      this.accountService.getUserData().subscribe((data: any) => {
        this.username = data.nombre;
        this.storageService.setUsername(data.nombre);
      });
    }
    this.obtenerRol();
  }

  obtenerRol() {
    if (this.storageService.getToken() != null && this.storageService.getRoles() == RoleConst.rolPublico) {  // si esta logeuado y no cargo los roles, los pide, sino los carga directo
      this.accountService.getRoles().subscribe((data: any) => {
        this.roles = data;
        this.storageService.setRoles(JSON.stringify(this.roles));

        if(this.comparadorRoles.esAdmin()){ // para actualizar el dashboard
          this.sharedRoles.setEsAdmin(true);
        }
        if(this.comparadorRoles.esSecretario()){ // para actualizar el dashboard
          this.sharedRoles.setEsSecretario(true);
        }
      });
    }
  }

  actualizarUserData() {
    if (this.storageService.getToken() != null) {
      this.showDropdownLogin = false;
      this.accountService.getUserData().subscribe((data: any) => {
        this.username = data.nombre;
        this.storageService.setUsername(data.nombre);
        this.obtenerRol();
        if(this.comparadorRoles.esAdmin()){
          this.sharedRoles.setEsAdmin(true);
        }
        if(this.comparadorRoles.esSecretario()){ // para actualizar el dashboard
          this.sharedRoles.setEsSecretario(true);
        }
      });
    }
  }

  async logout() {
    this.storageService.logout();
    this.sharedRoles.setEsAdmin(false);
    this.showDialog("Su sesión ha sido cerrada");
    this.showDropdownLogin = true;
    this.username = "Ingresa";
    window.location.reload( await this.router.navigate(['/']));
  }

  goToRegister() {
    this.router.navigate([LinkConst.urlRegister]);
  }

  goToLogin() {
    this.router.navigate([LinkConst.urlLogin]);
  }

  showDialog(title: string): void {
    this.dialog.open(DialogBodyComponent, {
      data: { title: title }
    });
  }
}
