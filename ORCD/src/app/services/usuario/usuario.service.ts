import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/models/user';
import { APIConst } from 'src/app/Constants/APIConst';
import { StorageService } from '../localStorage/storage.service';
import { shareReplay, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  token: string;
  bearer: string;

  constructor(
    private httpClient: HttpClient
  ) { }

  postNewUser(user: User) {
    return this.httpClient.post(APIConst.urlRegister, user);
  }

  enviarMailResetPass(email: any) {
    let body = new URLSearchParams();
    body.set('email', email);

    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
    return this.httpClient.post(APIConst.urlMailPassword, body.toString(), options);
  }

  resetPassword(email: any, password: string, token: string) {
    let body = new URLSearchParams();
    body.set('email', email);
    body.set('password', password);
    body.set('password_confirmation', password);
    body.set('token', token);

    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
    return this.httpClient.post(APIConst.urlResetPassword, body.toString(), options);
  }

  getUserData() {
    return this.httpClient.get(APIConst.urlGetUserData);
  }

  getUserDataPromise() {
    return this.httpClient.get(APIConst.urlGetUserData).toPromise();
  }

  getRoles() {
    return this.httpClient.get(APIConst.urlGetUserRole);
  }

  getRolesPromise() { // obtiene los roles del usuario logueado
    return this.httpClient.get(APIConst.urlGetUserRole).toPromise();
  }

  getRolesUserPromise(userId: string) { // obtiene roles de un user por id
    return this.httpClient.get(APIConst.urlAdminUsersRoles + userId).toPromise();
  }

  getAsistenciaEnReunion(reunionId: string) {
    return this.httpClient.get(APIConst.urlReunionAsistencias + '/' + reunionId);
  }

  getAllUsers() {
    return this.httpClient.get(APIConst.urlAdminUsers);
  }

  deleteUser(userId: string) {
    return this.httpClient.delete(APIConst.urlAdminUsers + userId);
  }

  editUser(user: User, id: string) {
    return this.httpClient.put(APIConst.urlAdminUsers + id, user);
  }

  getUserByIDPromise(id: string) {
    return this.httpClient.get(APIConst.urlAdminUsers + id).toPromise();
  }

  getUserByID(id: string) {
    return this.httpClient.get(APIConst.urlGetUserData + id);
  }

}
