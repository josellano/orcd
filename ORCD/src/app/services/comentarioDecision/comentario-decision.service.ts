import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIConst } from 'src/app/Constants/APIConst';
import { ComentarioDecision } from "src/app/models/comentarioDecision";

@Injectable({
  providedIn: 'root'
})
export class ComentarioDecisionService {


  constructor(
    private httpClient: HttpClient
  ) { }

  getComentarios(itemId: string) {
    return this.httpClient.get(APIConst.urlComentarioDecisionItem + itemId);
  }

  getComentariosUser(itemId: string) {
    return this.httpClient.get(APIConst.urlComentarioDecisionUser + itemId);
  }

  guardarComentario(comentario: ComentarioDecision) {
    return this.httpClient.post(APIConst.urlComentarioDecision, comentario);
  }

  editarComentario(comentario: ComentarioDecision, itemId: string) {
    return this.httpClient.put(APIConst.urlComentarioDecision + itemId, comentario);
  }

  eliminarComentario(comentarioId: string){
    return this.httpClient.delete(APIConst.urlComentarioDecision+comentarioId);
  }

  getComentarioByIDPromise(comentarioId: string){
    return this.httpClient.get(APIConst.urlComentarioDecision+comentarioId).toPromise();
  }
}
