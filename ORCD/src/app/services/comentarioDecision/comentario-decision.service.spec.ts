import { TestBed } from '@angular/core/testing';

import { ComentarioDecisionService } from './comentario-decision.service';

describe('ComentarioDecisionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComentarioDecisionService = TestBed.get(ComentarioDecisionService);
    expect(service).toBeTruthy();
  });
});
