import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { LinkConst } from "src/app/Constants/LinkConst";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material";
import { finalize, tap } from 'rxjs/operators';
import { StorageService } from '../localStorage/storage.service';
import { DialogBodyComponent } from 'src/app/dialog-body/dialog-body.component';
@Injectable()
export class Interceptor implements HttpInterceptor {

  token: string;
  bearer: string;

  constructor(
    private authSession: StorageService,
    private router: Router,
    private dialog: MatDialog,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    this.token = this.authSession.getToken();
    if(this.token){
      this.bearer= this.token.substring(1,this.token.length-1);
      req = req.clone({ headers: req.headers.set('Authorization', "Bearer " + this.bearer) });
    }

    let status: string;
    return next.handle(req).pipe(
      tap(
        event => {
          status = '';
          if (event instanceof HttpResponse) {
            status = 'succeeded';
          }
        },
        error => {/*
          if (error.status === 401) {
            this.authSession.closeCurrentSession(); //Close session because we dont have permission to do the requirement
            //No authorized, redirect to login

            this.router.navigate([LinkConst.urlLogin]);
          } else if (error.status == 500) {
            //this.router.navigate([LinkConst.urlRegister]);

          }

          else if (error.status !== 400 && error.status !== 404) { //Because the error 400 was catched in each component
            this.authSession.closeCurrentSession(); //Close session because we dont have permission to do the requirement
            //No authorized, redirect to login


            this.router.navigate([LinkConst.urlLogin]);

            this.router.navigate(['/']);
            this.dialog.open(DialogBodyComponent, {
              data: { title: "Esto es vergonzoso :(", body: "Ha ocurrido un problema con el servidor" }
            });

          }*/
        }
      ), finalize(() => { })
    );

  }
}
