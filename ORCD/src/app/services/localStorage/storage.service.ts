import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
export const TOKEN_NAME: string = 'currentUser';

import { LinkConst } from 'src/app/Constants/LinkConst';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { UsuarioService } from '../usuario/usuario.service';
import { SharedRolesService } from '../shared-roles/shared-roles.service';
import { ComparadorRolesService } from '../comparador-roles/comparador-roles.service';
import { RoleConst } from 'src/app/Constants/RoleConst';
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  roles: Promise<boolean>;
  rolesNombre: string;

  private localStorageService;
  private accountService: UsuarioService;
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);//this exist because we need it to hide or show navbar
  constructor(
    private router: Router,
    private sharedRoles: SharedRolesService
  ) {
    this.localStorageService = localStorage;
  }

  isTokenExpired(token?: string): boolean {
    if (!token)
      token = this.getToken();
    if (!token)
      return true;
    return false;
    /*
    const date = this.getTokenExpirationDate(token);
    if (date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
    */
  }

  getTokenExpirationDate(token: string): Date {

    const decoded:any = token;

    if (decoded.exp === undefined) return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token): void {
    localStorage.setItem(TOKEN_NAME, JSON.stringify(token));
  }

  setRoles(rolesNombre: string) {
    localStorage.setItem("roles", rolesNombre);
  }

  getRoles(): string {
    return localStorage.getItem("roles");
  }

  setUsername(username: string) {
    localStorage.setItem("username", username);
  }

  getUsername(): string {
    return localStorage.getItem("username");
  }

  closeCurrentSession(){
    if(this.getToken() != null){
      this.localStorageService.removeItem(TOKEN_NAME);
    }
  }

  login(token){
    this.loggedIn.next(true);
    this.setToken(token);
    //this.router.navigate(['/']);
  }

  logout(){
    if(this.getToken() != null){
      this.loggedIn.next(false);
      this.closeCurrentSession();
      this.setRoles(null);
      this.setUsername("Ingresa");
      //this.router.navigate([LinkConst.urlLogin]);
    }
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }



}
