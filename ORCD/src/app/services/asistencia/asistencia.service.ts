import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIConst } from 'src/app/Constants/APIConst';

@Injectable({
  providedIn: 'root'
})
export class AsistenciaService {

  constructor(
    private httpClient: HttpClient
    ) { }

  actualizarAsistenciaEnReunion(reunionId: string ) {
    return this.httpClient.post(APIConst.urlReunionAsistenciasAsistire+'/'+reunionId,null);
  }

  actualizarPresenteEnReunion(asistenciaId: string ) {
    return this.httpClient.post(APIConst.urlReunionAsistenciasPresente+'/'+asistenciaId,null);
  }
}
