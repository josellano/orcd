import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { APIConst } from 'src/app/Constants/APIConst';
import { LinkConst } from 'src/app/Constants/LinkConst';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceLoginService {

  constructor(
    private httpClient: HttpClient
  ) { }

  login(email: string, password:string){

    let body = new URLSearchParams();
    body.set('email', email);
    body.set('password', password);
    body.set('grant_type',"password");

    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
    return this.httpClient.post(APIConst.urlLogin, body.toString(),options);
  }
}


