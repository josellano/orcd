import { TestBed } from '@angular/core/testing';

import { ApiServiceLoginService } from './api-service-login.service';

describe('ApiServiceLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiServiceLoginService = TestBed.get(ApiServiceLoginService);
    expect(service).toBeTruthy();
  });
});
