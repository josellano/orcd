import { TestBed } from '@angular/core/testing';

import { ComparadorRolesService } from './comparador-roles.service';

describe('ComparadorRolesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComparadorRolesService = TestBed.get(ComparadorRolesService);
    expect(service).toBeTruthy();
  });
});
