import { Injectable } from '@angular/core';
import { StorageService } from '../localStorage/storage.service';
import { RoleConst } from 'src/app/Constants/RoleConst';

@Injectable({
  providedIn: 'root'
})
export class ComparadorRolesService {

  rolesDelUsuario: string;

  rolesDelConsejo = [ RoleConst.rolSecretarioAcademico, RoleConst.rolProfesor, RoleConst.rolAuxiliar, RoleConst.rolAlumno, RoleConst.rolDecano, RoleConst.rolVicedecano];
  rolAdmin = [ RoleConst.rolAdmin];
  rolSecretario = [ RoleConst.rolSecretarioAcademico];

  constructor( private storageService: StorageService) { }

  compararRoles(rolesRequeridos: string[]): boolean {
    this.rolesDelUsuario = this.storageService.getRoles();
    if (this.estaRol(rolesRequeridos))
       return true;
    else
      return false;
  }

  estaRol(rolesRequeridos: string []){
    var esta: boolean = false;
    for (var i = 0; i < rolesRequeridos.length && !esta; i++) {
      if (this.rolesDelUsuario.includes(rolesRequeridos[i]))
        esta = true;
    }
    return esta;
  }

  esAdmin(){
    this.rolesDelUsuario = this.storageService.getRoles();
    if (this.estaRol(this.rolAdmin))
       return true;
    else
      return false;
  }

  esSecretario(){
    this.rolesDelUsuario = this.storageService.getRoles();
    if (this.estaRol(this.rolSecretario))
       return true;
    else
      return false;
  }

  esMiembroDelConsejo(){
    this.rolesDelUsuario = this.storageService.getRoles();
    if (this.estaRol(this.rolesDelConsejo))
       return true;
    else
      return false;
  }
}
