import { TestBed } from '@angular/core/testing';

import { SharedRolesService } from './shared-roles.service';

describe('SharedRolesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedRolesService = TestBed.get(SharedRolesService);
    expect(service).toBeTruthy();
  });
});
