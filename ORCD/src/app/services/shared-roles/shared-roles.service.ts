import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedRolesService {

  private esAdmin: BehaviorSubject<boolean>;
  private esSecretario: BehaviorSubject<boolean>;

  constructor() {
    this.esAdmin = new BehaviorSubject<boolean>(false);
    this.esSecretario = new BehaviorSubject<boolean>(false);
  }


  setEsAdmin(newValue): void {
    this.esAdmin.next(newValue);
  }

  getEsAdmin(): Observable<boolean> {
    return this.esAdmin.asObservable();
  }

  setEsSecretario(newValue): void {
    this.esSecretario.next(newValue);
  }

  getEsSecretario(): Observable<boolean> {
    return this.esSecretario.asObservable();
  }
}
