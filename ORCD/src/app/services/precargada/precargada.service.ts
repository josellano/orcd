import { Injectable, SecurityContext } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIConst } from 'src/app/Constants/APIConst';
import { Precargada } from 'src/app/models/precargada';
import { BehaviorSubject, Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class PrecargadaService {


  private contenidoPrecargado: BehaviorSubject<string>;
  private urlPrevia: BehaviorSubject<string>;

  constructor(
    private httpClient: HttpClient,
    private sanitized: DomSanitizer
  ) {
    this.contenidoPrecargado = new BehaviorSubject<string>("");
    this.urlPrevia = new BehaviorSubject<string>("");
  }

  getAllItemsPrecargados() {
    return this.httpClient.get(APIConst.urlPrecargadaItem);
  }

  getAllDecisionesPrecargadas() {
    return this.httpClient.get(APIConst.urlPrecargadaDecision);
  }

  deletePrecargada(id: string) {
    return this.httpClient.delete(APIConst.urlPrecargada + id);
  }

  editPrecargada(precargada: Precargada, id: string) {
    return this.httpClient.put(APIConst.urlPrecargada + id, precargada);
  }

  getPrecargadaByIDPromise(id: string) {
    return this.httpClient.get(APIConst.urlPrecargadaId + id).toPromise();
  }

  crearPrecargada(precargada: Precargada, tipo: string) {
    return this.httpClient.post(APIConst.urlPrecargada + tipo, precargada);
  }

  setContenidoPrecargado(newValue): void {
    this.contenidoPrecargado.next(this.sanitized.sanitize(SecurityContext.HTML,newValue));
  }

  getContenidoPrecargado(): Observable<string> {
    return this.contenidoPrecargado.asObservable();
  }

  setUrlPrevia(newValue): void {
    this.urlPrevia.next(newValue);
  }

  getUrlPrevia(): Observable<string> {
    return this.urlPrevia.asObservable();
  }
}

