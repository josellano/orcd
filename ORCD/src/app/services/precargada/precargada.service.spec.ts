import { TestBed } from '@angular/core/testing';

import { PrecargadaService } from './precargada.service';

describe('PrecargadaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrecargadaService = TestBed.get(PrecargadaService);
    expect(service).toBeTruthy();
  });
});
