import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../localStorage/storage.service';
import { User } from 'src/app/models/user';
import { APIConst } from 'src/app/Constants/APIConst';
import { Reunion } from 'src/app/models/reunion';
import { share, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReunionService {

  constructor(
    private httpClient: HttpClient,
    private authSession: StorageService
  ) { }


  getAllReuniones() {
    return this.httpClient.get(APIConst.urlReunion).pipe(shareReplay());
  }

  getAllAsistenciasByRol(reunionId: string, rol: string, puesto: string ) {
    return this.httpClient.get(APIConst.urlReunionAsistencias+"/"+reunionId+"/"+rol+"/"+puesto);
  }

  getAllAsistenciasByRolPromise(reunionId: string, rol: string, puesto: string ) {
    return this.httpClient.get(APIConst.urlReunionAsistencias+"/"+reunionId+"/"+rol+"/"+puesto).toPromise();
  }

  postNewReunion(reunion: Reunion){
    return this.httpClient.post(APIConst.urlAdminReunion, reunion);
  }

  editReunion(reunion: Reunion, id: string){
    return this.httpClient.put(APIConst.urlAdminReunion+id,reunion);
  }

  getReunionByIDPromise(id: string){
    return this.httpClient.get(APIConst.urlReunion+id).toPromise();
  }

  getReunionByOrdendiaIDPromise(id: string){
    return this.httpClient.get(APIConst.urlReunionByOrdendia+id).toPromise();
  }

  getOrdendiaPromise(id: string){
    return this.httpClient.get(APIConst.urlOrdendia+id).toPromise();
  }

  deleteReunion(userId: string){
    return this.httpClient.delete(APIConst.urlAdminReunion+userId);
  }

  hayQuorum(reunionId: string){
    return this.httpClient.get(APIConst.urlAdminQuorum+reunionId);
  }

  // para probar enviar los mails despues, no sirvio
  enviarRecordatorio(reunionId: string){
    return this.httpClient.get(APIConst.urlAdminMail+reunionId);
  }

  getRoles() {
    return this.httpClient.get(APIConst.urlGetUserRole);
  }

  getRolesPromise() {
    return this.httpClient.get(APIConst.urlGetUserRole).toPromise();
  }



}
