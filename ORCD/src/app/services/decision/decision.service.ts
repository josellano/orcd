import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../localStorage/storage.service';
import { APIConst } from 'src/app/Constants/APIConst';
import { Item } from 'src/app/models/item';
import { Decision } from 'src/app/models/decision';

@Injectable({
  providedIn: 'root'
})
export class DecisionService {

  constructor(
    private httpClient: HttpClient,
    private authSession: StorageService
  ) { }

  postNewDecision(decision: Decision, itemId: string) {
    //return this.httpClient.post(APIConst.urlSecretarioDecision + itemId, decision);
    return this.httpClient.post(APIConst.urlDecision + itemId, decision);
  }

  editDecision(decision: Decision, decisionId: string){ // ojo esta id
    //return this.httpClient.put(APIConst.urlSecretarioDecision+decisionId,decision);
    return this.httpClient.put(APIConst.urlDecision+decisionId,decision);
  }

  deleteDecision(decisionId: string){
    //return this.httpClient.delete(APIConst.urlSecretarioDecision+decisionId);
    return this.httpClient.delete(APIConst.urlDecision+decisionId);
  }


  getDecisionByIDPromise(id: string){
    //return this.httpClient.get(APIConst.urlSecretarioDecision+id).toPromise();
    return this.httpClient.get(APIConst.urlDecision+id).toPromise();
  }

  getDecisionByIDMCPromise(id: string){
    //return this.httpClient.get(APIConst.urlDecisionListar+id).toPromise();
    return this.httpClient.get(APIConst.urlDecision+id).toPromise();
  }

  /*getItemsByTypePromise(ordendiaId: string, tipo: string) {
    return this.httpClient.get(APIConst.urlAdminItem + ordendiaId + "/" + tipo).toPromise();
  } lo saque despues de cambiar als rutas, estaba de mas por lo visto.*/


}
