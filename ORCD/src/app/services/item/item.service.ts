import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APIConst } from 'src/app/Constants/APIConst';
import { Item } from 'src/app/models/item';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getItemsByType(ordendiaId: string, tipo: string) {
    return this.httpClient.get(APIConst.urlItem + ordendiaId + "/" + tipo);
  }

  getItemsByTypeWithDecision(ordendiaId: string, tipo: string) {
    return this.httpClient.get(APIConst.urlDecisionListar + ordendiaId + "/" + tipo);
  }

  getItemsByTypePromise(ordendiaId: string, tipo: string) {
    return this.httpClient.get(APIConst.urlItem + ordendiaId + "/" + tipo).toPromise();
  }

  postNewItem(item: Item, ordendiaId: string, tipo: string, files: File) {
    let formData: FormData = new FormData();

    if (files != null)
        formData.append('files', files, files.name);

    for (var property in item) {
      if (item.hasOwnProperty(property)) {
        formData.append(property, item[property]);
      }
    }
    return this.httpClient.post(APIConst.urlAdminItem + ordendiaId + "/" + tipo, formData);
  }

  editItem(item: Item, itemId: string) {
    return this.httpClient.put(APIConst.urlAdminItem + itemId, item);
  }

  deleteItem(itemId: string) {
    return this.httpClient.delete(APIConst.urlAdminItem + itemId);
  }

  agregarAdjunto(itemId: string, files: File) {
    let formData: FormData = new FormData();
    formData.append('files', files, files.name);

    return this.httpClient.post(APIConst.urlAdminItemAdjuntoAgregar + itemId, formData);
  }

  deleteAdjunto(itemId: string) {
    return this.httpClient.delete(APIConst.urlAdminItemAdjunto + itemId);
  }

  getItemByIDPromise(id: string) {
    return this.httpClient.get(APIConst.urlAdminItem + id).toPromise();
  }

  getItemByIDPromiseSecretario(id: string) {
    return this.httpClient.get(APIConst.urlSecretarioItem + id).toPromise();
  }

  getItemByIDMCPromise(id: string) {
    return this.httpClient.get(APIConst.urlItem + id).toPromise();
  }

  upItemNumero(itemId: string) {
    return this.httpClient.post(APIConst.urlAdminItemUp + itemId, null);
  }

  downItemNumero(itemId: string) {
    return this.httpClient.post(APIConst.urlAdminItemDown + itemId, null);
  }

  getCantidadComentariosItem(itemId: string) {
    return this.httpClient.get(APIConst.urlCantidadComentarioDecisionItem + itemId);
  }
}
