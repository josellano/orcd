import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinkConst } from './Constants/LinkConst';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { RoleGuard } from './guards/role.guard';
import { RoleConst } from './Constants/RoleConst';
import { UsersComponent } from './components/users/users.component';
import { ReunionComponent } from './components/reunion/reunion.component';
import { AsistenciaComponent } from './components/asistencia/asistencia.component';
import { CrearReunionComponent } from './components/reunion/crear-reunion/crear-reunion.component';
import { OrdendiaComponent } from './components/ordendia/ordendia.component';
import { ItemComponent } from './components/item/item.component';
import { DecisionComponent } from './components/decision/decision.component';
import { ListarDecisionComponent } from './components/decision/listar-decision/listar-decision.component';
import { ActaComponent } from './components/acta/acta.component';
import { OrdendiaPDFComponent } from './components/ordendia/ordendia-pdf/ordendia-pdf.component';
import { ItemPrecargadaComponent } from './components/precargada/item-precargada/item-precargada.component';
import { DecisionPrecargadaComponent } from './components/precargada/decision-precargada/decision-precargada.component';
import { CrearPrecargadaComponent } from './components/precargada/crear-precargada/crear-precargada.component';
import { ComentarioDecisionComponent } from './components/comentario-decision/comentario-decision.component';
import { AdjuntoComponent } from './components/adjunto/adjunto.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: LinkConst.urlLogin, component: LoginComponent },

  { path: LinkConst.urlPassword, component: ResetPasswordComponent },
  { path: LinkConst.urlPasswordChange+ "/:token", component: ChangePasswordComponent },

  //para registrar usuarios
  { path: LinkConst.urlRegister, component: RegisterComponent },

  //para editar usuarios
  { path: LinkConst.urlRegister + "/:id", component: RegisterComponent, canActivate: [AuthenticatedGuard] },

  //para ABM usuarios del Admin
  {
    path: LinkConst.urlAdmin, component: UsersComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },

  //para reuniones
  { path: LinkConst.urlReunion, component: ReunionComponent },
  {
    path: LinkConst.urlReunionCrear, component: CrearReunionComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },
  {
    path: LinkConst.urlReunionCrear + "/:id", component: CrearReunionComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },

  //para ordenes del dia
  { path: LinkConst.urlOrdendia + "/:id", component: OrdendiaComponent },

  //para Items
  {
    path: LinkConst.urlItem + "/:ordenid" + "/:tipo", component: ItemComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },
  {
    path: LinkConst.urlItem + "/:itemid", component: ItemComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },
  { path: LinkConst.urlAdjuntoPDF + "/:id", component: AdjuntoComponent },

  //para Decisiones
  {
    path: LinkConst.urlDecision + "/:itemid", component: DecisionComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolDecano, RoleConst.rolVicedecano, RoleConst.rolSecretarioAcademico, RoleConst.rolProfesor, RoleConst.rolAlumno, RoleConst.rolAuxiliar] }
  },
  {
    path: LinkConst.urlDecision + "/:itemid", component: DecisionComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolSecretarioAcademico] }
  },
  {
    path: LinkConst.urlDecisionListar + "/:ordenid", component: ListarDecisionComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolDecano, RoleConst.rolVicedecano, RoleConst.rolSecretarioAcademico, RoleConst.rolAlumno, RoleConst.rolAuxiliar, RoleConst.rolProfesor] }
  },

  //para asistencias
  {
    path: LinkConst.urlAsistencia + "/:id", component: AsistenciaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolSecretarioAcademico] }
  },

  //para ordenes del dia
  { path: LinkConst.urlActa + "/:id", component: ActaComponent },
  { path: LinkConst.urlOrdendiaPDF + "/:id", component: OrdendiaPDFComponent },

  //para asistencias
  {
    path: LinkConst.urlAsistencia + "/:id", component: AsistenciaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolSecretarioAcademico] }
  },
  //para precargadas
  {
    path: LinkConst.urlPrecargadaItem, component: ItemPrecargadaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },
  {
    path: LinkConst.urlPrecargadaItem + "/:id", component: ItemPrecargadaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin] }
  },
  {
    path: LinkConst.urlPrecargadaDecision, component: DecisionPrecargadaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolSecretarioAcademico] }
  },
  {
    path: LinkConst.urlPrecargadaDecision + "/:id", component: DecisionPrecargadaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolSecretarioAcademico] }
  },
  {
    path: LinkConst.urlPrecargadaCrear + "/:tipo", component: CrearPrecargadaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolSecretarioAcademico] }
  },
  {
    path: LinkConst.urlPrecargadaCrear + "/:tipo/:id", component: CrearPrecargadaComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolAdmin, RoleConst.rolSecretarioAcademico] }
  },

   //para comentariosDecision
   {
    path: LinkConst.urlComentarioDecision + "/:itemid", component: ComentarioDecisionComponent, canActivate: [RoleGuard],
    data: { expectedRole: [RoleConst.rolSecretarioAcademico, RoleConst.rolProfesor, RoleConst.rolAuxiliar, RoleConst.rolAlumno] }
  },

  //default
  { path: '**', redirectTo: '' }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
