export class LinkConst{
    //public static url = "localhost:4200/";
    public static url = "192.168.0.111:8080/";

    public static urlRegister = "register";
    public static urlLogin = "login";

    public static urlPassword = "password";
    public static urlPasswordChange = LinkConst.urlPassword+"/reset";

    public static urlAdmin = "admin";

    public static urlReunion = "reunion";
    public static urlReunionCrear = LinkConst.urlReunion+"/crear";

    public static urlOrdendia = "ordendia";
    public static urlOrdendiaCrear = LinkConst.urlOrdendia+"/crear";
    public static urlOrdendiaPDF = LinkConst.urlOrdendia+"/PDF";

    public static urlItem = "item";
    public static urlAdjuntoPDF = "adjunto";

    public static urlDecision = "decision";
    public static urlDecisionListar = LinkConst.urlDecision+"/listar";

    public static urlAsistencia = "asistencia";

    public static urlActa = "acta";

    public static urlPrecargada =  "precargada/";
    public static urlPrecargadaCrear = LinkConst.urlPrecargada+"crear";
    public static urlPrecargadaItem = LinkConst.urlPrecargada+"item";
    public static urlPrecargadaDecision = LinkConst.urlPrecargada+"decision";

    public static urlComentarioDecision =  "comentarioDecision";
}
