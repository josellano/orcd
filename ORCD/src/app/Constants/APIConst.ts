export class APIConst {
  //public static url = "http://localhost:8000/";
  public static url = "http://192.168.0.111:8000/";
  //public static url = "https://hosting.cs.uns.edu.ar/~sgcd:8000/";
  public static api = APIConst.url + "api/"

  //Login
  public static urlLogin = APIConst.api + "login";

 //Password
 public static urlPassword = APIConst.api + "password/";
 public static urlMailPassword = APIConst.urlPassword + "email";
 public static urlResetPassword = APIConst.urlPassword + "reset";


  //register
  public static urlRegister = APIConst.api + "register";

  //usuario
  public static urlGetUserData = APIConst.api + "user/";
  public static urlGetUserRole = APIConst.urlGetUserData + "roles";
  public static urlGetAllUsers = APIConst.api + "GetAllUsers/";

  //rutas admin
  public static urlAdmin = APIConst.api + "admin/";
  public static urlAdminUsers = APIConst.urlAdmin + "users/";
  public static urlAdminUsersRoles = APIConst.urlAdminUsers + "roles/";
  public static urlAdminReunion = APIConst.urlAdmin + "reunion/";
  public static urlAdminItem = APIConst.urlAdmin + "item/";
  public static urlAdminItemUp = APIConst.urlAdmin + "itemup/";
  public static urlAdminItemDown = APIConst.urlAdmin + "itemdown/";
  public static urlAdminItemAdjunto = APIConst.urlAdminItem + "adjunto/";
  public static urlAdminItemAdjuntoAgregar = APIConst.urlAdminItemAdjunto + "agregar/";
  public static urlAdminQuorum = APIConst.urlAdminReunion + "quorum/";
  public static urlAdminMail = APIConst.urlAdminReunion + "mail/";

  //rutas precargadas
  public static urlPrecargada = APIConst.urlAdmin + "precargada/";
  public static urlPrecargadaId = APIConst.urlPrecargada + "id/";
  public static urlPrecargadaCrear = APIConst.urlPrecargada + "create/";
  public static urlPrecargadaItem = APIConst.urlPrecargada + "item/";
  public static urlPrecargadaDecision = APIConst.urlPrecargada + "decision/";

  //rutas reunion
  public static urlReunion = APIConst.api + "reunion/";
  public static urlReunionCrear = APIConst.api + "create";
  public static urlReunionAsistencias = APIConst.urlReunion + "asistencia";
  public static urlReunionAsistenciasAsistire = APIConst.urlReunionAsistencias + "/asistire";
  public static urlReunionAsistenciasPresente = APIConst.urlReunionAsistencias + "/presente";

  //rutas Orden del Dia
  public static urlOrdendia = APIConst.api + "reunion/ordendia/";
  public static urlReunionByOrdendia = APIConst.api + "ordendia/reunion/";

  //rutas Item
  public static urlItem = APIConst.api + "item/";

  //rutas Decision
  //public static urlSecretario = APIConst.api + "secretario/";
  public static urlDecision = APIConst.api + "decision/";
  public static urlDecisionListar = APIConst.urlDecision + "listar/";
  //public static urlSecretarioDecision = APIConst.api + "decision/";
  public static urlSecretarioItem = APIConst.urlDecision  + "item/";

  //rutas PDF
  public static urlActaPDF = APIConst.api + "acta/PDF/";
  public static urlOrdendiaPDF = APIConst.api + "ordendia/PDF/";
  public static urlAdjuntoPDF = APIConst.api + "adjunto/PDF/";

  //rutas comentarios
  public static urlComentarioDecision = APIConst.api + "comentarioDecision/";
  public static urlComentarioDecisionUser = APIConst.urlComentarioDecision + "user/";
  public static urlComentarioDecisionItem = APIConst.urlComentarioDecision + "item/";
  public static urlCantidadComentarioDecisionItem = APIConst.urlComentarioDecisionItem + "cantidad/";

}
