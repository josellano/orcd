export class RoleConst{
    public static rolAdmin = "admin";
    public static rolDecano = "decano";
    public static rolVicedecano = "vicedecano";
    public static rolSecretarioAcademico = "secretarioAcademico";
    public static rolProfesor = "profesor";
    public static rolAuxiliar = "auxiliar";
    public static rolPublico = "publico";
    public static rolAlumno = "alumno";

    public static claustroAdmin = "Administrativo";
    public static claustroDecano = "Decano";
    public static claustroVicedecano = "Vicedecano";
    public static claustroSecretarioAcademico = "Secretario Academico";
    public static claustroProfesor = "Claustro Profesor";
    public static claustroAuxiliar = "Claustro Auxiliar";
    public static claustroPublico = "Publico";
    public static claustroAlumno = "Claustro Alumno";


}
