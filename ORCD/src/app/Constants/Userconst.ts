export class UserConst{
  public static puestoTitular = "1";
  public static puestoSuplente = "0";

  public static puestoTitularString = "titular";
  public static puestoSuplenteString = "suplente";

  public static generoMasculino = "1";
  public static generoFemenino = "0";

  public static generoMasculinoString = "Masculino";
  public static generoFemeninoString = "Femenino";

  public static titulo_academico_dr = "Dr";
  public static titulo_academico_dra = "Dra";
  public static titulo_academico_mg = "Mg";
  public static titulo_academico_ing = "Ing";
  public static titulo_academico_lic = "Lic";
  public static titulo_academico_sr = "Sr";
  public static titulo_academico_sra = "Sra";


}
