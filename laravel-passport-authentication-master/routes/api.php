<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'PassportController@login');

Route::post('register', 'PassportController@register');


Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// rutas users
Route::middleware('auth:api')->group(function () {
    Route::get('user', 'UserController@details');
    Route::get('user/roles', 'UserController@roles');

    Route::get('reunion/asistencia/{reunion}', 'UserController@getAsistenciaEnReunion');
    Route::post('reunion/asistencia/asistire/{reunion}', 'AsistenciaController@actualizarAsistenciaEnReunion');
    Route::post('reunion/asistencia/presente/{asistencia}', 'AsistenciaController@actualizarPresenteEnReunion');
});

Route::prefix('admin')->middleware(['auth:api', 'can:manage-users'])->group(function () {
    Route::resource('/users', 'UserController', ['except' => ['show', 'create', 'store']]);
    Route::get('/users/{user}', 'UserController@getUserById');
    Route::get('/users/roles/{user}', 'UserController@getRolesUser');
});

// rutas reuniones
Route::prefix('admin')->middleware(['auth:api', 'can:manage-reuniones'])->group(function () {
    Route::post('/reunion', 'ReunionController@create');
    Route::put('/reunion/{reunion}', 'ReunionController@update');
    Route::delete('/reunion/{reunion}', 'ReunionController@destroy');
    Route::get('/reunion/quorum/{reunion}', 'ReunionController@getQuorum');
});
Route::get('reunion', 'ReunionController@index');
Route::get('/reunion/{reunion}', 'ReunionController@getReunionById');
Route::get('/reunion/ordendia/{reunion}', 'ReunionController@getOrdendia');
Route::get('/ordendia/reunion/{ordendia}', 'ReunionController@getReunionByOrdendiaId');

// rutas item
Route::prefix('admin')->middleware(['auth:api', 'can:manage-items'])->group(function () {
    Route::get('/item/{ordendia}/{tipo}', 'ItemController@getItemsByType');
    Route::post('/item/{ordendia}/{tipo}', 'ItemController@create');
    Route::put('/item/{item}', 'ItemController@update');
    Route::delete('/item/{item}', 'ItemController@destroy');
    Route::get('/item/{item}', 'ItemController@getItemById');
    Route::post('/itemup/{item}', 'ItemController@upItemNumero');
    Route::post('/itemdown/{item}', 'ItemController@downItemNumero');

    Route::post('/item/adjunto/agregar/{item}', 'ItemController@agregarAdjunto');
    Route::delete('/item/adjunto/{item}', 'ItemController@eliminarAdjunto');
});
Route::get('/item/{ordendia}/{tipo}', 'ItemController@getItemsByType');

// rutas decision
Route::middleware(['auth:api', 'can:manage-decisiones'])->group(function () {
    Route::post('/decision/{item}', 'DecisionController@create');
    Route::put('/decision/{decision}', 'DecisionController@update');
    Route::delete('/decision/{decision}', 'DecisionController@destroy');
    Route::get('/decision/{decision}', 'DecisionController@getDecisionById');

    Route::get('/decision/item/{item}', 'ItemController@getItemById');
});
Route::get('/decision/listar/{ordendia}/{tipo}', 'ItemController@getItemsByTypeWithDecision');

// para asistencias
Route::get('reunion/asistencia/{id}/{rol}/{puesto}', 'ReunionController@getAsistenciasByRol')->middleware(['auth:api', 'can:manage-asistencias']);

// para pdfs
Route::get('/ordendia/PDF/{ordendia}', 'PDFController@downloadOrdenPDF');
Route::get('/acta/PDF/{ordendia}', 'PDFController@downloadActaPDF');
Route::get('/adjunto/PDF/{item}', 'ItemController@verAdjunto');

// rutas precargadas
Route::prefix('admin')->middleware(['auth:api', 'can:manage-precargadas'])->group(function () {
    Route::get('/precargada/{tipo}', 'PrecargadaController@getPrecargadasByType');
    Route::post('/precargada/{tipo}/', 'PrecargadaController@create');
    Route::put('/precargada/{precargada}', 'PrecargadaController@update');
    Route::delete('/precargada/{precargada}', 'PrecargadaController@destroy');
    Route::get('/precargada/id/{precargada}', 'PrecargadaController@getPrecargadaById');
});

// para comentarios
Route::middleware(['auth:api', 'can:manage-comentarios'])->group(function () {
    Route::get('/comentarioDecision/item/{item}', 'ComentarioDecisionController@getComentariosDecisionItem');
    Route::post('/comentarioDecision', 'ComentarioDecisionController@create');
    Route::put('/comentarioDecision/{comentarioDecision}', 'ComentarioDecisionController@update');
    Route::delete('/comentarioDecision/{comentarioDecision}', 'ComentarioDecisionController@destroy');
    Route::get('/comentarioDecision/{comentarioDecision}', 'ComentarioDecisionController@getComentarioById');

    Route::get('/item/{item}', 'ItemController@getItemById');
    Route::get('/decision/{decision}', 'DecisionController@getDecisionById');

    Route::get('user/{user}', 'UserController@getUserById');
    Route::get('/comentarioDecision/user/{item}', 'ComentarioDecisionController@getComentariosDecisionItemUser');

    Route::get('/comentarioDecision/item/cantidad/{item}/', 'ComentarioDecisionController@getCantidadComentariosDecisionItem');

});
