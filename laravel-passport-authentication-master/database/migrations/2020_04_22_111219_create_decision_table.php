<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDecisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decision', function (Blueprint $table) {
            $table->increments('id');
            $table->text('contenido');
            $table->bigInteger('item_id')-> unsigned()->nullable();
            $table->bigInteger('comentarioDecision_id')-> unsigned()->nullable();
            $table->timestamps();
        });

        DB::table('decision', function (Blueprint $table) {

            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade');
            $table->foreign('comentarioDecision_id')->references('id')->on('comentarioDecision')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decision');
    }
}
