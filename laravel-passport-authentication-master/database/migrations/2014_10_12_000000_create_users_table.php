<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('email')->unique();
            $table->boolean('puesto');
            $table->boolean('genero');
            $table->string('telefono');
            $table->string('titulo_academico');
            $table->string('password');
            $table->bigInteger('comentarioDecision_id')-> unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('comentarioDecision', function (Blueprint $table) {

            $table->foreign('comentarioDecision_id')->references('id')->on('comentarioDecision')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
