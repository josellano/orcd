<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class AsistenciaForeignKey extends Migration
{
    public function up()
    {

        DB::table('asistencia', function (Blueprint $table) {

            $table->foreign('reunion_id')->references('id')->on('reunion')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::table('asistencia', function (Blueprint $table) {
            $table->dropForeign('reunion_id_foreign');
            $table->dropForeign('user_id_foreign');

        });
    }
}
