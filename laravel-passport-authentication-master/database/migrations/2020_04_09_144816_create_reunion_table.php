<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReunionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reunion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->smallInteger('numero');
            $table->string('fecha');
            $table->bigInteger('ordendia_id')-> unsigned()->nullable();
            $table->timestamps();
        });

        DB::table('reunion', function (Blueprint $table) {

            $table->foreign('ordendia_id')->references('id')->on('ordendia')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reunion');
    }
}
