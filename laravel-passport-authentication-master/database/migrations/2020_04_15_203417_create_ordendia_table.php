<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateOrdenDiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordendia', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('reunion_id')-> unsigned();
            $table->string('titulo');
            $table->string('separadorSobretabla');
            $table->string('separadorComunicaciones');
            $table->timestamps();
        });

        DB::table('ordendia', function (Blueprint $table) {

            $table->foreign('reunion_id')->references('id')->on('reunion')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordendia');
    }
}
