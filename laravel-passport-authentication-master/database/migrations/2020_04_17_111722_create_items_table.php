<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->smallInteger('numero');
            $table->text('contenido');
            $table->string('adjunto')->nullable();
            $table->bigInteger('ordendia_id')-> unsigned();
            $table->bigInteger('decision_id')-> unsigned()->nullable();
            $table->timestamps();

        });

        DB::table('item', function (Blueprint $table) {

            $table->foreign('ordendia_id')->references('id')->on('ordendia')->onDelete('cascade');
            $table->foreign('decision_id')->references('id')->on('decision')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
