<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateComentarioDecisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarioDecision', function (Blueprint $table) {
            $table->increments('id');
            $table->text('contenido');
            $table->bigInteger('item_id')-> unsigned();
            $table->bigInteger('user_id')-> unsigned();
            $table->timestamps();
        });

        DB::table('comentarioDecision', function (Blueprint $table) {
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarioDecision');
    }
}
