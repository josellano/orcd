<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Http\models\Precargada;
use Illuminate\Support\Facades\Config;

class PrecargadaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('precargada')->truncate();

       $this->crearPrecargadosItems();
       $this->crearPrecargadasDecisiones();
    }

    private function crearPrecargadosItems()
    {
        $contenido = 'Informes de Director y Secretarías';
        $this->crearPrecargadaItem($contenido);

        $contenido = 'Informes de Director';
        $this->crearPrecargadaItem($contenido);

        $contenido = 'Comunicaciones ingresadas';
        $this->crearPrecargadaItem($contenido);
    }

    private function crearPrecargadasDecisiones()
    {
        $contenido = 'Sin observaciones.';
        $this->crearPrecargadaDecision($contenido);

        $contenido = 'Se aprueba por unanimidad.';
        $this->crearPrecargadaDecision($contenido);

        $contenido = 'Se aprueba por unanimidad otorgar lo solicitado.';
        $this->crearPrecargadaDecision($contenido);
    }

    private function crearPrecargadaItem($contenido)
    {
        $itemPrecargado = Config::get('constants.precargada.precargada_item');
        $precargada = Precargada::create([
            'tipo' =>   $itemPrecargado,
            'contenido' => $contenido,
        ]);

        $precargada->save();
    }

    private function crearPrecargadaDecision($contenido)
    {
        $decisionPrecargada = Config::get('constants.precargada.precargada_decision');
        $precargada = Precargada::create([
            'tipo' =>   $decisionPrecargada,
            'contenido' => $contenido,
        ]);

        $precargada->save();
    }
}
