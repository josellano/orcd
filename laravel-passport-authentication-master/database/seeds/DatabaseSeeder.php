<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ReunionTableSeeder::class);
        $this->call(AsistenciaTableSeeder::class);
        $this->call(OrdendiaTableSeeder::class);
        $this->call(ItemTableSeeder::class);
        $this->call(PrecargadaTableSeeder::class);
    }
}
