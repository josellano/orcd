<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Http\models\Ordendia;
use App\Http\models\Item;
use App\Http\models\Decision;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item')->truncate();
        DB::table('decision')->truncate();

        $ordendia =  Ordendia::find(1)->where('id', 1)->first();
        /*
         //Creacion de Item comun
        for ($i = 1; $i <= 3; $i++) {
            $this->crearItemporTipo($ordendia,Config::get('constants.items.item_comun'),$i);
        }

        //Creacion de Item Sobretabla
        for ($i = 1; $i <= 3; $i++) {
            $this->crearItemporTipo($ordendia,Config::get('constants.items.item_sobretabla'),$i);
        }

          //Creacion de Item Comuniacion
          for ($i = 1; $i <= 3; $i++) {
            $this->crearItemporTipo($ordendia,Config::get('constants.items.item_comunicacion'),$i);
        }*/

        $this->crearItemDecisionReal($ordendia);
    }

    private function crearItemporTipo(Ordendia $ordendia, String $tipo, Int $numero)
    {
         $item1 = Item::create([
            'tipo' =>   $tipo,
            'ordendia_id' => $ordendia->id,
            'numero' => $numero,
            'contenido' => '<p>contenido de item '. $numero. ' '.$tipo.'.</p>',
        ]);
        $ordendia->items()->save($item1);

        $nuevadecision = $this->crearDecision($item1->id);
        $item1->decision_id = $nuevadecision->id;
        $item1->save();
    }


    private function crearDecision(String $itemid)
    {
        $decision = new Decision();
        $decision->item_id = $itemid;
        $decision->contenido = "<p>contenido de la decision.</p>";
        $decision->save();

        return $decision;
    }

    //crear ejemplo real
    private function crearItemDecisionReal(Ordendia $ordendia){

        $tipo = Config::get('constants.items.item_comun');
        $numero = 1;
        $contenidoItem = '<p>Informes de Director y Secretarías</p><ul><li>Solicitud de membresía del CLEI</li><li>Resumen charlas "Herramientas para la inserción laboral" y "Emprender en Serio"</li><li>Reunión con Municipio de Coronel Rosales</li><li>Reunión con INTA</li></ul>';
        $contenidoDecision = '<ul><li>El Dr. Falappa informa sobre la aplicación de la membresía del DCIC al CLEI, la cual considera importante pues otras Universidades nacionales participan activamente. Destaca que no solo se realizan conferencias sino también capacitaciones, ciertas ayudas financieras y algunos beneficios adicionales.</li><li>El Dr. Larrea informa sobre las charlas realizadas recientemente. Para la charla de inserción laboral vino mucha gente pero fue amplia disciplinarmente y no enfocada en computación. La charla de emprendedorismo considera que fue muy interesante, pero vinieron pocas personas. Comenta que constantemente busca mayor difusión e interés de los alumnos porque los temas son importantes para la formación profesional.</li><li>El Dr. Larrea informa que tuvo una reunión en Punta Alta con autoridades del Municipio, para interiorizarse en las necesidades en tecnologías de información, que son varias. Comenta que ha ofrecido tareas de auditoría y asesoramiento en función de esto, y espera avanzar en los próximos meses.</li><li>El Dr. Larrea informa que tuvo una reunión con el INTA pues están interesados en una aplicación que analice gráficamente el estado de los campos para diversos fines. No poseen financiamiento, asi que se buscará la forma de colaborar en el desarrollo desde trabajos de materias y trabajos finales de Carrera.</li></ul>';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 2;
        $contenidoItem = 'Comunicaciones ingresadas.';
        $contenidoDecision = 'Sin observaciones.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 3;
        $contenidoItem = 'Aprobación de las actas 6 y 7-19';
        $contenidoDecision = 'Se aprueba por unanimidad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 4;
        $contenidoItem = 'Ratificación de resoluciones emanadas por Director: DCIC-15/19: Designación del Sr. Santiago Pérez en un cargo de Ayudante de Docencia “B” en la asignatura “Lenguajes Formales y Autómatas”.';
        $contenidoDecision = 'Se aprueba por unanimidad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 5;
        $contenidoItem = 'Asignación complementaria para cubrir un cargo de Coordinador de Tutorías.';
        $contenidoDecision = 'Se propone designar al Dr. Luciano Tamargo. Se aprueba por unanimidad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 6;
        $contenidoItem = 'Renuncia del Dr. Javier Echaiz como Coordinador Suplente del Área IV: Sistemas';
        $contenidoDecision = 'Se aprueba por unanimidad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 7;
        $contenidoItem = 'Designación de un miembro titular y uno suplente para la conformación de la Comisión Autoevaluadora que intervendrá en el proceso de Evaluación Interna de la UNS.';
        $contenidoDecision = 'Se aprueba por unanimidad designar a quienes resulten interesados. El Dr. Martínez opina que debe estar formada por dos docentes de diferente antigüedad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 8;
        $contenidoItem = 'Designación de un miembro titular y uno suplente para la conformación de la Comisión Asesora de Planeamiento.';
        $contenidoDecision = 'Se aprueba por unanimidad designar a quienes resulten interesados.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 9;
        $contenidoItem = 'Solicitud de aval para la realización de “Reunión Nacional de Ramas Estudiantiles de IEEF 2019” los días 5,6 y 7 de septiembre en la UNS.';
        $contenidoDecision = 'El estudiante Santiago Orel se encuentra presente y explica los objetivos de la Reunión Nacional. Se aprueba por unanimidad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 10;
        $contenidoItem = 'Nómina de inscriptos PPS: Globant.';
        $contenidoDecision = 'Se aprueba por unanimidad.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 11;
        $contenidoItem = '<p>Dictámenes de Comisión:</p><ul><li>Asuntos Académicos:<ul><li>Solicitud de inscripción fuera de término en la asignatura “Lenguajes Formales y Autómatas” presentada por el alumno Nicolás E. Gallardo.</li></ul></li><li>Interpretación y Reglamento:<ul><li>Solicitud de licencia con goce de haberes presentada por el Dr. Diego Martínez para realizar una estadía de investigación en la Universidad de Bologna en el marco del proyecto MIREL</li></ul></li><li>Economía y Finanzas:<ul><li>Solicitud de ayuda económica para participar en la 10° edición del Training Camp Argentina.</li><li>Solicitud de apoyo económico para asistir al SASE 2019 presentada por el Ing. Mariano Coccia.</li></ul></li></ul>';
        $contenidoDecision = '<ul><li>Se aprueba por unanimidad otorgar lo solicitado.</li><li>Se aprueba por unanimidad, con la abstención del Dr. Martínez.</li><li>Se aprueba por unanimidad otorgar lo solicitado.</li><li>Se aprueba por unanimidad otorgar lo solicitado.</li></ul>';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);

        //sobretablas
        $tipo = Config::get('constants.items.item_sobretabla');
        $numero = 1;
        $contenidoItem = 'Ayuda económica para alumnos para asistir al SASE 2019';
        $contenidoDecision = 'La propuesta cuenta con el aval del plantel docente de la asignatura Sistemas Embebidos. Se aprueba por unanimidad otorgar lo solicitado.';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);

        //comunicaciones
        $tipo = Config::get('constants.items.item_comunicacion');
        $numero = 1;
        $contenidoItem = 'Propuesta de Proyecto Final presentada por los alumnos María Romina Mirasson y Leandro Pedro Manzanal. Dr: Dra. Laura Cobo.';
        $contenidoDecision = '';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);


        $numero = 2;
        $contenidoItem = '<p>Informes de licencias:</p><ul><li>Carlos Chesñevar – 10/06 – Art. 5f</li><li>Damián Flores Choque – 12-14-19-21-26-28/06 – Art. 9</li><li>Pablo Fillottrani – 10 y 11/06 – Art. 5f</li></ul>';
        $contenidoDecision = '';
        $this->crearItemporTipoReal($ordendia, $tipo, $numero, $contenidoItem,$contenidoDecision);

    }

    private function crearItemporTipoReal(Ordendia $ordendia, String $tipo, Int $numero, String $contenidoItem, String $contenidoDecision)
    {
         $item1 = Item::create([
            'tipo' =>   $tipo,
            'ordendia_id' => $ordendia->id,
            'numero' => $numero,
            'contenido' => $contenidoItem,
        ]);
        $ordendia->items()->save($item1);

        $nuevadecision = $this->crearDecisionReal($item1->id,$contenidoDecision);
        $item1->decision_id = $nuevadecision->id;
        $item1->save();
    }


    private function crearDecisionReal(String $itemid, String $contenidoDecision)
    {
        $decision = new Decision();
        $decision->item_id = $itemid;
        $decision->contenido = $contenidoDecision;
        $decision->save();

        return $decision;
    }

}
