<?php

use App\Http\models\Reunion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ReunionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reunion')->truncate();
        DB::table('reunion_user')->truncate();

        $reunion = Reunion::create([
            'fecha' => "2019-06-11 10:15",
            'tipo' => "ordinaria",
            'numero' => 1,
        ]);

        $miembrosConsejo = DB::table('role_user')
        ->join('roles', 'role_id', '=', 'roles.id')->where('roles.name', '!=', Config::get('constants.roles.role_admin'))->where('roles.name', '!=', Config::get('constants.roles.role_publico'))
        ->join('users', 'user_id', '=', 'users.id')
        ->get();

        foreach ($miembrosConsejo as $miembro)
            $reunion->users()->attach($miembro->id);
    }
}
