<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Http\models\Asistencia;
use App\Http\models\Reunion;
use App\Http\models\User;
use Illuminate\Support\Facades\Config;

class AsistenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asistencia')->truncate();

        $reunion =  Reunion::find(1)->where('id', 1)->first();

        //decano
        $user =  User::find(1)->where('id', 2)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);



        //secretario
        $user =  User::find(1)->where('id', 3)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        //primer profesor
        $user =  User::find(1)->where('id', 4)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 5)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 6)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        //profesor suplente
        $user =  User::find(1)->where('id', 7)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 8)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 9)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        //ultimo profesor suplente
        $user =  User::find(1)->where('id', 10)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);


        //auxilires
        $user =  User::find(1)->where('id', 11)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 12)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 13)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 14)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        //alumnos
        $user =  User::find(1)->where('id', 15)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 16)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 17)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 0,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 0,
        ]);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user->asistencias()->save($asistencia1);
        $reunion->asistencias()->save($asistencia1);

        $user =  User::find(1)->where('id', 18)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user =  User::find(1)->where('id', 19)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);

        $user =  User::find(1)->where('id', 20)->first();

        $asistencia1 = Asistencia::create([
            'asistire' => 1,
            'reunion_id' => $reunion->id,
            'user_id' => $user->id,
            'presente' => 1,
        ]);


         //vicedecano
         $user =  User::find(1)->where('id', 21)->first();

         $asistencia1 = Asistencia::create([
             'asistire' => 0,
             'reunion_id' => $reunion->id,
             'user_id' => $user->id,
             'presente' => 0,
         ]);

         $user->asistencias()->save($asistencia1);
         $reunion->asistencias()->save($asistencia1);
    }
}
