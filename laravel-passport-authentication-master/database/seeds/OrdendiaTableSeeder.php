<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\Http\models\Reunion;
use App\Http\models\Ordendia;

class OrdendiaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ordendia')->truncate();

        $reunion =  Reunion::find(1)->where('id', 1)->first();

        $ordendia1 = Ordendia::create([
            'reunion_id' => $reunion->id,
        ]);

        $reunion->setOrdenId($ordendia1->id);
        $reunion->save();
        //$reunion->ordendia()->save($ordendia1);
    }
}
