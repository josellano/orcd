<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\Http\models\User;
use App\Http\models\Role;
use Illuminate\Support\Facades\Config;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $this->crearAdministrativas();
        $this->crearDecano();
        $this->crearSecretarios();
        $this->crearProfesores();
        $this->crearAuxiliares();
        $this->crearAlumnos();
        $this->crearVicedecano();

        $publicoRole = Role::where('name', Config::get('constants.roles.role_publico'))->first();
        $user = User::create([
            'nombre' => 'Generic User',
            'email' => 'user@user.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'publico apellido',
            'genero' => 0,
            'titulo_academico' => 'Sr',
            'password' => Hash::make('adminadmin')
        ]);
        $user->roles()->attach($publicoRole);

    }

    private function crearAdministrativas()
    {
        $adminRole = Role::where('name', Config::get('constants.roles.role_admin'))->first();
        $admin = User::create([
            'nombre' => 'AdministrativaUser',
            'email' => 'admin@admin.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'AdministrativaApellido',
            'genero' => 0,
            'titulo_academico' => 'Admin',
            'password' => Hash::make('adminadmin')
        ]);
        $admin->roles()->attach($adminRole);
    }

    private function crearDecano()
    {
        $decanoRole = Role::where('name', Config::get('constants.roles.role_decano'))->first();
        $decano = User::create([
            'nombre' => 'Marcelo',
            'email' => 'decano@decano.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'Falappa',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $decano->roles()->attach($decanoRole);
    }

    private function crearVicedecano()
    {
        $viceDecanoRole = Role::where('name', Config::get('constants.roles.role_vicedecano'))->first();
        $viceDecano = User::create([
            'nombre' => 'Martin',
            'email' => 'vicedecano@videcano.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'Larrea',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $viceDecano->roles()->attach($viceDecanoRole);
    }

    private function crearSecretarios()
    {
        $secretarioRole = Role::where('name', Config::get('constants.roles.role_secretario'))->first();
        $profesorRole = Role::where('name', Config::get('constants.roles.role_profesor'))->first();

        $secretario = User::create([
            'nombre' => 'SecretarioUser',
            'email' => 'secretario@secretario.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'SecretarioApellido',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $secretario->roles()->attach($profesorRole);
        $secretario->roles()->attach($secretarioRole);
    }

    private function crearProfesores()
    {
        $profesorRole = Role::where('name', Config::get('constants.roles.role_profesor'))->first();

        $profesor = User::create([
            'nombre' => 'ProfesorUser',
            'email' => 'Profesor@profesor.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'ProfesorApellido',
            'genero' => 0,
            'titulo_academico' => 'Dra',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor->roles()->attach($profesorRole);

        $profesor2 = User::create([
            'nombre' => 'ProfesorUser2',
            'email' => 'profesor2@profesor.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'ProfesorApellido2',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor2->roles()->attach($profesorRole);

        $profesor3 = User::create([
            'nombre' => 'ProfesorUser3',
            'email' => 'profesor3@profesor.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'ProfesorApellido3',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor3->roles()->attach($profesorRole);

        $profesor4 = User::create([
            'nombre' => 'ProfesorUser4',
            'email' => 'Profesor4@profesor.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'ProfesorApellido4',
            'genero' => 0,
            'titulo_academico' => 'Dra',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor4->roles()->attach($profesorRole);

        $profesor5 = User::create([
            'nombre' => 'ProfesorUser5',
            'email' => 'profesor5@profesor.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'ProfesorApellido5',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor5->roles()->attach($profesorRole);

        $profesor6 = User::create([
            'nombre' => 'ProfesorUser6',
            'email' => 'profesor6@profesor.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'ProfesorApellido6',
            'genero' => 0,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor6->roles()->attach($profesorRole);

        $profesor7 = User::create([
            'nombre' => 'ProfesorUser7',
            'email' => 'profesor7@profesor.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'ProfesorApellido7',
            'genero' => 1,
            'titulo_academico' => 'Dr',
            'password' => Hash::make('adminadmin')
        ]);
        $profesor7->roles()->attach($profesorRole);
    }

    private function crearAuxiliares()
    {
        $auxiliarRole = Role::where('name', Config::get('constants.roles.role_auxiliar'))->first();

        $auxiliar = User::create([
            'nombre' => 'AuxiliarUser',
            'email' => 'auxiliar@aux.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'AuxiliarApellido',
            'genero' => 0,
            'titulo_academico' => 'Sra',
            'password' => Hash::make('adminadmin')
        ]);
        $auxiliar->roles()->attach($auxiliarRole);

        $auxiliar2 = User::create([
            'nombre' => 'AuxiliarUser2',
            'email' => 'auxiliar2@aux.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'AuxiliarApellido2',
            'genero' => 0,
            'titulo_academico' => 'Ing',
            'password' => Hash::make('adminadmin')
        ]);
        $auxiliar2->roles()->attach($auxiliarRole);

        $auxiliar3 = User::create([
            'nombre' => 'AuxiliarUser3',
            'email' => 'auxiliar3@aux.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'AuxiliarApellido3',
            'genero' => 0,
            'titulo_academico' => 'Sra',
            'password' => Hash::make('adminadmin')
        ]);
        $auxiliar3->roles()->attach($auxiliarRole);

        $auxiliar4 = User::create([
            'nombre' => 'AuxiliarUser4',
            'email' => 'auxiliar4@aux.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'AuxiliarApellido4',
            'genero' => 0,
            'titulo_academico' => 'Sra',
            'password' => Hash::make('adminadmin')
        ]);
        $auxiliar4->roles()->attach($auxiliarRole);
    }

    private function crearAlumnos()
    {
        $alumnoRole = Role::where('name', Config::get('constants.roles.role_alumno'))->first();


        $alumno = User::create([
            'nombre' => 'Jose',
            'email' => 'llanojose.jl@gmail.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'Llano',
            'genero' => 1,
            'titulo_academico' => 'Sr',
            'password' => Hash::make('adminadmin')
        ]);
        $alumno->roles()->attach($alumnoRole);

        $alumno2 = User::create([
            'nombre' => 'AlumnoUser2',
            'email' => 'alumno@alumno.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'AlumnoApellido2',
            'genero' => 1,
            'titulo_academico' => 'Sr',
            'password' => Hash::make('adminadmin')
        ]);
        $alumno2->roles()->attach($alumnoRole);

        $alumno3 = User::create([
            'nombre' => 'AlumnoUser3',
            'email' => 'alumnos3@alumno.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'AlumnoApellido3',
            'genero' => 1,
            'titulo_academico' => 'Sr',
            'password' => Hash::make('adminadmin')
        ]);
        $alumno3->roles()->attach($alumnoRole);

        $alumno4 = User::create([
            'nombre' => 'AlumnoUser4',
            'email' => 'alumnos4@alumno.com',
            'telefono' => '123231233',
            'puesto' => 0,
            'apellido' => 'AlumnoApellido4',
            'genero' => 1,
            'titulo_academico' => 'Sr',
            'password' => Hash::make('adminadmin')
        ]);
        $alumno4->roles()->attach($alumnoRole);

        $alumno5 = User::create([
            'nombre' => 'AlumnoUser5',
            'email' => 'alumnos5@alumno.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'AlumnoApellido5',
            'genero' => 0,
            'titulo_academico' => 'Sra',
            'password' => Hash::make('adminadmin')
        ]);
        $alumno5->roles()->attach($alumnoRole);

        $alumno6 = User::create([
            'nombre' => 'AlumnoUser6',
            'email' => 'alumnos6@alumno.com',
            'telefono' => '123231233',
            'puesto' => 1,
            'apellido' => 'AlumnoApellido6',
            'genero' => 1,
            'titulo_academico' => 'Sr',
            'password' => Hash::make('adminadmin')
        ]);
        $alumno6->roles()->attach($alumnoRole);
    }
}
