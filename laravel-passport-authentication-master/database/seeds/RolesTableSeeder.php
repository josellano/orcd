<?php

use Illuminate\Database\Seeder;
use App\Http\models\Role;
use Illuminate\Support\Facades\Config;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create(['name'=>Config::get('constants.roles.role_admin')]);
        Role::create(['name'=>Config::get('constants.roles.role_decano')]);
        Role::create(['name'=>Config::get('constants.roles.role_vicedecano')]);
        Role::create(['name'=>Config::get('constants.roles.role_secretario')]);
        Role::create(['name'=>Config::get('constants.roles.role_profesor')]);
        Role::create(['name'=>Config::get('constants.roles.role_auxiliar')]);
        Role::create(['name'=>Config::get('constants.roles.role_alumno')]);
        Role::create(['name'=>Config::get('constants.roles.role_publico')]);

    }
}
