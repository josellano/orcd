<?php
return [

    // nombre de los roles que pueden tener los usuarios.
    'roles' => [
        'role_admin' => 'admin',
        'role_decano' => 'decano',
        'role_vicedecano' => 'vicedecano',
        'role_secretario' => 'secretarioAcademico',
        'role_profesor' => 'profesor',
        'role_auxiliar' => 'auxiliar',
        'role_publico' => 'publico',
        'role_alumno' => 'alumno',
    ],

    'precargada' => [
        'precargada_item' => 'item',
        'precargada_decision' => 'decision',
    ],

    'items' => [
        'item_comun' => 'comun',
        'item_sobretabla' => 'sobretabla',
        'item_comunicacion' => 'comunicacion',
    ],

    'userPuesto' => [
        'puesto_titular' => '1',
        'puesto_suplente' => '0',
    ],

    'userTitulo' => [
        'titulo_dr' => 'Dr',
        'titulo_dra' => 'Dra',
        'titulo_ing' => 'Ing',
        'titulo_mg' => 'Mg',
        'titulo_lic' => 'Lic',
        'titulo_sr' => 'Sr',
        'titulo_sra' => 'Sra',
    ],

    'reunion' => [
        'reunion_ordinaria' => 'ordinaria',
        'reunion_extraordinaria' => 'extraordinaria',
        'reunion_constitutiva' => 'constitutiva',
    ]



];

