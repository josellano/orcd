<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menso 6 caracteres y coincidir ocn la de confirmacion.',
    'reset' => 'Tu contraseña ha sido reseteada.',
    'sent' => 'Te hemos enviado un email para resetear la contraseña.',
    'token' => 'El token de reserteo es incorrecto..',
    'user' => "No se puede encontrar un usuario con ese email.",

];
