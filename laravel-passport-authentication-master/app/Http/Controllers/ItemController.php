<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\models\Ordendia;
use App\Http\models\Item;
use App\Http\models\Reunion;
use App\Http\models\Decision;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\File;

class ItemController extends Controller
{
    /**
     * Lista los Item de una Ordendia segun su tipo.
     * Parametros: Id de la Ordendia, Nombre del tipo a filtrar.
     * Retorna coleccion de Item.
     */
    public function getItemsByType(String $ordenId, String $tipo)
    {
        $items = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', $tipo);
        $itemsOrdenado = $items->sortBy('numero')->values()->all();

        return response()->json($itemsOrdenado, 200);
    }

    /**
     * Lista los Item de una Ordendia segun su tipo con su respectiva Decision.
     * Parametros: Id de la Ordendia, Nombre del tipo a filtrar.
     * Retorna coleccion con Item y su Decision.
     */
    public function getItemsByTypeWithDecision(String $ordenId, String $tipo)
    {
        $items = DB::table('item')
            ->where('item.ordendia_id', '=', $ordenId)
            ->where('item.tipo', '=', $tipo)
            ->join('decision', 'decision_id', '=', 'decision.id')->select(['item.*', 'item.contenido as itemcontenido', 'decision.*', 'decision.contenido as decisioncontenido'])
            ->get();

        return $items;
    }


    /**
     * Crea un Item y su correspondiente Decision vacia.
     * Parametros: datos del Item.
     * Retorna Item.
     */
    public function create(ItemRequest $request)
    {
        $request->validated();

        $ordenID = $request->ordendia_id;


        $item = new Item();
        $item->ordendia_id = $ordenID;
        $item->contenido =  $request->contenido;
        $item->tipo = $this->determinarTipo($ordenID, $request->tipo);
        $numeroNuevo = $this->getLastNumero($request->ordendia_id, $item->tipo) + 1;
        $item->numero = $numeroNuevo;

        if ($request->hasFile('files')) {
            $file = $request->file('files');
            $this->uploadFile($file, $item);
        }

        $item->save();

        $nuevaDecision = $this->crearDecision($item->id);
        $item->decision_id = $nuevaDecision->id;
        $item->decision()->save($nuevaDecision);

        $ordendia =  Ordendia::find(1)->where('id', $request->ordendia_id)->first();
        $ordendia->items()->save($item);


        if ($item)
            return response()->json($item, 200);
        else
            return response()->json(['Error al crear item.'], 500);
    }

    /**
     * Determina si el item que se va a crear debera ser Sobretabla o mantener su tipo.
     * Parametros: Id de Ordendia, tipo del item que se esta creando.
     * Retorna tipo de Item.
     */
    private function determinarTipo(string $ordenId, string $tipoIngresado)
    {
        $reunionFecha = Reunion::find(1)->where('ordendia_id', $ordenId)->first()->fecha;
        $tipo = null;
        $fechaActual = Carbon::now();
        $fechaTopeItemComun = Carbon::parse($reunionFecha)->subDay();
        if ($fechaActual->greaterThanOrEqualTo($fechaTopeItemComun))
            $tipo = Config::get('constants.items.item_sobretabla');
        else
            $tipo = $tipoIngresado;
        return $tipo;
    }

    /**
     * Carga y vincula un archivo a un Item.
     * Parametros: archivo a adjuntar y Item al que se vinculara.
     * Retorna nada.
     */
    public function uploadFile(UploadedFile $file, Item $item)
    {
        $filename = $file->getClientOriginalName();
        //$extension = $file->getClientOriginalExtension();
        $nombreArchivo = date('His') . '-' . $filename;
        $file->move(public_path('adjuntos'), $nombreArchivo);

        $item->adjunto = $nombreArchivo;
    }

    /**
     * Obtiene el archivo adjunto de un Item.
     * Parametros: archivo a adjuntar y Item al que se vinculara.
     * Retorna nada.
     */
    public function verAdjunto(string $itemId)
    {
        $pdf = null;
        //$item = Item::find(1)->where('id','=', $itemId)->first();
        $item = DB::table('item')->where('id', $itemId)->first();
        if ($item->adjunto != null)
            $pdf = File::get($this->obtenerPathAdjunto($item->adjunto));

        return response($pdf, 200);
    }

    /**
     * Obtiene el path al archivo adjunto de un Item.
     * Parametros: nombre del archivo adjunto.
     * Retorna Path al archivo adjunto.
     */
    private function obtenerPathAdjunto(string $adjunto)
    {
        $nombreAdjunto = $adjunto;
        $primerCaracter = substr($nombreAdjunto, 0, 1);
        $nombreAdjuntoSlash = addcslashes($primerCaracter, $primerCaracter);
        $slash = substr($nombreAdjuntoSlash, 0, 1);
        $path = public_path('adjuntos') . $slash . $nombreAdjunto;

        return $path;
    }

    /**
     * Crea una Decision y la vincula a un Item.
     * Parametros: item_id del Item a la cual se vinculara la Decision.
     * Retorna Decision creada.
     */
    private function crearDecision(String $itemid)
    {
        $decision = new Decision();
        $decision->item_id = $itemid;
        $decision->contenido = "";
        $decision->save();

        return $decision;
    }

    /**
     * Obtiene el ultimo item segun numero de un determinado tipo.
     * Parametros: id de la Ordendia y tipo de Item.
     * Retorna Item con mayor numero.
     */
    private function getLastNumero(string $ordenId, string $tipo)
    {
        $itemFinal = DB::table('item')
            ->where('item.ordendia_id', '=', $ordenId)
            ->where('item.tipo', '=', $tipo)->max('numero');

        return $itemFinal;
    }

    /**
     * Edita un Item.
     * Parametros: datos del Item e Item a modificar.
     * Retorna mensaje resultado de operacion.
     */
    public function update(ItemRequest $request, Item $item)
    {
        $request->validated();

        $item->contenido = $request->contenido;
        $item->ordendia_id = $request->ordendia_id;

        if ($request->hasFile('files')) {
            $file = $request->file('files');
            $this->uploadFile($file, $item);
        }

        if ($item->save())
            return response()->json(['Item editado correctamente'], 200);
        else
            return response()->json(['Error al editar item'], 500);
        return null;
    }

    /**
     * Elimina el archivo adjunto de un Item.
     * Parametros: id del Item.
     * Retorna mensaje resultado de operacion.
     */
    public function eliminarAdjunto(string $itemId)
    {
        $item = Item::where('id', $itemId)->first();
        if ($item->adjunto != null) {

            $elimino = File::delete($this->obtenerPathAdjunto($item->adjunto));
            if ($elimino) {
                $item->adjunto = null;
                $item->save();
                return response()->json('Adjunto eliminado', 200);
            } else
                return response()->json(['Error al eliminar adjunto'], 500);
        }
    }

     /**
     * Agrega un archivo adjunto a un Item.
     * Parametros: datos del archivo y id del Item.
     * Retorna nada.
     */
    public function agregarAdjunto(Request $request, String $itemId)
    {
        $item = Item::find(1)->where('id', $itemId)->first();
        if ($request->hasFile('files')) {
            $file = $request->file('files');
            $filename = $file->getClientOriginalName();
            //$extension = $file->getClientOriginalExtension();
            $nombreArchivo = date('His') . '-' . $filename;
            $file->move(public_path('adjuntos'), $nombreArchivo);

            $item->adjunto = $nombreArchivo;
            $item->save();
        }
    }

    /**
     * Elimina un Item.
     * Parametros: Item a eliminar.
     * Retorna mensaje resultado de operacion.
     */
    public function destroy(Item $item)
    {
        $this->decrementarNroItem($item);
        if ($item->delete())
            return response()->json('Item eliminado', 200);
        else
            return response()->json(['Error al eliminar item'], 500);
        return null;
    }

     /**
     * Decrementa el numero de item de los items subsiguientes a un item que fue eliminado.
     * Parametros: item que sera eliminado.
     * Retorna Nada.
     */
    private function decrementarNroItem(Item $itemEliminar)
    {
        $itemsReunion = Item::all()->where('ordendia_id', $itemEliminar->ordendia_id)->where('tipo', $itemEliminar->tipo);

        $itemsOrdenado = $itemsReunion->sortBy('numero')->values()->all();

        $encontro = 0;
        foreach ($itemsOrdenado as $item => $items) {
            if (!$encontro) {
                if ($items->numero == $itemEliminar->numero) { //los items siguientes a este deberan decrementar su numero.
                    $encontro = 1;
                }
            } else {
                $items->numero = $items->numero - 1; // se aumenta el numero de los siguientes items al item encontrado.
                $items->save();
            }
        }
    }

     /**
     * Obtiene un Item.
     * Parametros: id del Item a buscar.
     * Retorna Item buscado.
     */
    public function getItemById(String $id)
    {
        $item = DB::table('item')->where('id', $id)->first();
        if ($item)
            return response()->json($item, 200);
        else
            return response()->json(['Error al buscar item'], 500);
    }

     /**
     * Incrementa el numero de un Item.
     * Parametros: id del Item.
     * Retorna mensaje resultado de operacion.
     */
    public function upItemNumero(String $id)
    {
        $item = Item::find(1)->where('id', $id)->first();
        $itemNumeroActual = $item->numero;
        $itemSiguiente = Item::find(1)->where('numero', $itemNumeroActual + 1)->first();

        if ($itemSiguiente) {
            $item->numero = $itemNumeroActual + 1;
            if (!$item->save())
                return response()->json(['Error al aumentar numero item.', 500]);

            $itemSiguiente->numero = $itemNumeroActual;
            if (!$itemSiguiente->save())
                return response()->json(['Error al aumentar numero item.', 500]);

            return response()->json(['Numero item actualizado correctamente.'], 200);
        }
        return response()->json(['Numero item no puede ser aumentado.'], 200);
    }

    /**
     * Decrementa el numero de un Item.
     * Parametros: id del Item.
     * Retorna mensaje resultado de operacion.
     */
    public function downItemNumero(String $id)
    {
        $item = Item::find(1)->where('id', $id)->first();
        $itemNumeroActual = $item->numero;
        $itemAnterior = Item::find(1)->where('numero', $itemNumeroActual - 1)->first();

        if ($itemAnterior) {
            $item->numero = $itemNumeroActual - 1;
            if (!$item->save())
                return response()->json(['Error al disminuir numero item.', 500]);

            $itemAnterior->numero = $itemNumeroActual;
            if (!$itemAnterior->save())
                return response()->json(['Error al disminuir numero item.', 500]);

            return response()->json(['Numero item actualizado correctamente.'], 200);
        }
        return response()->json(['Numero item no puede ser disminuido.'], 200);
    }
}
