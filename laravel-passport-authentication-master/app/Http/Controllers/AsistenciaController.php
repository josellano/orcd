<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\models\Asistencia;
use Illuminate\Support\Facades\DB;

class AsistenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Obtiene una asistencia por Id.
     * Parametros: id de una asistencia.
     * Retorna Asistencia o mensaje de error correspondiente.
     */
    public function getAsistenciaById(String $id)
    {
        $asistencia = DB::table('asistencia')->where('id', $id)->first();
        if ($asistencia)
            return response()->json($asistencia, 200);
        else
            response()->json(['Error al buscar asistencia'], 500);
    }

    /**
     * Obtiene el User asociado a una Asistencia.
     * Parametros: id de una asistencia.
     * Retorna asistencia o mensaje de error correspondiente.
     */
    public function getUser(String $id)
    {
        $asistencia = DB::table('asistencia')->where('id', $id)->first();
        $user = $asistencia->user();
        if ($user)
            return response()->json($asistencia, 200);
        else
            response()->json(['Error al buscar usuario.'], 500);
    }

    /**
     * Crea una Asistencia.
     * Parametros: datos de la Asistencia.
     * Retorna asistencia o mensaje de error correspondiente.
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'asistire' => 'required',
            'reunion_id' => 'required'

        ]);

        $asistencia = Asistencia::create([
            'asistire' => $request->asistire,
            'reunion_id' => $request->ruenion_id,
        ]);

        if ($asistencia)
            return response()->json($asistencia, 200);
        else
            response()->json(['Error al crear asistencia.'], 500);
    }

     /**
     * Actualiza una Asistencia.
     * Parametros: datos de la Asistencia y la Asistencia a actualizar.
     * Retorna asistencia o mensaje de error correspondiente.
     */
    public function update(Request $request, Asistencia $asistencia)
    {
        $asistencia->asistire = $request->asistire;
        $asistencia->presente = $request->presente;

        if ($asistencia->save())
            return response()->json(['Asistencia actualizada correctamente.'], 200);
        else
            return response()->json(['Error al actualizar asistencia.'], 500);
        return null;
    }

    /**
     * Elimina una Asistencia.
     * Parametros: Asistencia a eliminar.
     * Retorna mensaje de correspondiente.
     */
    public function destroy(Asistencia $asistencia)
    {
        if ($asistencia->delete())
            return response()->json('Asistencia eliminada', 200);
        else
            return response()->json(['Error al eliminar asistencia'], 500);
        return null;
    }

    /**
     * Actualiza el asisstire de una Asistencia en una reunion.
     * Parametros: id de una Asistencia.
     * Retorna asistencia actualizada.
     */
    public function actualizarAsistenciaEnReunion(string $reunionId)
    {
        $userId = auth()->user()->id;

        $asistencia = Asistencia::find(1)->where('reunion_id', $reunionId)->where('user_id', $userId)->first();

        if ($asistencia->asistire)
            $asistencia->asistire = $asistencia->asistire - 1;
        else
            $asistencia->asistire = $asistencia->asistire + 1;

        $asistencia->save();

        return response()->json($asistencia, 200);
    }

    /**
     * Actualiza el presente de una Asistencia en una reunion.
     * Parametros: id de una Asistencia.
     * Retorna Asistencia actualizada o mensaje de error correspondiente
     */
    public function actualizarPresenteEnReunion(string $asistenciaId)
    {
        $asistencia = Asistencia::find(1)->where('id', $asistenciaId)->first();

        if ($asistencia->asistire) { // si el usuario ya confirmo asistencia

            if ($asistencia->presente)
                $asistencia->presente = $asistencia->presente - 1;
            else
                $asistencia->presente = $asistencia->presente + 1;

            $asistencia->save();

            return response()->json($asistencia, 200);
        }
        else
            return response()->json(["El usuario no confirmo asistencia."], 500);
    }
}
