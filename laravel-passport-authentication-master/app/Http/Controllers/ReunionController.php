<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ReunionRequest;

use App\Http\models\Asistencia;
use App\Http\models\Reunion;
use App\Http\models\Ordendia;
use App\Http\models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Mail\ReunionEmail;
use Illuminate\Support\Facades\Mail;

class ReunionController extends Controller
{

    /**
     * Lista las reuniones existentes ordenadas por Fecha de Reunion.
     * Parametros: no hay.
     * Retorna coleccion de reuniones.
     */
    public function index()
    {
        $reuniones =  Reunion::all();
        $reunionesOrdenado = $reuniones->sortBy('fecha')->values()->all();
        return response()->json($reunionesOrdenado);
    }

    /**
     * Crea una Reunion.
     * Parametros: datos de la Reunion.
     * Retorna Reunion.
     */
    public function create(ReunionRequest $request)
    {
        $request->validated();

        $reunion = Reunion::create([
            'fecha' => $request->fecha,
            'tipo' => $request->tipo,
            'numero' => $this->obtenerNroReunion($request->fecha),
        ]);

        $miembrosConsejo = $this->getMiembrosConsejo();

        foreach ($miembrosConsejo as $miembro) {
            $reunion->users()->attach($miembro->id); //asignacion de usuarios a la reunion

            $asistenciaNueva = $this->crearAsistencia($reunion->id, $miembro->id);
            $reunion->asistencias()->save($asistenciaNueva); //asignacion de asistencias a la reunion

            $this->enviarRecordatorio($miembro,$reunion);
        }

        $ordendiaNueva = $this->crearOrdendia($reunion->id);
        $reunion->setOrdenId($ordendiaNueva->id); //asignacion de ordendia a la reunion
        $reunion->save();

        if($reunion)
            return response()->json($reunion, 200);
        else
            return response()->json(['Error al crear reunion.'], 500);
    }

     /**
     * Obtiene el numero de Reunion para la Reunion que se esta creando y actualiza el numero de las demas Reuniones de manera acorde.
     * Parametros: Fecha de la Reunion que se esta creando.
     * Retorna numero para la Reunion nueva.
     */
    private function obtenerNroReunion(string $actualReunionFecha)
    {
        $numero = null;
        $dt = Carbon::parse($actualReunionFecha);
        $anioActual = $dt->year;
        $reunionesConEseAnio = Reunion::whereYear('fecha', $anioActual)->get();

        $reunionesOrdenado = $reunionesConEseAnio->sortBy('fecha')->values()->all();

        $encontro = 0;
        $len = count($reunionesOrdenado);
        if ($len == 0) { // si no hay ninguna es la primera de ese año
            $numero = 1;
        } else {
            foreach ($reunionesOrdenado as $reunion => $items) {
                if ($encontro == 0) {
                    if ($items->fecha > $actualReunionFecha) { //si encontro uno con mayor fecha, ese sera el numero de reunion de la reunion que se esta creando
                        $numero = $items->numero;
                        $encontro = 1;
                        $items->numero = $items->numero + 1; // se aumenta el numero de la reunion mayor
                        $items->save();
                    }
                } else {
                    $items->numero = $items->numero + 1; // se aumenta el numero de las siguientes reuniones a la reyunion mayor encontrada.
                    $items->save();
                }
            }
            if ($numero == null) //si no habia ninguna menor, tiene que ser el numero de la ultima +1
                $numero = $len + 1;
        }
        return $numero;
    }

    /**
     * Decrementa el numero de Reunion de las reuniones subsiguientes a una Reunion que fue eliminada.
     * Parametros: Fecha y Numero de la Reunion eliminada.
     * Retorna Nada.
     */
    private function decrementarNroReunion(string $FechaReunionEliminada, int $numeroEliminado)
    {
        $dt = Carbon::parse($FechaReunionEliminada);
        $anioActual = $dt->year;
        $reunionesConEseAnio = Reunion::whereYear('fecha', $anioActual)->get();

        $reunionesOrdenado = $reunionesConEseAnio->sortBy('fecha')->values()->all();

        $encontro = 0;
        foreach ($reunionesOrdenado as $reunion => $items) {
            if (!$encontro) {
                if ($items->numero == $numeroEliminado) { //las reuniones siguientes a esta deberan decrementar su numero.
                    $encontro = 1;
                }
            } else {
                $items->numero = $items->numero - 1; // se aumenta el numero de las siguientes reuniones a la reunion mayor encontrada.
                $items->save();
            }
        }
    }

    /**
     * Actualiza el numero de Reunion de las reuniones de un determinado anio cuando alguna de estas es modificada.
     * Parametros: Fecha de la Reunion modificada.
     * Retorna Nada.
     */
    private function actualizarNroReunion(string $FechaReunionActualizada)
    {
        $dt = Carbon::parse($FechaReunionActualizada);
        $anioActual = $dt->year;
        $reunionesConEseAnio = Reunion::whereYear('fecha', $anioActual)->get();

        $reunionesOrdenado = $reunionesConEseAnio->sortBy('fecha')->values()->all();

        $nro = 1;
        foreach ($reunionesOrdenado as $reunion => $items) {
            $items->numero = $nro;
            $items->save();
            $nro++;
        }
    }

    /**
     * Envia un mail a un User con datos de una Reunion.
     * Parametros: User destinatario del mail y Reunion.
     * Retorna Nada.
     */
    public function enviarRecordatorio($user, $reunion)
    {
        $dt = Carbon::parse($reunion->fecha);
        $anio = $dt->year;
        $mesTraducido = $dt->locale('es')->monthName;
        $dia = $this->agregarDigito($dt->day);
        $hora = $this->agregarDigito($dt->hour);
        $minuto = $this->agregarDigito($dt->minute);

        $objDemo = new \stdClass();
        if ($user->genero)
            $objDemo->saludo = 'Estimado ' . $user->nombre . ',';
        else
            $objDemo->saludo = 'Estimada ' . $user->nombre . ',';

        $objDemo->mensaje = 'Se le notifica por este medio que ha sido incorporado a la reunión del día '
            . $dia . ' de ' . $mesTraducido . ' de ' . $anio . ' a las ' . $hora . ':' . $minuto . ' horas.';

        $objDemo->sender = 'Organizador de Reuniones del Consejo Universitario.';

        Mail::to($user->email)->send(new ReunionEmail($objDemo));
    }

    /**
     * Envia un mail a un User con datos de una Reunion reprogramada.
     * Parametros: User destinatario del mail y Reunion.
     * Retorna Nada.
     */
    public function enviarRecordatorioReprogramado($user, $reunion, $fechaVieja)
    {
        $dt = Carbon::parse($reunion->fecha);
        $anio = $dt->year;
        $mesTraducido = $dt->locale('es')->monthName;
        $dia = $this->agregarDigito($dt->day);
        $hora = $this->agregarDigito($dt->hour);
        $minuto = $this->agregarDigito($dt->minute);

        $dtVieja = Carbon::parse($fechaVieja);
        $anioVieja = $dtVieja->year;
        $mesTraducidoVieja = $dtVieja->locale('es')->monthName;
        $diaVieja = $this->agregarDigito($dtVieja->day);
        $horaVieja = $this->agregarDigito($dtVieja->hour);
        $minutoVieja = $this->agregarDigito($dtVieja->minute);

        $objDemo = new \stdClass();
        if ($user->genero)
            $objDemo->saludo = 'Estimado ' . $user->nombre . ',';
        else
            $objDemo->saludo = 'Estimada ' . $user->nombre . ',';

        $objDemo->mensaje = 'Se le notifica por este medio que la reunión del día '
            . $diaVieja . ' de ' . $mesTraducidoVieja . ' de ' . $anioVieja . ' a las ' . $horaVieja . ':' . $minutoVieja . ' horas ha sido reprogramada para el día' . $dia . ' de ' . $mesTraducido . ' de ' . $anio . ' a las ' . $hora . ':' . $minuto . ' horas.';

        $objDemo->sender = 'Organizador de Reuniones del Consejo Universitario.';

        Mail::to($user->email)->send(new ReunionEmail($objDemo));
    }

    /**
     * Agrega un digito a las fechas que son de un solo digito.
     * Parametros: numero a chequear y modificar.
     * Retorna numero modificado.
     */
    private function agregarDigito(int $numero)
    {
        $numeroString = '';
        if ($numero < 10)
            $numeroString = '0' . $numero;
        else
            $numeroString = '' . $numero;
        return $numeroString;
    }

    /**
     * Crea una Ordendia y la vincula a una Reunion.
     * Parametros: reunion_id de la Reunion a la cual se vinculara la Ordendia.
     * Retorna Ordendia creada.
     */
    private function crearOrdendia(String $reunionid)
    {
        $ordendia = new Ordendia();
        $ordendia->reunion_id = $reunionid;
        $ordendia->save();

        return $ordendia;
    }

    /**
     * Crea una Asistencia y la vincula a una Reunion y un User.
     * Parametros: reunion_id de la Reunion y user_id del User a la cual se vinculara la Asistencia.
     * Retorna Asistencia creada.
     */
    private function crearAsistencia(String $reunionid, String $miembroid)
    {
        $asistencia = new Asistencia();
        $asistencia->asistire = 0;
        $asistencia->reunion_id = $reunionid;
        $asistencia->user_id = $miembroid;
        $asistencia->save();

        return $asistencia;
    }

    /**
     * Lista los User que son miembros del consejo universitario.
     * Parametros: no hay.
     * Retorna coleccion de User.
     */
    public function getMiembrosConsejo()
    {
        $miembrosConsejo = DB::table('role_user')
            ->join('roles', 'role_id', '=', 'roles.id')->where('roles.name', '!=', Config::get('constants.roles.role_admin'))->where('roles.name', '!=', Config::get('constants.roles.role_publico'))
            ->join('users', 'user_id', '=', 'users.id')
            ->get();

        return $miembrosConsejo;
    }

    /**
     * Obtiene una Ordendia a partir de una ordendia_id.
     * Parametros: ordendia_id de la Ordendia buscada.
     * Retorna Ordendia.
     */
    public function getOrdendia(String $ordenId)
    {
        $ordendia = Ordendia::find(1)->where('reunion_id', '=', $ordenId)->first();

        if($ordendia)
            return response()->json($ordendia, 200);
        else
            return response()->json(['Error al buscar orden del dia.'], 500);
    }


    /**
     * Actualiza una reunion.
     * Parametros: datos a actualizar y Reunion a modificar.
     * Retorna Reunion Actualizada.
     */
    public function update(ReunionRequest $request, Reunion $reunion)
    {
        $request->validated();
        $reunion->users()->sync($request->users);

        $fechaViejaString = $reunion->fecha;
        $fechaVieja = Carbon::parse($reunion->fecha);
        $anioVieja = $fechaVieja->year;

        $fechaNueva = Carbon::parse($request->fecha);
        $anioNueva = $fechaNueva->year;

        $reunion->fecha = $request->fecha;
        $reunion->tipo = $request->tipo;

        if ($reunion->save()){
            $this->actualizarNroReunion($request->fecha);
            if($anioVieja!=$anioNueva)
                $this->actualizarNroReunion($fechaViejaString);

                //hay que enviar mail con el nuevo horario.
                $miembrosConsejo = $this->getMiembrosConsejo();
                foreach ($miembrosConsejo as $miembro) {
                    $this->enviarRecordatorioReprogramado($miembro,$reunion, $fechaViejaString);
                }

            return response()->json(['Reunion editada correctamente'], 200);
        }
        else
            return response()->json(['Error al editar reunion'], 500);
        return null;
    }

     /**
     * Elimina una Reunion.
     * Parametros: Reunion a eliminar.
     * Retorna Mensaje con resultado  de operacion.
     */
    public function destroy(Reunion $reunion)
    {
        $reunion->users()->detach();
        $this->decrementarNroReunion($reunion->fecha, $reunion->numero);
        if ($reunion->delete())
            return response()->json('Reunion eliminada', 200);
        else
            return response()->json(['Error al eliminar reunion'], 500);
        return null;
    }

     /**
     * Obtiene una Reunion.
     * Parametros: reunion_id de la Reunion a buscar.
     * Retorna Reunion buscada.
     */
    public function getReunionById(String $id)
    {
        $reunion = DB::table('reunion')->where('id', $id)->first();
        if ($reunion)
            return response()->json($reunion, 200);
        else
            response()->json(['Error al buscar reunion'], 500);
    }

    /**
     * Obtiene un Reunion.
     * Parametros: ordendia_id de la Ordendia de la Reunion a buscar.
     * Retorna Reunion buscada.
     */
    public function getReunionByOrdendiaId(String $id)
    {
        $reunion = DB::table('reunion')->where('ordendia_id', $id)->first();
        if ($reunion)
            return response()->json($reunion, 200);
        else
            response()->json(['Error al buscar reunion'], 500);
    }

    /**
     * Lista las asistencias segun el Rol y Puesto de los User de una Reunion.
     * Parametros: Id de la Reunion, Nombre del rol y puesto del User a filtrar.
     * Retorna coleccion de asistencias.
     */
    public function getAsistenciasByRol(String $reunionId, String $rol, String $puesto)
    {
        $asistencias = DB::table('asistencia')->where('reunion_id', '=', $reunionId)
            ->join('users', 'user_id', '=', 'users.id')->where('puesto', '=', $puesto)
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_id', '=', 'roles.id')->where('roles.name', '=', $rol)
            ->select('users.puesto', 'users.nombre', 'users.apellido', 'users.telefono', 'asistencia.asistire', 'asistencia.presente', 'asistencia.id as asistencia_id')
            ->get();

        return response()->json($asistencias);
    }

    /**
     * Calcula si una reunion tiene quorum.
     * Parametros: Id de la Reunion.
     * Retorna el quorum de Reunnion.
     */
    public function getQuorum(String $reunionId)
    {
        $asistencias = Asistencia::where('reunion_id', $reunionId)->count(); // obtengo la cantidad maxima de asistencias de la reunion
        $asistenciasConfirmadas = Asistencia::where('reunion_id', $reunionId)->where('asistire', 1)->count();// obtengo la cantidad asistencias con asistire positivo

        $hayQuorum = $asistencias / 2 + 1 <= $asistenciasConfirmadas;

        return response()->json($hayQuorum);
    }
}
