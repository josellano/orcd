<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\models\Ordendia;
use App\Http\models\Item;
use App\Http\models\Decision;
use App\Http\models\Reunion;
use App\Http\models\User;
use App\Http\models\Asistencia;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use Illuminate\Support\Facades\Config;

class PDFController extends Controller
{
    /**
     * Crea el PDF de una Ordendia.
     * Parametros: id de la Ordendia.
     * Retorna Ordendia en formato PDF.
     */
    public function downloadOrdenPDF(string $ordenId)
    {
        $pdf = app('dompdf.wrapper');
        $ordendiaPDF = $this->armarOrdendia($ordenId);
        $pdf->loadHTML($ordendiaPDF);

        return $pdf->download('Ordendia.pdf');
    }

    /**
     * Crea el PDF de un acta de una Ordendia.
     * Parametros: id de la Ordendia.
     * Retorna acta de una Ordendia en formato PDF.
     */
    public function downloadActaPDF(string $ordenId)
    {
        $pdf = app('dompdf.wrapper');
        $acta = $this->armarActa($ordenId);
        $pdf->loadHTML($acta);

        return $pdf->download('Acta.pdf');
    }

    /**
     * Elimina caracteres indeseados en el HTML.
     * Parametros: texto a limpiar.
     * Retorna: texto sin caracteres indeseados.
     */
    private function removerOcultos(string $texto){
        $textoSinOcultos = preg_replace('/[\x00-\x1F\x7F\xA0]/u', '', $texto);
        return  $textoSinOcultos;
    }

    /**
     * Arma el HTML de un Ordendia.
     * Parametros: id de la Ordendia.
     * Retorna: formato HTML del Ordendia.
     */
    private function armarOrdendia(string $ordenId)
    {
        $reunion = Reunion::find(1)->where('ordendia_id', '=', $ordenId)->first();
        $ordendia = Ordendia::find(1)->where('id', '=', $ordenId)->first();
        $itemsComunes = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', Config::get('constants.items.item_comun'));
        $itemsSobretabla = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', Config::get('constants.items.item_sobretabla'));
        $itemsComunicaciones = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', Config::get('constants.items.item_comunicacion'));

        $html = '<!doctype html>';
        $html .= '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . $this->concatenarCSS() . '</head>';
        $html .= '<body>';
        $html .= '<header>
                    <img src="encabezadoPDF.PNG" alt="encabezado-PDF" border="0">
                  </header>';
        $html .= '<footer> </footer>';
        $html .= '<main>';
        $html .= '<div class="user-div">';

        $html .= '<div class="headerOfComponent">
                            <h1 class="display-4">' . $ordendia->titulo . '</h1>
                        </div>';

        $html .= $this->concatenarIntroduccionOrdendia($reunion);


        $html .= '<div class="textoIzquierda">';
        foreach ($itemsComunes as $item) {

            $html .= ' <div class="div-contenido">';
                $html .= '<div class="contenido-item">';
                $html .= '<div class="contenido-numero">'.$item->numero . '. </div><div class="contenido-item-texto">' . $this->removerOcultos($item->contenido).'</div>';
                $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>'; //textoIzqueirda

        $html .= '<div class="textoIzquierdaNegrita">' . $ordendia->separadorSobretabla . '</div>';

        $html .= '<div class="textoIzquierda">';
        foreach ($itemsSobretabla as $item) {
            $html .= ' <div class="div-contenido">';
                $html .= '<div class="contenido-item">';
                $html .= '<div class="contenido-numero">'.$item->numero . '. </div><div class="contenido-item-texto">' . $this->removerOcultos($item->contenido).'</div>';
                $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>'; //textoIzqueirda

        $html .= '<div class="textoIzquierdaNegrita">' . $ordendia->separadorComunicaciones . '</div>';

        $html .= '<div class="textoIzquierda">';
        foreach ($itemsComunicaciones as $item) {
            $html .= ' <div class="div-contenido">';
                $html .= '<div class="contenido-item">';
                $html .= '<div class="contenido-numero">'.$item->numero . '. </div><div class="contenido-item-texto">' . $this->removerOcultos($item->contenido).'</div>';
                $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>'; //textoIzqueirda

        $html .= '</div>'; //de user-div
        $html .= '</main>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    /**
     * Arma el HTML de un acta de una Ordendia.
     * Parametros: id de la Ordendia.
     * Retorna: formato HTML del acta de una Ordendia.
     */
    private function armarActa(string $ordenId)
    {
        $reunion = Reunion::find(1)->where('ordendia_id', '=', $ordenId)->first();
        $ordendia = Ordendia::find(1)->where('id', '=', $ordenId)->first();
        $itemsComunes = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', Config::get('constants.items.item_comun'));
        $itemsSobretabla = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', Config::get('constants.items.item_sobretabla'));
        $itemsComunicaciones = Item::all()->where('ordendia_id', '=', $ordenId)->where('tipo', '=', Config::get('constants.items.item_comunicacion'));

        //para el titulo
        $dt = Carbon::parse($reunion->fecha,3);
        $anio = $dt->year;

        $html = '';
        $html .= '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . $this->concatenarCSS() . '</head>';
        $html .= '<body>';
        $html .= '<header>
                        <img src="encabezadoPDF.PNG" alt="encabezado-PDF" border="0">
                    </header>';

        $html .= '<footer></footer>';
        $html .= '<main>';
        $html .= '<div class="user-div">';

        $html .= '<div class="headerOfComponent">
                              <h1 class="display-4">Acta '.$reunion->numero.'/'.substr( $anio, -2).'</h1>
                         </div>';

        $html .= $this->concatenarIntroduccionActa($reunion);

        $html .= '<div class="textoIzquierda">';
        foreach ($itemsComunes as $item) {
            $decision = Decision::find(1)->where('decision.id', '=', $item->decision_id)->first();

            $html .= ' <div class="div-contenido">';
            $html .= '<div class="contenido-item">';
            $html .= '<div class="contenido-numero">'.$item->numero . '. </div><div class="contenido-item-texto">' . $this->removerOcultos($item->contenido).'</div>';
            $html .= '</div>';
            $html .= '</div>';

            $html .= '<div class="div-decision">';
            $html .= '<div class="contenido-decision">';
            $html .= 'Respuesta: ' . $this->removerOcultos($decision->contenido);
            $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>'; //textoIzqueirda

        $html .= '<div class="textoIzquierdaNegrita">' . $ordendia->separadorSobretabla . '</div>';

        $html .= '<div class="textoIzquierda">';
        foreach ($itemsSobretabla as $item) {
            $decision = Decision::find(1)->where('decision.id', '=', $item->decision_id)->first();

            $html .= ' <div class="div-contenido">';
            $html .= '<div class="contenido-item">';
            $html .= '<div class="contenido-numero">'.$item->numero . '. </div><div class="contenido-item-texto">' . $this->removerOcultos($item->contenido).'</div>';
            $html .= '</div>';
            $html .= '</div>';

            $html .= '<div class="div-decision">';
            $html .= '<div class="contenido-decision">';
            $html .= 'Respuesta: ' . $this->removerOcultos($decision->contenido);
            $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>'; //textoIzqueirda

        $html .= '<div class="textoIzquierdaNegrita">' . $ordendia->separadorComunicaciones . '</div>';

        $html .= '<div class="textoIzquierda">';
        foreach ($itemsComunicaciones as $item) {
            $decision = Decision::find(1)->where('decision.id', '=', $item->decision_id)->first();

            $html .= ' <div class="div-contenido">';
            $html .= '<div class="contenido-item">';
            $html .= '<div class="contenido-numero">'.$item->numero . '. </div><div class="contenido-item-texto">' . $this->removerOcultos($item->contenido).'</div>';
            $html .= '</div>';
            $html .= '</div>';

            $html .= '<div class="div-decision">';
            $html .= '<div class="contenido-decision">';
            $html .= 'Respuesta: ' . $this->removerOcultos($decision->contenido);
            $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>'; //textoIzqueirda

        $html .= '</div>'; //de user-div
        $html .= '</main>';
        $html .= '</body>';

        return $html;
    }

    /**
     * Arma el HTML de la introduccion de una Ordendia.
     * Parametros: Reunioni a la que pertenece el Ordendia.
     * Retorna: formato HTML de la introduccion de la Ordendia.
     */
    public function concatenarIntroduccionOrdendia(Reunion $reunion)
    {
        $dt = Carbon::parse($reunion->fecha);
        $anio = $dt->year;
        $mesTraducido = $dt->locale('es')->monthName;
        $dia = $this->agregarDigito($dt->day);
        $hora = $this->agregarDigito($dt->hour);
        $minuto = $this->agregarDigito($dt->minute);

        $html = '<div class="textoCentrado">
                    Reunión <b>' . $reunion->tipo . '</b> de Consejo Departamental, del dia <b>' . $dia . '</b> de <b>' . $mesTraducido . '</b> de <b>' . $anio . '</b> a las <b>' . $hora . ':' . $minuto . ' horas.</b>
                </div>';

        return $html;
    }

    /**
     * Arma el HTML de la introduccion del acta de una Ordendia.
     * Parametros: Reunioni a la que pertenece el Ordendia.
     * Retorna: formato HTML de la introduccion del acta de una Ordendia.
     */
    public function concatenarIntroduccionActa(Reunion $reunion)
    {
        $preside = $this->getPresidente($reunion);

        $dt = Carbon::parse($reunion->fecha,3);
        $anio = $dt->year;
        $mesTraducido = $dt->locale('es')->monthName;
        $dia = $this->agregarDigito($dt->day);
        $hora = $this->agregarDigito($dt->hour);
        $minuto = $this->agregarDigito($dt->minute);

        $html = '<div class="textoJustificado">
              En la ciudad de Bahía Blanca, a los ' . $dia . ' días del mes de ' . $mesTraducido . ' de ' . $anio . ' y siendo las ' . $hora . ':' . $minuto . ' horas, se realiza la reunión ' . $reunion->tipo . ' del Consejo Departamental de Ciencias e Ingeniería de la Computación, bajo la presidencia del '. $preside->titulo_academico.'. '.$preside->nombre.' '.$preside->apellido.'
              y con la presencia de '.$this->getMiembrosConsejo($reunion->id, $preside->id).'
            </div>';

        return $html;
    }

     /**
     * Agrega un digito a las fechas que son de un solo digito.
     * Parametros: numero a chequear y modificar.
     * Retorna numero modificado.
     */
    public function agregarDigito(int $numero){
        $numeroString = '';
        if ($numero < 10)
          $numeroString = '0'.$numero;
        else
        $numeroString = ''.$numero;
        return $numeroString;
    }

    /**
     * Lista los User que son miembros del consejo universitario que estuvieron presentes en la reunion.
     * Parametros: Id de la reunion.
     * Retorna coleccion de User.
     */
    private function getMiembrosConsejo(string $reunionid, string $presidenteId){
        $html='';

        $users = DB::table('asistencia')->where('asistencia.reunion_id', '=', $reunionid)->where('asistencia.presente', '=', 1)
        ->join('users', 'user_id', '=', 'users.id')->where('users.id', '!=', $presidenteId)
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'role_id', '=', 'roles.id')->where('roles.name', '!=', Config::get('constants.roles.role_admin'))->where('roles.name', '!=', Config::get('constants.roles.role_publico'))->where('roles.name', '!=', Config::get('constants.roles.role_decano'))->where('roles.name', '!=', Config::get('constants.roles.role_vicedecano'))
        ->select('users.nombre', 'users.apellido', 'users.titulo_academico', 'users.genero')
       ->get()->unique();

        $i = 0;
        $len = count($users);
        foreach ($users as $user => $items) {
            if($items->genero)
                $html.='el ';
            else
                $html.='la ';

            $html.=$items->titulo_academico.'. ';
            $html.=$items->nombre.' ';

            if( $i == $len-2) // agregar el conector y para el ultimo miembro del consejo.
                $html.=$items->apellido.' y ';
            else
                if( $i < $len-1)
                    $html.=$items->apellido.', '; // separa cada miembro con ,
                else
                    $html.=$items->apellido.'.'; //el ultimo miembro coloca un .

            $i++;
        }
        return $html;
    }

/**
     * Obtiene al USer que peside la Reunion.
     * Parametros: no tiene.
     * Retorna User que preside la reunion.
     */
    private function getPresidente(Reunion $reunion){
        $preside = $this->getDecanoConsejo();
        $presenteDecano = Asistencia::find(1)->where('reunion_id', '=', $reunion->id)->where('user_id', '=', $preside->id)->first();

        // Si el decano no se enceuntra presente, el vice debera estarlo.
        if(!$presenteDecano->presente)
            $preside = $this->getVicedecanoConsejo();
        return $preside;
    }

    /**
     * Obtiene al Decano del Consejo Universitario.
     * Parametros: no tiene.
     * Retorna el User que es decano del consejo.
     */
    private function getDecanoConsejo(){
        $userDecanoId = DB::table('role_user')
        ->join('roles', 'role_id', '=', 'roles.id')->where('roles.name', '=', Config::get('constants.roles.role_decano'))
        ->join('users', 'user_id', '=', 'users.id')
        ->select('users.id')
        ->get();

        $decano = User::find(1)->where('id', '=', $userDecanoId[0]->id)->first();

        return $decano;
    }

    /**
     * Obtiene al Vicedecano del Consejo Universitario
     * Parametros: no tiene.
     * Retorna el User que es vicedecano del consejo.
     */
    private function getVicedecanoConsejo(){
        $userVicedecanoId = DB::table('role_user')
        ->join('roles', 'role_id', '=', 'roles.id')->where('roles.name', '=', Config::get('constants.roles.role_vicedecano'))
        ->join('users', 'user_id', '=', 'users.id')
        ->select('users.id')
        ->get();

        $viceDecano = User::find(1)->where('id', '=', $userVicedecanoId[0]->id)->first();

        return $viceDecano;
    }

    /**
     * Concatena los estilos del HTML.
     * Parametros: no tiene.
     * Retorna los estilos en un string.
     */
    public function concatenarCSS()
    {
        $html = '
        <style>
            @page {
                margin: 0cm 0cm;
                font-family: Arial;
            }

            body {
                margin-top: 3cm;
                margin-left: 0cm;
                margin-right: 0cm;
                margin-bottom: 3cm;
                padding-top: 1%;
            }

            main {
                margin: 0cm 0cm 0cm;
            }

            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;
                background-color: white;
                text-align: center;
                line-height: 30px;
            }

            .img{
                width="100%";
                height="100%"
            }

            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;
                background-color: white;
                text-align: center;
                line-height: 1.5cm;
            }

            p {
                margin-top: 0px;
                margin-bottom: 0px;
                padding: 0px;
                display: inline;
             }

            .headerOfComponent {
                padding: 0;
                margin: 0px;
                //margin-top: 1%;
                margin-bottom: 0px;
                padding-left: 5%;
                padding-right: 5%;
            }

            .display-4{
                margin: 0px;
            }

            .h1{
                margin: 0px;
                margin-top: 0px;
                margin-bottom: 0px;
            }

            .div-contenido{
                margin: 0;
                padding: 0;
                display: flex;
            }

            .div-decision{
                font-style: italic;
            }

            .div-contenido p {
                margin-top: 0px;
                margin-bottom: 0px;
                padding: 0px;
            }

            .div-decision p {
                margin-top: 0px;
                margin-bottom: 0px;
                padding: 0px;
            }

            .div-contenido ul,ol {
                margin-block-start: 0px;
                margin-block-end: 0px;
            }

            .div-decision ul,ol {
                margin-top: 0px;
                margin-bottom: 0px;
                //padding: 0px;
               // margin-block-start: 0px;
               // margin-block-end: 0px;
            }

            .div-decision li {
                margin-top: 0px;
                margin-bottom: 0px;
              //  padding: 0px;
              //  margin-block-start: 0px;
              //  margin-block-end: 0px;
            }

            .contenido-item{
                padding-left: 1%;
            }

            .contenido-item-texto{
                padding-left: 4%;
            }

            .contenido-numero{
                width: initial;
            }

            .contenido-decision{
                padding-left: 6%;
            }

            .contenido-item p {
                margin-top: 0px;
                margin-bottom: 0px;
                padding: 0px;
                margin-block-start: 0px;
                margin-block-end: 0px;
            }

            .contenido-item p {
                margin-top: 0px;
                margin-bottom: 0px;
                padding: 0px;
                margin-block-start: 0px;
                margin-block-end: 0px;
            }

            .contenido-decision p {
                margin-top: 0px;
                margin-bottom: 0px;
                padding: 0px;
                margin-block-start: 0px;
                margin-block-end: 0px;

            }

            .div-contenido ol {
                margin-block-start: 0px;
                margin-block-end: 0px;
            }

            ul,li {
                margin-top: 0px;
                margin-bottom: 0px;
                margin-block-start: 0px;
                margin-block-end: 0px;
            }

            .user-div{
                margin: 0;
                padding: 0;
                text-align: center;
                margin-block-start: 0px;
                margin-block-end: 0px;
            }

            .textoCentrado{
                text-align: center;
                padding: 5%;
                padding-top: 1%;
                padding-bottom: 1%;
            }

            .textoJustificado{
                text-align: justify;
                padding: 5%;
                padding-top: 1%;
                padding-bottom: 1%;
            }

            .textoIzquierda{
                text-align: left;
                padding: 5%;
                padding-top: 1%;
                padding-bottom: 1%;
            }

            .textoIzquierdaNegrita{
                text-align: left;
                font-weight: bolder;
                padding: 5%;
                padding-top: 1%;
                padding-bottom: 0;
            }
            </style>
            ';

            return $html;
        }
    }
