<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Controllers\Controller;
use App\Http\models\User;
use App\Http\models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Lista los User.
     * Parametros: no hay.
     * Retorna coleccion de User.
     */
    public function index()
    {
        if (Gate::denies('manage-users'))
            return response()->json(['UnAuthorised'], 401);

        $users = User::all();
        return response()->json($users);
    }

    /**
     * Obtiene los detalles del User logueado.
     * Parametros: no hay.
     * Retorna User.
     */
    public function details()
    {
        $user = auth()->user();
        return response()->json($user, 200);
    }

    /**
     * Obtiene un User.
     * Parametros: id del User a buscar.
     * Retorna User buscado.
     */
    public function getUserById(String $id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        if ($user)
            return response()->json($user, 200);
        else
            response()->json(['Error al buscar usuario'], 500);
    }

    /**
     * Obtiene los Roles del User logueado.
     * Parametros: no hay.
     * Retorna colleccion de Role.
     */
    public function roles()
    {
        $userId = auth()->user()->id;

        $roles = DB::table('role_user')
            ->join('roles', 'role_id', '=', 'roles.id')
            ->join('users', 'user_id', '=', 'users.id')->where('users.id', '=', $userId)
            ->select('roles.name')
            ->get();

        return response()->json($roles, 200);
    }

    /**
     * Obtiene los Roles de un User.
     * Parametros: id del User.
     * Retorna colleccion de Role.
     */
    public function GetRolesUser(string $userId)
    {
        $roles = DB::table('role_user')
            ->join('roles', 'role_id', '=', 'roles.id')
            ->join('users', 'user_id', '=', 'users.id')->where('users.id', '=', $userId)
            ->selectRaw('roles.name')
            ->get();

        return response()->json($roles, 200);
    }

    /**
     * Obtiene la Asistencia del User logueado para un Reunion.
     * Parametros: id de la Reunion.
     * Retorna AsistenciaS.
     */
    public function getAsistenciaEnReunion(string $reunionId)
    {
        $userId = auth()->user()->id;

        $asistencia = DB::table('asistencia')
            ->where('user_id', '=', $userId)
            ->where('reunion_id', '=', $reunionId)
            ->get();

        return response()->json($asistencia, 200);
    }

    /**
     * Actualiza un User.
     * Parametros: datos a actualizar y User a modificar.
     * Retorna User Actualizada.
     */
    public function edit(User $user)
    {
        return null;
    }

    /**
     * Actualiza un User.
     * Parametros: datos a actualizar y User a modificar.
     * Retorna User Actualizado.
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $request->validated();
        $ids_roles = [];

        $roles = $request->roles;
        foreach ($roles as $rol) {
            $role = Role::select('id')->where('name', $rol)->first();
            $ids_roles[] = $role->id;
        }

        $user->roles()->sync($ids_roles);

        $user->nombre = $request->nombre;
        $user->email = $request->email;
        $user->puesto = $request->puesto;
        $user->telefono = $request->telefono;
        $user->apellido = $request->apellido;
        $user->genero = $request->genero;
        $user->titulo_academico = $request->titulo_academico;

        $roles = $request->roles;

        if ($user->save())
            return response()->json(['Usuario editado correctamente'], 200);
        else
            return response()->json(['Error al editar usuario'], 500);
        return null;
    }

    /**
     * Elimina un User.
     * Parametros: User a eliminar.
     * Retorna Mensaje con resultado  de operacion.
     */
    public function destroy(User $user)
    {
        $user->roles()->detach();
        if ($user->delete())
            return response()->json('Usuario eliminado', 200);
        else
            return response()->json(['Error al eliminar usuario'], 500);
        return null;
    }
}
