<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\models\ComentarioDecision;
use App\Http\Requests\ComentarioDecisionRequest;
use Illuminate\Support\Facades\DB;

class ComentarioDecisionController extends Controller
{
    /**
     * Lista los ComentariosDecision de un Item.
     * Parametros: Id del Item.
     * Retorna coleccion de ComentarioDecision.
     */
    public function getComentariosDecisionItem(String $itemId)
    {
        $comentariosDecision = DB::table('comentarioDecision')->where('item_id', $itemId)
            ->join('users', 'user_id', '=', 'users.id')
            ->select('users.nombre', 'users.apellido', 'comentarioDecision.*')
            ->get();

        $comentariosDecisionOrdenado = $comentariosDecision->sortBy('id');

        return $comentariosDecisionOrdenado;
    }

    /**
     * Lista los ComentariosDecision de un Item de un User.
     * Parametros: Id del Item.
     * Retorna coleccion de ComentarioDecision.
     */
    public function getComentariosDecisionItemUser(String $itemId)
    {
        $user_id = auth()->user()->id;
        $comentariosDecision = DB::table('comentarioDecision')->where('item_id', $itemId)
            ->join('users', 'user_id', '=', 'users.id')->where('user_id', $user_id)
            ->select('users.nombre', 'users.apellido', 'comentarioDecision.*')
            ->get();

        $comentariosDecisionOrdenado = $comentariosDecision->sortBy('id');

        return $comentariosDecisionOrdenado;
    }

    /**
     * Crea un ComentarioDecision.
     * Parametros: datos del ComentarioDecision.
     * Retorna ComentarioDecision.
     */
    public function create(ComentarioDecisionRequest $request)
    {
       $request->validated();

        $user_id = auth()->user()->id;

        $comentarioDecision = new ComentarioDecision();
        $comentarioDecision->item_id = $request->item_id;
        $comentarioDecision->user_id = $user_id;
        $comentarioDecision->contenido =  $request->contenido;
        $comentarioDecision->save();

        return response()->json($comentarioDecision, 200);
    }

    /**
     * Actualzia un ComentarioDecision.
     * Parametros: datos del ComentarioDecision a modificar y el ComentarioDecision a actualizar.
     * Retorna ComentarioDecision.
     */
    public function update(ComentarioDecisionRequest $request, ComentarioDecision $comentarioDecision)
    {
        $request->validated();

        $comentarioDecision->contenido =  $request->contenido;

        if ($comentarioDecision->save())
            return response()->json(['Comentario editado correctamente.'], 200);
        else
            return response()->json(['Error al editar comentario.'], 500);
        return null;
    }

    /**
     * Elimina un ComentarioDecision.
     * Parametros: ComentarioDecision a eliminar.
     * Retorna mensaje resultado de operacion.
     */
    public function destroy(ComentarioDecision $comentarioDecision)
    {
        if ($comentarioDecision->delete())
            return response()->json('Comentario eliminado.', 200);
        else
            return response()->json(['Error al eliminar comentario.'], 500);
        return null;
    }

    /**
     * Obtiene un ComentarioDecision.
     * Parametros: comentarioDecision_id del ComentarioDecision a buscar.
     * Retorna ComentarioDecision buscada.
     */
    public function getComentarioById(String $comentarioId)
    {
        $comentarioDecision = DB::table('comentarioDecision')->where('id', $comentarioId)->first();
        if ($comentarioDecision)
            return response()->json($comentarioDecision, 200);
        else
            return response()->json(['Error al buscar comentario.'], 500);
    }

    /**
     * Lista los ComentariosDecision de un Item.
     * Parametros: Id del Item.
     * Retorna coleccion de ComentarioDecision.
     */
    public function getCantidadComentariosDecisionItem(String $itemId)
    {
        $cantidadComentariosDecision = DB::table('comentarioDecision')->where('item_id', $itemId)->count();

        return $cantidadComentariosDecision;
    }
}
