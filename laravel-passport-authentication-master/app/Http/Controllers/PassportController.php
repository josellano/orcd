<?php

namespace App\Http\Controllers;

use App\Http\models\User;
use App\Http\models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Exception;

class PassportController extends Controller
{
    /**
     * Registra un User nuevo.
     * Paramtres: datos del User.
     * Retorna: token de acceso del User creado.
     */
    public function register(UserRequest $request)
    {
        $request->validated();
        $user = User::create([
            'nombre' => $request->nombre,
            'email' => $request->email,
            'puesto' => $request->puesto,
            'telefono' => $request->telefono,
            'apellido' => $request->apellido,
            'genero' => $request->genero,
            'titulo_academico' => $request->titulo_academico,
            'password' => bcrypt($request->password)
        ]);

        $roles = $request->roles;

        foreach ($roles as $rol) {
            $role = Role::select('id')->where('name', $rol)->first();
            $user->roles()->attach($role);
        }

        $token = $user->createToken('TutsForWeb')->accessToken;

        if ($user)
            return response()->json([$token], 200);
        else
            return response()->json(['Error al crear usuario'], 401);
    }

    /**
     * Inicio de sesion de un User.
     * Paramtres: datos de acceso del User.
     * Retorna: token de acceso del User creado.
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            return response()->json($token, 200);
        } else
            return response()->json(['UnAuthorised'], 401);
    }
}
