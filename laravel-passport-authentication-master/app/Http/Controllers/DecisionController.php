<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\models\Decision;
use App\Http\models\Item;

use Illuminate\Support\Facades\DB;

class DecisionController extends Controller
{
    /**
     * Crea una Decision.
     * Parametros: datos de la Decision.
     * Retorna Decision.
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'contenido' => 'required|min:1',
            'item_id' => 'required',
        ]);

        $decision = new Decision();
        $decision->item_id = $request->item_id;
        $decision->contenido =  $request->contenido;
        $decision->save();

        $item =  Item::find(1)->where('id', $request->item_id)->first();
        $item->decision()->save($decision);


        if ($decision)
            return response()->json($decision, 200);
        else
            return response()->json(['Error al crear decision.'], 500);
        return null;
    }

    /**
     * Edita una Decision.
     * Parametros: datos de la Decision y Decision a modificar.
     * Retorna mensaje resultado de operacion.
     */
    public function update(Request $request, Decision $decision)
    {
        $decision->contenido = $request->contenido;
        $decision->item_id = $request->item_id;

        if ($decision->save())
            return response()->json(['Decision editada correctamente.'], 200);
        else
            return response()->json(['Error al editar decision.'], 500);
        return null;
    }

    /**
     * Elimina una Decision.
     * Parametros: Decision a eliminar.
     * Retorna mensaje resultado de operacion.
     */
    public function destroy(Decision $decision)
    {
        if ($decision->delete())
            return response()->json('Decision eliminada.', 200);
        else
            return response()->json(['Error al eliminar decision.'], 500);
        return null;
    }

    /**
     * Obtiene una Decision.
     * Parametros: decision_id de la Decision a buscar.
     * Retorna Decision buscada.
     */
    public function getDecisionById(String $id)
    {
        $decision = DB::table('decision')->where('id', $id)->first();
        if ($decision)
            return response()->json($decision, 200);
        else
            return response()->json(['Error al buscar decision.'], 500);
    }
}
