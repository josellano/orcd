<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PrecargadaRequest;
use App\Http\models\Precargada;
use Illuminate\Support\Facades\DB;

class PrecargadaController extends Controller
{
    /**
     * Lista Precargada segun su tipo.
     * Parametros: Nombre del tipo a filtrar.
     * Retorna coleccion de Precargada.
     */
    public function getPrecargadasByType(String $tipo)
    {
        $precargadas = DB::table('precargada')
            ->where('precargada.tipo', '=', $tipo)
            ->get();

        return $precargadas;
    }

    /**
     * Crea una Precargada
     * Parametros: datos de la Precargada.
     * Retorna Precargada.
     */
    public function create(PrecargadaRequest $request)
    {
        $request->validated();

        $precargada = new Precargada();
        $precargada->tipo = $request->tipo;
        $precargada->contenido =  $request->contenido;
        $precargada->save();

        if ($precargada->save())
            return response()->json($precargada, 200);
        else
            return response()->json(['Error al crear precargada.'], 500);
    }

    public function update(PrecargadaRequest $request, Precargada $precargada)
    {
        $request->validated();

        $precargada->tipo = $request->tipo;
        $precargada->contenido =  $request->contenido;

        if ($precargada->save())
            return response()->json(['Contenido precargado editado correctamente.'], 200);
        else
            return response()->json(['Error al editar precargada.'], 500);
        return null;
    }

    /**
     * Elimina una Precargada.
     * Parametros: Precargada a eliminar.
     * Retorna mensaje resultado de operacion.
     */
    public function destroy(Precargada $precargada)
    {
        if ($precargada->delete())
            return response()->json('Contenido precargado eliminado.', 200);
        else
            return response()->json(['Error al eliminar el Contenido precargado.'], 500);
        return null;
    }

    /**
     * Obtiene una Precargada.
     * Parametros: id de la Precargada a buscar.
     * Retorna Precargada buscada.
     */
    public function getPrecargadaById(String $id)
    {
        $precargada = DB::table('precargada')->where('id', $id)->first();
        if ($precargada)
            return response()->json($precargada, 200);
        else
            return response()->json(['Error al buscar el contenido precargado.'], 500);
    }
}
