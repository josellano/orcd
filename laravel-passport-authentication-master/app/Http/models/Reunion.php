<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Reunion extends Model
{
    protected $table = 'reunion';

    protected $fillable = [
        'fecha', 'tipo', 'numero',
    ];

    protected $casts = [
        'fecha' => 'Y-m-d H:i:s'
    ];

    public function getFecha($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('date:hh:mm');
    }

    public function setDate($date)
    {
        $this->fecha = strtotime("$date");
    }

    public function setOrdenId($ordendiaId)
    {
        $this->ordendia_id = $ordendiaId;
    }


    public function asistencias()
    {
        return $this->hasMany('App\Http\models\Asistencia');
    }

    public function users()
    {
        return $this->belongsToMany('App\Http\models\User');
    }

    public function ordendia()
    {
        return $this->hasOne('App\Http\models\Ordendia');
    }


}
