<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ordendia extends Model
{
    protected $table = "ordendia";

    protected $titulo;
    protected $separadorSobretabla;
    protected $separadorComunicaciones;

    protected $attributes  = [
        'titulo' => "Orden del día",
        'separadorSobretabla' => "Items Sobretabla",
        'separadorComunicaciones' => "Comunicaciones ingresadas",
     ];

    public function items()
    {
        return $this->hasMany('App\Http\models\Item');
    }

    public function reunion()
    {
        return $this->belongsTo('App\Http\models\Reunion');
    }


}
