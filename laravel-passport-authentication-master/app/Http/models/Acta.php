<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class Acta extends Model
{
    protected $table = "acta";

    protected $fillable = [
        'introduccion',
    ];

    protected $attributes  = [
        'introduccion' => "",
     ];

    public function reunion()
    {
        return $this->belongsTo('App\Http\models\Reunion');
    }


}
