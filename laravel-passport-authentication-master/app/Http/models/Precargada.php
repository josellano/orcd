<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class Precargada extends Model
{
    protected $table = "precargada";

    protected $fillable = [
        'contenido', 'tipo',
    ];
}
