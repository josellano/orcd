<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\Http\models\User');
    }
}
