<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{

    protected $table = "asistencia";

    protected $fillable = [
        'asistire', 'presente',
    ];

    protected $casts = [
        'asistire'  =>  'boolean',
        'presente'  =>  'boolean'
    ];

    protected $attributes  = [
        'asistire' => 0,
        'presente' => 0,
     ];

    public function reunion()
    {
        return $this->belongsTo('App\Http\models\Reunion');
    }

    public function user()
    {
        return $this->belongsTo('App\Http\models\User');
    }

}
