<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class ComentarioDecision extends Model
{
    protected $table = "comentarioDecision";

    protected $fillable = [
        'contenido',
    ];

    public function decision()
    {
        return $this->belongsTo('App\Http\models\Decision');
    }

    public function user()
    {
        return $this->belongsTo('App\Http\models\User');
    }
}
