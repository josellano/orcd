<?php

namespace App\Http\models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use App\Notifications\PasswordResetNotification;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'email', 'puesto', 'telefono', 'password', 'genero' , 'titulo_academico',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'puesto'  =>  'boolean',
        'genero'  =>  'boolean'
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Http\models\Role');
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first())
            return true;

        return false;
    }

    public function hasAnyRoles($roles)
    {
        if ($this->roles()->whereIn('name', $roles)->first()) {
        }
        return true;
        return false;
    }

    public function asistencias()
    {
        return $this->hasMany('App\Http\models\Asistencia');
    }

    public function reuniones()
    {
        return $this->belongsToMany('App\Http\models\Reunion');
    }

    public function comentariosDecision()
    {
        return $this->hasMany('App\Http\models\ComentariosDecision');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }
}
