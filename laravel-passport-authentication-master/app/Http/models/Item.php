<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "item";

    protected $numero;

    protected $fillable = [
        'contenido', 'tipo', 'numero', 'adjunto'
    ];


    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    public function setOrdenId($ordendiaId)
    {
        $this->ordendia_id = $ordendiaId;
    }

    public function ordendia()
    {
        return $this->belongsTo('App\Http\models\Ordendia');
    }

    public function setDecisionId($decisionId)
    {
        $this->decision_id = $decisionId;
    }

    public function decision()
    {
        return $this->hasOne('App\Http\models\Decision');
    }
}
