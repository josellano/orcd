<?php

namespace App\Http\models;

use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    protected $table = "decision";

    protected $fillable = [
        'contenido',
    ];

    public function setItemId($itemId)
    {
        $this->item_id = $itemId;
    }

    public function item()
    {
        return $this->belongsTo('App\Http\models\Item');
    }

    public function comentarioDecision()
    {
        return $this->hasOne('App\Http\models\ComentarioDecision');
    }
}
