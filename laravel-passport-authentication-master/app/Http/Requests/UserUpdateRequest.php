<?php

namespace App\Http\Requests;

use App\Http\models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Config;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->getUserByEmail($this->request->get('email'));

        return [
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => ['required', Rule::unique('users','email')->ignore($id,'id')], //id que ignora para ignorarse a si mismo al actualizar.
            'telefono' => 'required|min:6',
            'puesto' => ['required', 'boolean'],
            'genero' => ['required', 'boolean'],
            'titulo_academico' => ['required', Rule::in([Config::get('constants.userTitulo.titulo_dr'), Config::get('constants.userTitulo.titulo_dra'), Config::get('constants.userTitulo.titulo_ing'), Config::get('constants.userTitulo.titulo_lic'), Config::get('constants.userTitulo.titulo_sr'), Config::get('constants.userTitulo.titulo_sra')])],
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Nombre es requerido.',
            'apellido.required' => 'Apellido es requerido.',
            'email.required' => 'Email es requerido.',
            'email.unique' => 'Email ya esta en uso.',
            'telefono.required' => 'Telefono es requerido.',
            'telefono.min' => 'Telefono debe tener minimo 6 digitos.',
            'puesto.required' => 'Puesto es requerido.',
            'puesto.boolean' => 'Puesto desconocido.',
            'genero.required' => 'Genero es requerido.',
            'genero.boolean' => 'Genero desconocido.',
            'titulo_academico.required' => 'Titulo academico es requerido.',
            'titulo_academico.in' => 'Titulo academico desconocido.',
            'password.required' => 'Password es requerida.',
            'password.min' => 'Password demasiado corta.'

        ];
    }

    public function getUserByEmail(String $email)
    {
        $user = User::find(1)->where('email', $email)->first();
        if($user)
            return $user->id;
        else
            return 0;
    }
}
