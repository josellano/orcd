<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Config;

class ReunionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fecha' => 'required|date',
            'tipo'  => ['required', Rule::in([Config::get('constants.reunion.reunion_ordinaria'),Config::get('constants.reunion.reunion_extraordinaria'),Config::get('constants.reunion.reunion_constitutiva')])]
        ];
    }

    public function messages()
    {
        return [
            'tipo.required' => 'Tipo es requerido.',
            'tipo.in' => 'Tipo desconocido.',
            'fecha.min' => 'Fecha en formato desconocido.',
            'fecha.date' => 'Fecha incorrecta.'
        ];
    }
}
