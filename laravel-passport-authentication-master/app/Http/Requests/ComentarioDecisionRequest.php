<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Config;

class ComentarioDecisionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contenido' => 'required|min:1',
            'item_id' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'contenidon.required' => 'Contenido es requerido.',
            'contenido.min' => 'Contenido no debe estar vacio.',
            'item_id.required' => 'El comentario debe pertenecer a una decision de un item.',
            'item_id.integer' => 'Item incorrecto.'
        ];
    }
}
