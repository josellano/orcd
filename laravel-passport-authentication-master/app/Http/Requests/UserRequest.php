<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Config;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => 'required|email|unique:users',
            'telefono' => 'required|min:6',
            'puesto' => ['required', 'boolean'],
            'genero' => ['required', 'boolean'],
            'titulo_academico' => ['required', Rule::in([Config::get('constants.userTitulo.titulo_dr'), Config::get('constants.userTitulo.titulo_dra'), Config::get('constants.userTitulo.titulo_ing'), Config::get('constants.userTitulo.titulo_lic'), Config::get('constants.userTitulo.titulo_sr'), Config::get('constants.userTitulo.titulo_sra')])],
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Nombre es requerido.',
            'apellido.required' => 'Apellido es requerido.',
            'email.required' => 'Email es requerido.',
            'email.unique' => 'Email ya esta en uso.',
            'telefono.required' => 'Telefono es requerido.',
            'telefono.min' => 'Telefono debe tener minimo 6 digitos.',
            'puesto.required' => 'Puesto es requerido.',
            'puesto.boolean' => 'Puesto desconocido.',
            'genero.required' => 'Genero es requerido.',
            'genero.boolean' => 'Genero desconocido.',
            'titulo_academico.required' => 'Titulo academico es requerido.',
            'titulo_academico.in' => 'Titulo academico desconocido.',
            'password.required' => 'Password es requerida.',
            'password.min' => 'Password demasiado corta.'

        ];
    }
}
