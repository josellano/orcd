<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Config;

class PrecargadaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contenido' => 'required|min:1',
            'tipo' => ['required', Rule::in([Config::get('constants.precargada.precargada_item'),Config::get('constants.precargada.precargada_decision')])]
        ];
    }

    public function messages()
    {
        return [
            'contenidon.required' => 'Contenido es requerido.',
            'contenido.min' => 'Contenido no debe estar vacio.',
            'tipo.required' => 'Tipo es requerido.',
            'tipo.in' => 'Tipo desconocido.'
        ];
    }
}
