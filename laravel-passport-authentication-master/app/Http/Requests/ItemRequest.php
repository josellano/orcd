<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Config;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contenido' => 'required|min:1',
            'tipo' => ['required', Rule::in([Config::get('constants.items.item_comun'),Config::get('constants.items.item_sobretabla'),Config::get('constants.items.item_comunicacion')])],
            'ordendia_id' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'contenidon.required' => 'Contenido es requerido.',
            'contenido.min' => 'Contenido no debe estar vacio.',
            'tipo.required' => 'Tipo es requerido.',
            'tipo.in' => 'Tipo desconocido.',
            'ordendia_id.required' => 'El item debe pertenecer a una Orden del día',
            'ordendia_id.integer' => 'Orden del día incorrecta.'
        ];
    }
}
