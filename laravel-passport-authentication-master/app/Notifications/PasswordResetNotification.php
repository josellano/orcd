<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class PasswordResetNotification extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //$urlToResetForm = "http://vueapp.test/vue-app/reset-password-form/?token=". $this->token;
        $urlToResetForm = "http://192.168.0.111:8080/#/password/reset/". $this->token;
        return (new MailMessage)
            ->greeting('Hola,')
            ->subject(Lang::getFromJson('ORCD, Resetear contraseña'))
            ->line(Lang::getFromJson('Para resetar su contraseña, presione el siguiente botón.'))
            ->action(Lang::getFromJson('Resetear Contraseña'), $urlToResetForm)
            ->line(Lang::getFromJson('Si usted no solicito realizar el cambio de contraseña, ninguna accion es requerida, solo ignore el mensaje.Token: ==>'. $this->token));


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
