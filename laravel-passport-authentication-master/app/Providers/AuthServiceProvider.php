<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Config;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Gate::define('manage-users', function($user){
            //return $user->hasAnyRoles(['admin']);
            return $user->hasRole(Config::get('constants.roles.role_admin'));
        });

        Gate::define('edit-users', function($user){
            return $user->hasRole('admin');
        });

        Gate::define('delete-users', function($user){
            return $user->hasRole(Config::get('constants.roles.role_admin'));
        });

        Gate::define('manage-reuniones', function($user){
            return $user->hasRole(Config::get('constants.roles.role_admin'));
        });

        Gate::define('manage-items', function($user){
            return $user->hasRole(Config::get('constants.roles.role_admin'));
        });

        Gate::define('manage-precargadas', function($user){
            return $user->hasAnyRoles([Config::get('constants.roles.role_admin'), Config::get('constants.roles.role_secretario')]);
        });

        Gate::define('manage-decisiones', function($user){
            return $user->hasAnyRoles([Config::get('constants.roles.role_admin'), Config::get('constants.roles.role_secretario')]);
        });

        Gate::define('manage-asistencias', function($user){
            return $user->hasAnyRoles([Config::get('constants.roles.role_admin'), Config::get('constants.roles.role_secretario')]);
        });

        Gate::define('manage-comentarios', function($user){
            return $user->hasAnyRoles([Config::get('constants.roles.role_secretario'), Config::get('constants.roles.role_profesor'), Config::get('constants.roles.role_auxiliar'),Config::get('constants.roles.role_alumno')]);
        });
    }
}
